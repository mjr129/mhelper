from myptools.common_build import common_build
import os
my_dir = os.path.dirname( __file__ )

print( "[MHELPER BUILD]" )

#
# Compile our attributions
#
common_build(my_dir)

print( "[MHELPER BUILD] COMPLETE." )