"""
Provides `ConsoleHandler`.
"""

import logging.handlers
import logging
import logging.config
import logging.handlers
import logging.handlers
import sys
from dataclasses import dataclass
from typing import Callable, Dict, Optional

from mhelper import ansi, string_helper

from .progress_helper import Progress


__all__ = "ConsoleHandler"


class ConsoleHandler( logging.Handler ):
    """
    A logging handler that formats for the terminal and displays thread and
    process information.
    
    There are several class variables which control behaviour. These may also
    be set at instance scope.
    
    :cvar show_name:        Include logger name
    :cvar show_processes:   Include logger process
    :cvar show_threads:     Include logger threads
    :cvar target:           Where to write text to (any callable).
                            See `DefaultTarget` and `AltScreenTarget`. 
    """


    class DefaultTarget:
        """
        Writes to a function (target).
        By default this writes to stderr.
        """
        target: Callable[[str], None] = sys.stderr.write


        def __call__( self, x ):
            self.target( x )


    class AltScreenTarget:
        """
        A variation of `DefaultTarget` that is designed for logging when a
        alternate ANSI screen is visible.
        
        This works by switching to the primary ANSI screen to write output,
        then switching back to the alternate ANSI screen.
        
        Note::
        
            * This will cause some flickering on the alternate screen.
            * The target must be disengaged when resuming the regular screen.
            * Raw mode is assumed to be active, and carriage returns are
              inserted into the logs.
        """
        target: Callable[[str], None] = None


        def __call__( self, x ):
            x = x.replace( "\n", "\r\n" )
            x = f"{ansi.ALTERNATE_SCREEN_OFF}{x}{ansi.ALTERNATE_SCREEN}"
            self.target( x )


    @dataclass
    class __Colours:
        name: str
        sigil_n: str
        sigil_0: str

    default_target: Callable[[str], None] = DefaultTarget()
    AltScreenTarget.target = default_target
    target: Callable[[str], None] = default_target
    show_name: bool = True
    show_processes: bool = True
    show_threads: bool = True
    indent: int = 0
    __FG_COLOURS = [ansi.FR,
                    ansi.FG,
                    ansi.FB,
                    ansi.FC,
                    ansi.FY,
                    ansi.FM,
                    ansi.FW,
                    ansi.FK,
                    ansi.FBR,
                    ansi.FBG,
                    ansi.FBB,
                    ansi.FBC,
                    ansi.FBY,
                    ansi.FBM,
                    ansi.FBW,
                    ansi.FBK]
    __BG_COLOURS = [ansi.BR,
                    ansi.BG,
                    ansi.BB,
                    ansi.BC,
                    ansi.BY,
                    ansi.BM,
                    ansi.BW,
                    ansi.BK,
                    ansi.BBR,
                    ansi.BBG,
                    ansi.BBB,
                    ansi.BBC,
                    ansi.BBY,
                    ansi.BBM,
                    ansi.BBW,
                    ansi.BBK]


    def __repr__( self ):
        return "{}('{}')".format( "log_helper.Handler", self.name )


    def __str__( self ):
        return self.name

    def __init__( self,
                  *,
                  show_processes = None,
                  target = None,
                  show_threads = None,
                  show_name = None,
                  minimal = False ):
        """
        :param show_processes:      Show the process sidebar.
                                    Overrides the Handler cvar if provided. 
        :param target:              Where to send the output.
                                    Overrides the Handler cvar if provided.
        :param show_threads:        Show the thread sidebar.
                                    Overrides the Handler cvar if provided. 
        :param show_name:           Show the name sidebar.
                                    Overrides the Handler cvar if provided.
        :param minimal:             When `True`, overrides the Handler cvars `show_processes`, `show_threads` and `show_name`.
        """
        super().__init__()

        if minimal:
            self.show_processes = False
            self.show_threads = False
            self.show_name = False

        if show_name is not None:
            self.show_name = show_name

        if show_processes is not None:
            self.show_processes = show_processes

        if target is not None:
            self.target = target

        if show_threads is not None:
            self.show_threads = show_threads

        self.__colours: Dict[str, ConsoleHandler.__Colours] = { }


    def __get_colours( self, name: str ) -> __Colours:
        colours: Optional[ConsoleHandler.__Colours] = self.__colours.get( name )

        if colours is not None:
            return colours

        if "." in name:
            title = ".".join( name.rsplit( ".", 2 )[-2:] )
        else:
            title = name

        hash = string_helper.string_to_hash( name, binary = True )

        c1 = hash[0] % len( self.__FG_COLOURS )
        c2 = hash[1] % len( self.__BG_COLOURS )

        if c2 == c1:
            c2 = (c1 + 1) % len( self.__FG_COLOURS )

        foreground: str = self.__FG_COLOURS[c1]
        background: str = self.__BG_COLOURS[c2]

        colours = self.__Colours( title,
                                  foreground + background + " {} ".format( string_helper.fix_width( title, 15 ) ) + ansi.RESET + " " + ansi.FORE_CYAN + " ",
                                  foreground + background + " {} ".format( string_helper.fix_width( "''", 15 ) ) + ansi.RESET + " " + ansi.FORE_CYAN + " " )

        self.__colours[name] = colours
        return colours


    def fake( self, level, message ):
        self.emit( logging.LogRecord( self.name, 30, "", level, message, (), None ) )


    def emit( self, record: logging.LogRecord ):
        colours = self.__get_colours( record.name )

        if self.show_name:
            sigil_n = colours.sigil_n
            sigil_0 = colours.sigil_0
        else:
            sigil_n = ""
            sigil_0 = ""

        fw = string_helper.fix_width
        otext = text = record.getMessage()

        #
        # Indent?
        #
        if ConsoleHandler.indent > 0:
            indent = ansi.FORE_BRIGHT_BLACK + ("||||" * ConsoleHandler.indent) + ansi.FORE_CYAN
            text = indent + text.replace( "\n", "\n" + indent )

        if otext.startswith( Progress.INDENT_PREFIX ):
            ConsoleHandler.indent += 1
        elif otext.endswith( Progress.UNINDENT_SUFFIX ):
            ConsoleHandler.indent -= 1

        #
        # Colours
        #
        text = string_helper.highlight_quotes( text, "«", "»", ansi.FORE_YELLOW, ansi.FORE_GREEN )

        #
        # Thread
        #
        thread_name = record.threadName

        if not self.show_threads:
            thread_wid = 0
            thread = ""
        elif thread_name == "MainThread":
            thread_wid = 0
            thread = ""
        elif thread_name.startswith( "Thread-" ) and thread_name[7:].isdigit():
            thread_wid = 3
            thread = ansi.BACK_BLUE + ansi.FORE_WHITE + " {} ".format( fw( thread_name[7:].strip(), thread_wid ) ) + ansi.RESET
        else:
            thread_wid = 15
            thread = ansi.BACK_BLUE + ansi.FORE_WHITE + " {} ".format( fw( thread_name, thread_wid ) ) + ansi.RESET

        #
        # Process
        #
        pid = record.process

        if self.show_processes:
            process = ansi.BACK_BLUE + ansi.FORE_BRIGHT_BLUE + " {} ".format( fw( str( pid ), 8 ) ) + ansi.RESET
        else:
            process = ""

        sigil = sigil_n

        text = text.split( "\n" )

        for tex in text:
            tex = ansi.FORE_GREEN + "{}{}{}{}\n".format( process, thread, sigil, tex ) + ansi.FORE_RESET
            self.target( tex )

            sigil = sigil_0
            process = process and ansi.BACK_BLUE + ansi.FORE_BRIGHT_BLUE + " {} ".format( fw( "''", 8 ) ) + ansi.RESET
            thread = thread and ansi.BACK_BLUE + ansi.FORE_WHITE + " {} ".format( fw( "'' ", thread_wid ) ) + ansi.RESET
