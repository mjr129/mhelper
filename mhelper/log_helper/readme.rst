MLog - logging helpers
----------------------


Progress helper
---------------

Time tracking, completion time prediction, and formatting of progress bars.


Wrapper
-------

A `Logger` wrapper classes: `MLog`, `LogGroup`.

Provides nested logs and an interface to progress helper.


ConsoleHandler
--------------

A logging handler that formats for an ANSI colour terminal, including
thread and process information.


LogServer
---------

Uses a single multicast port (to avoid the clients having to know the port!).
Configuration is simplified (and safe!) and only allows logs to be turned on and
off, and the list of available loggers enumerated by the client.

* Many-to-one logging over UDP. (i.e. one client receives from multiple applications)
* One-to-many configuration over UDP. (i.e. one client configures multiple applications) 


LogClient
---------

Boilerplate client code for the above server.


Documentation
-------------

Extensive API documentation is provided in PEP-257 doc comments and PEP-484
annotations and is not repeated here. Please view it in an IDE.


Examples - Wrapper and progress helper
--------------------------------------

Import package
~~~~~~~~~~~~~~

Input::

    import mhelper.log_helper as lh

Log a nested message
~~~~~~~~~~~~~~

Input::

    log = lh.MLog("my_messages")
    log.attach_to_console()

    with log.action("Some action"):
        log("hello, world")
    
Output::

    30700     my_messages       hello, world


Log the progress of a task
~~~~~~~~~~~~~~~~~~~~~~~~~~

Input::

    items = [1,2,3,4,5]
     
    for item in log.iterate(items):
        pass


Output::

     42672  my_messages  |=(TASK)==>
     42672  my_messages      ==> 0 of 5 (0%) -> [688μs]
     42672  my_messages      ==> 4 of 5 (80%) - [960μs] -> ~240μs for 1 remaining at 4,166r/s or 240μs/r (complete)
        
        
Log the time taken for a task
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input::

    with log.time("spam"):
        pass
        
Output::

     25684  my_messages  |=spam==>
     ''     ''               ==> (Please wait)
     25684  my_messages      ==> Completed in 1,684Hz.

Nest progress bars
~~~~~~~~~~~~~~~~~~

Input::

    for item in log.iterate(items):
        for index, item2 in log.enumerate(items):
            pass

Output::

     28960  my_messages  |=(TASK)==>
     28960  my_messages    ==> 0 of 5 (0%) -> [333μs]
     28960  my_messages      |=(TASK)==>
     28960  my_messages        ==> 4 of 5 (80%) - [253μs] -> ~63us for 1 remaining at 15,835r/s or 63us/r
     ...
     
Examples - ConsoleHandler
-------------------------

Attach a logger to the ConsoleHandler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input::

    my_logger = logging.get_logger("my logger")
    my_logger.handlers.append( lh.LogConsole() )

Attach multiple loggers to ConsoleHandler and LogServer.handler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input::

    group = lh.MLogGroup()
    my_logger = lh.MLog("my_logger", group=group)
    my_other_logger = lh.MLog("my_other_logger", group=group)
    group.attach_as_root()
     
Examples - LogServer
--------------------


Start the log server
~~~~~~~~~~~~~~~~~~~~

Input::

    lh.LogServer.start()
    
    
Attach a logger to LogServer.handler
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input::

    my_logger = logging.get_logger("my logger")
    my_logger.handlers.append( lh.LogServer.handler ) 
    

Enable a logger on via the LogClient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Server::

    lh.LogServer.start()
    my_logger = logging.get_logger("my logger")
    my_logger.handlers.append( lh.LogServer.handler )
    my_other_logger = logging.get_logger("my_other_logger")    
    
Client::

    lh.LogClient.send( "l 1 my_other_logger" ) # enable logging for my_other_logger
