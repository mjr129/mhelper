"""
Provides the `MLog` class.

This is a basic wrapper around `logging.logger`.
"""
import logging.handlers
import functools
import logging
import logging.config
import logging.handlers
import logging.handlers
from typing import Any, Callable, cast, Iterable, Iterator, List, Optional, Set, TypeVar, Union

from mhelper import string_helper, exception_helper

from . import progress_helper

T = TypeVar( "T" )

__all__ = ["MLog", "TLogger", "unwrap_mlog", "LogGroup", "ILogger"]


class ILogger:
    """
    Common interface for loggers.
    """

    def callback( self, callback: Callable[[logging.Logger], None] ):
        """
        Calls the `callback` method on the `Logger` instance(s) for this object.
        """
        raise NotImplementedError( "abstract" )

    def get_loggers( self ) -> Iterable[logging.Logger]:
        """
        Returns `Logger` instance(s) for this object.
        """
        raise NotImplementedError( "abstract" )

    def attach_to_console( self ):
        """
        Convenience function that attaches to a `ConsoleHandler` instance.
        """
        from .console_handler import ConsoleHandler
        console_handler = ConsoleHandler()

        def attach( logger: logging.Logger ):
            logger.setLevel( logging.DEBUG )
            logger.handlers.append( console_handler )

        self.callback( attach )

    def attach_as_root( self ):
        """
        Convenience method that shows the logs for this object in the console
        and starts the logging server.
        """
        from .console_handler import ConsoleHandler
        from .server_handler import LogServer

        LogServer.start()
        console_handler = ConsoleHandler()

        def attach( logger: logging.Logger ):
            logger.setLevel( logging.DEBUG )
            logger.handlers.append( console_handler )
            logger.handlers.append( LogServer.handler )

        self.callback( attach )


    def add_handler( self, handler: logging.Handler ) -> None:
        """
        Adds a `handler` to the `Logger` instance(s) for this object.
        
        :param handler:     Handler to register. 
        """

        def attach( logger: logging.Logger ):
            logger.setLevel( logging.DEBUG )
            logger.handlers.append( handler )

        self.callback( attach )

    def union( self, other: "ILogger" ) -> "LogGroup":
        """
        Creates a `LogGroup` that is the union of this object and one or more
        other `ILogger`\s.  
        """
        assert isinstance( other, ILogger ) or isinstance( other, logging.Logger )
        return LogGroup( "union", "union", [*self.get_loggers(), other] )


    def __or__( self, other: "ILogger" ):
        """
        Alias for `union`.
        """
        assert isinstance( other, ILogger ) or isinstance( other, logging.Logger )
        return self.union( other )


class MLogType( type ):
    """
    This metaclass simply allows __init__ to reuse the same instances.
    """

    def __call__( cls, *args, **kwargs ):
        name = args[0]

        self = MLog.registered.get( name, None )

        if self is None:
            self = cls.__new__( cls, *args, **kwargs )
            self.__init__( *args, **kwargs )
            MLog.registered[name] = self
        else:
            self._reacquire( *args, **kwargs )

        return self


class MLog( ILogger, metaclass = MLogType ):
    """
    Wraps `logging.Logger`.
    
    * Provides features such as progress bars, timers, etc.
    * Does away with different logging levels.
        
    :cvar level:               For simplicity, we use a fixed logger level. This is it.
    :cvar registered:          This is the list of `MLog`\s that have been created, mapped by their name.
    :ivar logger:              Reference to the underlying logger.
    :ivar doc:                 Documentation for the logger, for reference only.
    """
    level: int = logging.INFO
    registered = { }
    __allow_enable_hint = True
    __slots__ = "logger", "doc"


    def __init__( self,
                  name: str,
                  doc: str = "",
                  group: "LogGroup" = None ):
        """
        Constructor.
        
        :param name:        Name of the underlying logger.
                            Calling the constructor multiple times with the same name will use the same `MLog` instance.
        :param doc:         Documentation for the logger, for reference only. 
        :param group:       When specified, this logger is added to the group.  
        """
        # Prevent deprecated old-style calls
        exception_helper.assert_type( "name", name, str )
        exception_helper.assert_type( "comment", doc, str )
        exception_helper.assert_type_or_none( "group", group, LogGroup )

        MLog.registered[name] = self

        self.logger = logging.getLogger( name )
        self.doc = None
        self._reacquire( name, doc, group )

        LogGroup.ALL.add( self )


    def _reacquire( self,
                    name: str,
                    doc: str = "",
                    group: "LogGroup" = None ):
        """
        Called when an `MLog` instance is reacquired to update properties if
        required. See `__init__` for argument documentation.
        """
        assert name == self.name

        self.doc = self.doc or doc

        if group is not None:
            group.add( self )


    def __repr__( self ):
        """
        Alias for `name`.
        """
        return self.logger.name


    @property
    def name( self ) -> str:
        """
        Name of the underlying logger.
        """
        return self.logger.name


    def __bool__( self ) -> bool:
        """
        Alias for `enabled`.
        """
        return self.enabled


    @property
    def enabled( self ) -> bool:
        """
        Gets or sets whether the underlying logger is enabled (at `level`).
        """
        return self.logger.isEnabledFor( self.level )


    @enabled.setter
    def enabled( self, value: bool ) -> None:
        """
        Sets whether the logger is enabled (at `level`).
        """
        if value:
            self.logger.setLevel( self.level )
        else:
            self.logger.setLevel( logging.NOTSET )


    def __call__( self, *args, **kwargs ) -> "MLog":
        """
        Logs text.
        """
        if self.enabled:
            self.print( self.format( *args, *kwargs ) )

        return self


    def format( self, *args, **kwargs ) -> str:
        """
        Formats text for output.
        """
        if len( args ) == 1:
            r = str( args[0] )
        elif len( args ) > 1:
            vals: List = list( args[1:] )

            for i in range( len( vals ) ):
                v = vals[i]

                if type( v ) in (set, list, tuple, frozenset):
                    v = string_helper.array_to_string( v, **kwargs )

                vals[i] = "«" + str( v ) + "»"

            r = args[0].format( *vals )
        else:
            r = ""

        return r


    class defer:
        """
        Allows a function passed via `format` to lazily load its string representation.
        """


        def __init__( self, fn: Callable[[], object] ):
            self.fn = fn


        def __repr__( self ) -> str:
            return str( self.fn() )


    def print( self, message: str ) -> None:
        """
        Directly sends `message` to the underlying logger.
        """
        self.logger.log( self.level, message )


    def __enter__( self ) -> None:
        """
        Indents the logger.
        
        Since `self` is returned from `__call__`, both `with log:` and
        `with log("spam"):` are permissible.
        """
        self( progress_helper.Progress.INDENT_PREFIX )


    def __exit__( self, exc_type, exc_val, exc_tb ) -> None:
        """
        Un-indents the logger.
        """
        self( progress_helper.Progress.UNINDENT_SUFFIX )


    def timed( self, fn: Optional[Callable] = None ) -> Callable:
        """
        A decorator that wraps a function in a `MLog.time` block.
        
        Both `@timed` and `@timed()` are acceptable decorators.
        """


        def dec( fn: Callable ) -> Callable:
            @functools.wraps( fn )
            def new_fn( *args, **kwargs ):
                with self.time( fn.__name__ ):
                    return fn( *args, **kwargs )


            return new_fn


        if fn:
            return dec( fn )
        else:
            return dec


    def attach( self, fn: Optional[Callable] = None ) -> Callable:
        """
        A decorator that logs when a function is called.
        
        Both `@attach` and `@attach()` are acceptable decorators.
        """


        def dec( fn: Callable ) -> Callable:
            def ret( *args, **kwargs ):
                self( "@{}", fn.__name__ )

                with self:
                    return fn( *args, **kwargs )


            return ret


        if fn:
            return dec( fn )
        else:
            return dec


    def enumerate( self, *args, **kwargs ):
        """
        Logs the iterations of an `enumerate`.
        See `iterate`.
        """
        return enumerate( self.iterate( *args, **kwargs ) )


    def action( self,
                title = None,
                total: Optional[int] = None,
                *,
                period: float = None,
                item: str = "record",
                text: Optional[Union[str, Callable[[Any], str]]] = None ) -> "progress_helper.ProgressMaker":
        """
        Designates a region as an activity, indenting the logger and providing
        optional progress reports. Typically used in a `with` block, but see
        `ProgressMaker` for more usage details::
        
            with logger.action(. . .) as action:
                . . . 
        
        :param title:   Passed to `ProgressMaker`. 
        :param total:   Passed to `ProgressMaker`.
        :param period:  Passed to `ProgressMaker`. 
        :param item:    Passed to `ProgressMaker`. 
        :param text:    Passed to `ProgressMaker`. 
        :return:        `ProgressMaker`
        """
        return progress_helper.ProgressMaker( title = title,
                                              total = total,
                                              issue = cast( Any, self ),
                                              period = period,
                                              item = item,
                                              text = text )


    time = action  # alias


    def iterate( self,
                 records: Iterable[T],
                 count = None,
                 *,
                 title = None,
                 period: float = None,
                 item: str = "record",
                 text: Optional[str] = None ) -> Iterator[T]:
        """
        Logs the iterations.
        
        :param records:     Iterable. 
        :param count:       See `progress_helper.ProgressMaker`.
        :param title:       See `progress_helper.ProgressMaker`.
        :param period:      See `progress_helper.ProgressMaker`.
        :param item:        See `progress_helper.ProgressMaker`.
        :param text:        See `progress_helper.ProgressMaker`.
        :return:            Iterator over `records` that logs the iterator's progress. 
        """
        if not self:
            for record in records:
                yield record
        else:
            if count is None:
                try:
                    # noinspection PyTypeChecker
                    count = len( records )
                except Exception:
                    count = None

            with self.action( total = count,
                              title = title,
                              period = period,
                              item = item,
                              text = text ) as action:
                for current, record in enumerate( records ):
                    action.report( current )
                    yield record


    def union( self, other ):
        assert isinstance( other, ILogger ) or isinstance( other, logging.Logger )
        return self._as_group().union( other )


    def _as_group( self ):
        g = LogGroup( self.name )
        g.add( self )
        return g

    def callback( self, callback: Callable[[logging.Logger], None] ):
        callback( self.logger )

    def get_loggers( self ) -> Iterable[logging.Logger]:
        return self.logger,


# noinspection PyAbstractClass
class LogGroup( ILogger ):
    """
    Maintains a group of loggers.
    
    :cvar ALL: Concrete `LogGroup` instance that contains all registered loggers.
    """
    __slots__ = "__name", "__contents", "__union", "doc"
    ALL: "LogGroup" = None


    def __init__( self, name: str, doc: str = "", contents: Iterable[ILogger] = () ):
        """
        :param name:        Name of the group  
        """
        self.__name = name
        self.__contents: Set[logging.Logger] = set()
        self.doc = doc
        self.callbacks = []

        for logger in contents:
            if not isinstance( logger, ILogger ) and not isinstance( logger, logging.Logger ):
                raise TypeError( f"Preliminary check, not a logger: {type( logger ).__name__} - {logger!r}" )

            self.add( logger )


    def add( self, logger: "TLogger" ):
        """
        Adds a logger to this group.
        
        If the group is a union, the logger is added to *each* subgroup.
        """
        if isinstance( logger, logging.Logger ):
            loggers = logger,
        elif isinstance( logger, ILogger ):
            loggers = logger.get_loggers()
        else:
            raise TypeError( f"Not a logger: {type( logger ).__name__} - {logger!r}" )

        for logger_ in loggers:
            self.__contents.add( logger_ )

            for callback in self.callbacks:
                callback( logger_ )


    def get_loggers( self ) -> Iterable[logging.Logger]:
        return self.__contents


    def callback( self, callback: Callable[[logging.Logger], None] ):
        for logger in self.get_loggers():
            callback( logger )

        self.callbacks.append( callback )


TLogger = Union[str, logging.Logger, ILogger]


def unwrap_mlog( obj ):
    if isinstance( obj, logging.Logger ):
        return obj
    elif isinstance( obj, MLog ):
        return obj.logger
    elif isinstance( obj, str ):
        from .utilities import get_manager
        return get_manager().loggerDict[obj]
    else:
        raise TypeError( "Not a `TLogger`." )


LogGroup.ALL = LogGroup( "ALL" )
