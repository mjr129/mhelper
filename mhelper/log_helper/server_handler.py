"""
A TCP/UDP server implementation.
See `LogServer`.
"""
import collections
import os
import logging
import logging.config
import logging.handlers
import logging.handlers
import logging.handlers
import hashlib
import secrets
import sys
import threading
from functools import lru_cache
from logging import LogRecord
from typing import Dict, Iterable, Literal, Optional, Sequence, Set, Tuple, Union

from .wrapper import MLog
from . import utilities

__all__ = ["LogServer"]


class LogServer:
    """
    !STATIC
    
    A log server implementation.
    
    This logs and is configured via UDP only. Configuration takes the form
    of simple logger-enable and logger-disable calls for the log server
    only and cannot be used to add or control other handlers.

    This avoids many issues with the native TCP server.
    
    Usage::
    
        LogServer.start( ... )
        
        
    Configuration is issued via UDP to the server, it is transmitted as a
    UINT32-prefixed text-block in UTF8. Each line should begin with a member of
    `Commands`, followed by an optional tab-delimited list of arguments.
    `server_handler_client.py` contains the boilerplate code for this.
    
    :cvar broadcast_log:        Information from the log server itself and configuration
                                requests are sent here. This log is always handled by
                                the log server.
    :cvar transmitter:          The log server handler.
                                Transmits the logs.
    :cvar receiver:             The log server config watcher.
                                Receives and parses configurations.
    :cvar configurator:         Intermediate between the `sender` and `receiver`.
    :cvar handler:              A proxy for `transmitter` that may set as a loggers
                                handler before the `transmitter` becomes available.
                                While there is no `transmitter` any logs received
                                are discarded. 
    """

    broadcast_log: MLog
    transmitter: Optional["LogServerInstance"] = None
    receiver: Optional["_LogServerConfigReceiver"] = None
    configurator: Optional["_LogServerConfigProcessor"] = None

    class _HandlerProxy( logging.Handler ):
        """
        See :cvar:`LogServer.handler`.
        """

        def emit( self, record: LogRecord ) -> None:
            if LogServer.transmitter is not None:
                LogServer.transmitter.emit( record )

    handler = _HandlerProxy()


    class Constants:
        COMMAND_DELIMITER = "\n"
        PARAM_DELIMITER = "\t"
        ENCODING = "utf8"
        BROADCAST_LOG_NAME = "log_helper.log_server"
        DEFAULT_CONFIG_PORT = 9999
        DEFAULT_PORT = logging.handlers.DEFAULT_TCP_LOGGING_PORT
        DEFAULT_HOST = "localhost"
        DEFAULT_PROTOCOL = "udp"
        DEFAULT_CONFIG_MULTICAST_GROUP = "224.1.1.1"
        DEFAULT_CONFIG_SECRET: Union[str, bool] = True
        SECRET_SALT_SIZE = 16
        SIGNATURE_SIZE = 32  # must be the size of the hash function
        _ASTERISK = -1


    class Commands:
        """
        Commands accepted by the log server.
        
        Commands are newline separated.
        Commands start with a member of this class, optionally followed by
        a tab-delimited list of parameters.
        
        :cvar RESET: 'r'
        
            Description
            -----------
            
            Restore all original handlers.
            
            
            Parameters
            ----------
        
            No parameters.
            
            
            Examples
            --------
            
            Reset handlers::
            
                r
                                
        :cvar GET_STATE: 's'
        
            Description
            -----------
            
            List available loggers and thread filters. 
            
            This can be used by the client to get the list of logger names, or
            to inspect or confirm the currently enabled loggers and threads.
            See `Responses` for the response format.
        
        
            Parameters
            ----------
            
            * Params[0] - State filtering mask. All on by default.
                          l - loggers
                          t - threads
                          p - processes
                          d - descriptions
                                
            Examples
            --------
              
            Show process and thread state::
            
                s␉pt
        
        :cvar ECHO: 'e'             
        
            Description
            -----------
            
            Test connection / echo parameters.
        
        
            Parameters
            ----------
            
            * Params[0] is the text to echo.
            * Params[1] is the name of the logger to use for the echo.
              If this is not specified, the main logserver logger is used.            
            
            Examples
            --------
            
            Echo "Hello, world!"::
            
                e␉Hello, world!
        
        :cvar SET_LOGGER_STATE: 'l'
        
            Description
            -----------
            
            Enable or disable loggers.
            
            * When a logger is enabled, this sets the loggers level to DEBUG
              and tells it to issue to `LogServer.sender`.
            * When a logger is disabled this sets the logger's level to NONE.
              This means the logger is disabled entirely and it won't show up
              for other handlers, if any.


            Parameters
            ----------
            
            * Params[0] is 1 or 0.
            * Params[1:] are the logger name(s).
            
            To indicate all known loggers use::
            
                *
                
              
            Examples
            --------
            
            Enable logging for 'my_logger'::
            
                l␉1␉my_logger
                                
        :cvar SET_PROCESS_STATE: 'p'
        
            Description
            -----------
            
            Enable or disable logging.
            
            * When a process is disabled, all logging is stopped, equivalent to
              a "SET_LOGGER_STATE 0 *" request.
              Any `SET_LOGGER_STATE` requests received after the process is disabled
              will be queued but not acted upon.
              If the process is at some point re-enabled, the logging state is
              restored and the queue enacted, bringing it in line with any
              other processes.
            * All processes are enabled by default.
            
            
            Parameters
            ----------
                                
            * Params[0] is 1 or 0.
            * Params[1:] are the process ID(s).
            
            
            For all processes use::
            
                *
            
            
            Examples
            --------
            
            Enable logging for process #12345::
                
                p␉1␉12345
                                
        :cvar SET_THREAD_STATE: 't'
        
            Description
            -----------
            
            Enables or disables logging for a thread.
            
            * Enabling or disabling threads does not affect logging.
              It is a filter that is only applied just before messages are sent to the client.
            * Thread IDs may be reused.
            * Wildcard forms may only be used to *enable* all threads.
              To *disable* all threads `SET_PROCESS_STATE` should be used instead.
            * All threads are enabled by default.
        
        
            Parameters
            ----------
            
            * Params[0] is 0 or 1.
            * Params[1:] are the thread IDs.
            
            Thread IDs have the form::
            
                <process_id>.<thread_id>.
                
            For all threads::
             
                *.*
                
            For all threads for a process::
            
                <process_id>.* 
            
            
            Examples
            --------
            
            Enable logging for thread #12345::
            
                p␉1␉12345
        """
        RESET = "r"
        GET_STATE = "s"
        ECHO = "e"
        SET_LOGGER_STATE = "l"
        SET_PROCESS_STATE = "p"
        SET_THREAD_STATE = "t"


    class Responses:
        """
        Responses issued by the log server.
        Responses are in the form of an issued log message.
        
        The response format is identical to the command format.
        The message contains newline delimited commands, where each command is a member of this class.
        Each command is optionally followed by a tab delimited list of parameters. 
        Note that the first command in any message is always `RESPONDING`, which allows the response
        to be identified.
        
        Typically, due to constraints on the size of any datagram:
        * Responses only contain one command at a time (besides `RESPONDING`).
        * One `LOGGER_STATE` or `THREAD_STATE` is issued per logger or thread, rather than one for all the loggers or threads. 
        
        :cvar RESPONDING:
        
            The first command in any message is always `RESPONDING`.
            
            * Params[0] is the process ID.
            
        :cvar ECHO:
        
            Response to `Commands.ECHO`.
            
            * Params[0] is a success state (0|1).
            * Params[1] is that passed to the original command.
            
        :cvar LOGGER_STATE:
        
            Response to `Commands.GET_STATE`.
            
            * params[0] is 0 or 1.
            * params[1:] is a list of logger name(s).
            
        :cvar THREAD_STATE:
        
            Response to `Commands.GET_STATE`.
            
            * params[0] is always 1.
            * params[1:] is a list of thread ID(s).
            
        :cvar PROCESS_STATE:
        
            Response to `Commands.GET_STATE`.
            
            * params[0] is 0 or 1.
            * params[1] is the process ID.
            
        :cvar LOGGER_DESCRIPTION:
        
            Response to `Commands.GET_STATE`.
            
            * params[0] is the logger name.
            * params[1] is the logger description.
        """
        RESPONDING = "!!"
        ECHO = "E"
        LOGGER_STATE = "L"
        THREAD_STATE = "T"
        PROCESS_STATE = "P"
        LOGGER_DESCRIPTION = "D"


    @staticmethod
    def start( host = Constants.DEFAULT_HOST,
               port = Constants.DEFAULT_PORT,
               config_host = Constants.DEFAULT_HOST,
               config_port = Constants.DEFAULT_CONFIG_PORT,
               config_multicast: Optional[str] = Constants.DEFAULT_CONFIG_MULTICAST_GROUP,
               config_secret: Union[bool, str] = Constants.DEFAULT_CONFIG_SECRET,
               ) -> bool:
        """
        Starts the server, if it is not started already.
        
        :return:    `True` if the server was started using these parameters,
                    `False` if the server is already running.
        """
        if LogServer.transmitter is not None:
            return False

        message = f"[MLOG] Starting log server on {host}:{port} config port {config_host}:{config_port}"

        LogServer.transmitter = LogServerInstance( host, port )
        LogServer.receiver = _LogServerConfigReceiver( config_host, config_port, config_multicast, config_secret )
        LogServer.configurator = _LogServerConfigProcessor( LogServer.transmitter )

        _broadcast_log.add_handler( LogServer.transmitter )

        #
        # Send a test message
        #
        _broadcast_log( message )
        return True


    @staticmethod
    def parse_commands( source: Union[bytes, str, memoryview] ) -> Iterable[Tuple[str, Sequence[str]]]:
        if isinstance( source, bytes ):
            config_text = source.decode( LogServer.Constants.ENCODING )
        elif isinstance( source, memoryview ):
            config_text = source.tobytes().decode( LogServer.Constants.ENCODING )
        elif isinstance( source, str ):
            config_text = source
        else:
            assert False

        commands_src = config_text.split( LogServer.Constants.COMMAND_DELIMITER )

        for command_src in commands_src:
            params_src = command_src.split( LogServer.Constants.PARAM_DELIMITER )
            command = params_src[0]
            params = params_src[1:]
            yield command, params


    @staticmethod
    def format_response( source: Sequence[Sequence[str]] ) -> str:
        assert isinstance( source, collections.Sequence )
        assert isinstance( source[0], collections.Sequence )
        assert isinstance( source[0][0], str )
        return LogServer.Constants.COMMAND_DELIMITER.join(
                (LogServer.Constants.PARAM_DELIMITER.join( str( x ) for x in source1 )
                 for source1 in source) )

    @staticmethod
    @lru_cache
    def load_secret() -> bytes:
        from mhelper import specific_locations
        import os
        file_name = specific_locations.get_application_file_name( "rusilowicz", "mhelper", "log_helper", "config_secret.dat" )
        if os.path.isfile( file_name ):
            with open( file_name, "rb" ) as fin:
                return fin.read()

        secret = secrets.token_bytes( 64 )

        with open( file_name, "wb" ) as fout:
            fout.write( secret )

        return secret


class LogServerInstance( logging.handlers.DatagramHandler ):


    def emit( self, record: LogRecord ) -> None:
        super().emit( record )


class _LogServerConfigProcessor:
    def __init__( self, server: LogServerInstance ):
        self.server = server
        self.thread_filter: Optional[Set[int]] = None
        self.is_process_enabled = True
        self.recorded_logger_state: Optional[Dict[str, bool]] = None
        self.original_logger_states: Dict[str, Tuple[int, Sequence[logging.Handler]]] = { }


    def reset_all( self ):
        self.set_process_enabled( os.getpid(), True )
        self.thread_filter = None

        for logger_name, (level, handlers) in self.original_logger_states.items():
            lg = logging.getLogger( logger_name )
            lg.setLevel( level )
            lg.handlers.clear()
            lg.handlers.extend( handlers )


    def set_thread_enabled( self, pid: int, tid: int, e: bool ):
        msg = f"Enable thread {pid!r}.{tid!r}-->{e!r}: "

        if pid != LogServer.Constants._ASTERISK and int( pid ) != os.getpid():
            _broadcast_log( f"{msg}Not me" )
            return

        if tid == LogServer.Constants._ASTERISK:
            LogServer.transmitter.thread_filter = None
            _broadcast_log( f"{msg}OK" )
            return

        if e == self.get_thread_enabled( tid ):
            _broadcast_log( f"{msg}Already so" )
            return

        if e:
            LogServer.transmitter.thread_filter.remove( tid )

            if not LogServer.transmitter.thread_filter:
                LogServer.transmitter.thread_filter = None
        else:
            if LogServer.transmitter.thread_filter is None:
                LogServer.transmitter.thread_filter = set()

            LogServer.transmitter.thread_filter.add( tid )

        _broadcast_log( f"{msg}OK" )


    def get_thread_enabled( self, tid: int ) -> bool:
        return self.thread_filter is None or tid not in self.thread_filter


    def set_process_enabled( self, pid: int, e: bool ):
        if pid != LogServer.Constants._ASTERISK and int( pid ) != os.getpid():
            _broadcast_log( f"Enable process {pid!r}-->{e!r}: Not me" )
            return

        if e == self.is_process_enabled:
            _broadcast_log( f"Enable process {pid!r}-->{e!r}: Already so" )
            return

        if e:
            self.is_process_enabled = True

            for logger_name, is_enabled in self.recorded_logger_state.items():
                self.set_logger_enabled( logger_name, is_enabled )

            self.recorded_logger_state = None
        else:
            self.recorded_logger_state = { }

            for li in utilities.list_loggers():
                self.recorded_logger_state[li.name] = self.__get_logger_enabled( li )
                self.set_logger_enabled( li.name, False )

            self.is_process_enabled = False

        _broadcast_log( f"Enable process {pid!r}-->{e!r}: OK" )


    def set_logger_enabled( self, lid: str, e: bool ):
        if not self.is_process_enabled:
            self.recorded_logger_state[lid] = e
            _broadcast_log( f"Enable logger {lid!r}-->{e!r}: Applied to state" )
            return

        if e == self.__get_logger_state():
            _broadcast_log( f"Enable logger {lid!r}-->{e!r}: Already so" )

        if lid == _broadcast_log.name:
            _broadcast_log( f"Enable logger {lid!r}-->{e!r}: Cannot modify" )
            return

        lg = logging.getLogger( lid )

        if lid not in self.original_logger_states:
            self.original_logger_states[lid] = lg.level, tuple( lg.handlers )

        if e:
            lg.handlers.clear()
            lg.handlers.append( LogServer.transmitter )
            lg.setLevel( logging.DEBUG )
        else:
            lg = logging.getLogger( lid )
            lg.handlers.remove( LogServer.transmitter )
            lg.setLevel( logging.NOTSET )

        _broadcast_log( f"Enable logger {lid!r}-->{e!r}: OK" )


    def get_logger_enabled( self, lid ):
        lg = logging.getLogger( lid )
        return self.__get_logger_enabled( lg )


    def __get_logger_enabled( self, lg: logging.Logger ):
        return any( handler is LogServer.transmitter
                    or handler is LogServer.handler
                    for handler
                    in lg.handlers )


    def broadcast_state( self, filter: str = "" ):
        if not filter:
            filter = "ltpd"

        _broadcast_log( f"Broadcasting state with filter {filter!r}." )

        if "l" in filter:
            self.__broadcast_state_of_loggers()

        if "t" in filter:
            self.__broadcast_state_of_threads()

        if "p" in filter:
            self.__broadcast_state_of_process()

        if "d" in filter:
            self.__broadcast_state_of_descriptions()

    def __broadcast_state_of_descriptions( self ):
        for logger in utilities.list_loggers():
            self.broadcast_command( LogServer.Responses.LOGGER_DESCRIPTION, logger.name, utilities.get_description( logger ) )

    def __broadcast_state_of_process( self ):
        is_process_enabled_str = "1" if self.is_process_enabled else "0"
        pid = os.getpid()
        self.broadcast_command( LogServer.Responses.PROCESS_STATE, is_process_enabled_str, f"{pid}" )

    def __broadcast_state_of_threads( self ):
        pid = os.getpid()
        disabled_threads = self.thread_filter or ()
        for tid in disabled_threads:
            self.broadcast_command( LogServer.Responses.THREAD_STATE, "0", f"{pid}.{tid}" )

    def __broadcast_state_of_loggers( self ):
        disabled_loggers, enabled_loggers = self.__get_logger_state()
        for ln in disabled_loggers:
            self.broadcast_command( LogServer.Responses.LOGGER_STATE, "0", ln )
        for ln in enabled_loggers:
            self.broadcast_command( LogServer.Responses.LOGGER_STATE, "1", ln )


    def broadcast_command( self, *args: str ):
        msg = []
        msg.append( [LogServer.Responses.RESPONDING] )
        msg.append( args )
        txt = LogServer.format_response( msg )
        _broadcast_log( txt )


    def broadcast_echo( self, echo_text: str, echo_logger: Optional[str] = None, success: bool = True ):
        msg = []
        msg.append( LogServer.Responses.RESPONDING )
        msg.append( f"{LogServer.Responses.ECHO}\t{1 if success else 0}\t{echo_text}" )

        if echo_logger is None:
            logger = _broadcast_log
        else:
            logger = getattr( logging.Logger, "manager" ).loggerDict.get( echo_logger, None )

            if logger is None:
                self.broadcast_echo( f"Echo failed. No such logger as {echo_logger}." )
                return

        logger( LogServer.Constants.COMMAND_DELIMITER.join( msg ) )


    def __get_logger_state( self ):
        enabled_loggers = []
        disabled_loggers = []

        for li in utilities.list_loggers():
            if self.__get_logger_enabled( li ):
                enabled_loggers.append( li.name )
            else:
                disabled_loggers.append( li.name )

        return disabled_loggers, enabled_loggers


class _LogServerConfigReceiver:
    """
    Receives log configurations.
    See `LogServer`.
    """


    def __init__( self, host: str, port: int, multicast_group: Optional[str], secret: Union[bytes, bool] ):
        self.host = host
        self.port = port
        self.multicast_group = multicast_group

        # Start the listener thread
        self.thread = threading.Thread( target = self.__thread_main )
        self.thread.daemon = True
        self.thread.start()

        if secret is True:
            secret = LogServer.load_secret()
        elif secret is False:
            secret = None

        self.secret: Optional[bytes] = secret


    def __thread_main( self ):
        """
        Listener thread.
        """
        import socket
        import struct
        socket_ = socket.socket( socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP )
        socket_.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

        if self.multicast_group:
            assert self.host == "localhost", "multicast_group must be using localhost"
            membership: bytes = struct.pack( "4sl", socket.inet_aton( self.multicast_group ), socket.INADDR_ANY )
            socket_.setsockopt( socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, membership )

            try:
                socket_.bind( (self.multicast_group, self.port) )
            except OSError:
                socket_.bind( ("", self.port) )  # Receiving multicast from a specific group doesn't work on Windows
        else:
            socket_.bind( (self.host, self.port) )

        buffer = bytearray( 2048 )
        memory = memoryview( buffer )

        while True:
            num_bytes_received: int = socket_.recv_into( memory, len( buffer ) )

            if num_bytes_received < 4:
                continue

            rx_bytes: memoryview = memory[:num_bytes_received]
            num_expected_bytes: int = struct.unpack( ">L", memory[:4] )[0]

            if num_expected_bytes + 4 != num_bytes_received:
                _broadcast_log( f"Discarding datagram because the length prefix is wrong {num_expected_bytes} {num_bytes_received}." )
                continue

            rx_bytes = rx_bytes[4:]

            data_bytes: memoryview = self.__remove_signature( rx_bytes, self.secret )
            self.__process( data_bytes )


    def __process( self, rx_bytes: memoryview ):
        """
        Processes the command block.
        """
        cora = LogServer.configurator

        _broadcast_log( "Command received." )

        for command, params in LogServer.parse_commands( rx_bytes ):
            _broadcast_log( f"Processing command: {command}" )

            try:
                if command == LogServer.Commands.RESET:
                    cora.reset_all()
                elif command == LogServer.Commands.ECHO:
                    cora.broadcast_echo( *params )
                elif command == LogServer.Commands.SET_LOGGER_STATE:
                    enabled = bool( int( params[0] ) )

                    if "*" in params[1:]:
                        lx = [x.name for x in utilities.list_loggers()]
                    else:
                        lx = params[1:]

                    for l in lx:
                        cora.set_logger_enabled( l, enabled )
                elif command == LogServer.Commands.SET_THREAD_STATE:
                    enabled = bool( int( params[0] ) )

                    for l in params[1:]:
                        pid, tid = (_strtoi( x ) for x in l.split( ".", 1 ))
                        cora.set_thread_enabled( pid, tid, enabled )
                elif command == LogServer.Commands.SET_PROCESS_STATE:
                    enabled = bool( int( params[0] ) )

                    for l in params[1:]:
                        pid = _strtoi( l )
                        cora.set_process_enabled( pid, enabled )
                elif command == LogServer.Commands.GET_STATE:
                    cora.broadcast_state( *params )
                else:
                    raise ValueError( "No such command." )
            except Exception as ex:
                _broadcast_log( f"Invalid command: {ex}" )
            else:
                _broadcast_log( "Done processing command OK." )

    @staticmethod
    def __remove_signature( data: memoryview, secret: Optional[bytes] ) -> memoryview:
        """
        Strips the signature and returns the actual data.
        The signature is a fixed length salt and a hash of the data, salt and secret.
        If the signature doesn't match, an empty byte string is returned.
        
        :param data:    Data 
        :param secret:  Secret.
                        If `None` no signature is removed.
        :return:        Data without the signature. 
        """
        if not secret:
            return data

        SIG_LEN: int = LogServer.Constants.SIGNATURE_SIZE
        SALT_LEN: int = LogServer.Constants.SECRET_SALT_SIZE
        sig_bytes = data[-SIG_LEN:]
        data_bytes = data[:-SIG_LEN]

        hasher = hashlib.sha256()
        hasher.update( data_bytes )
        hasher.update( secret )
        expected_sig: bytes = hasher.digest()

        print( "" )
        print( f"PACKET IS {data.nbytes} {data.tobytes()}" )
        print( f"DATA IS {data_bytes[:SALT_LEN].nbytes} {data_bytes[:SALT_LEN].tobytes()}" )
        print( f"SALT IS {data_bytes[SALT_LEN:].nbytes} {data_bytes[SALT_LEN:].tobytes()}" )
        print( f"SIG IS {sig_bytes.nbytes} {sig_bytes.tobytes()}" )
        print( f"EXPECTED SIG IS {expected_sig}" )
        print( "" )

        if sig_bytes != expected_sig:
            _broadcast_log( f"Rejected {data.nbytes}-byte configuration due to bad signature: {sig_bytes.tobytes()}" )
            return memoryview( b"" )

        return data_bytes[:-SALT_LEN]


def _strtoi( x: str ):
    """
    String to int. "*" to `_ASTERISK`.
    """
    if x == "*":
        return LogServer.Constants._ASTERISK
    else:
        return int( x )


_broadcast_log = MLog( LogServer.Constants.BROADCAST_LOG_NAME,
                       doc = "Log server messages and config."
                             "A handler to this log is *always* added during `LogServer.start`, "
                             "even if no other logs are enabled (`register` = `False`). "
                             "This ensures the clients know the server is working. "
                             "Clients can disable this log thereafter by updating the config."
                             "Additionally, when using `list` config, "
                             "a handler is always added to the config received." )
LogServer.broadcast_log = _broadcast_log
