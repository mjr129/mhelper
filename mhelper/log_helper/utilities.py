import logging
from typing import List, Optional

# noinspection PyUnresolvedReferences
_lm = logging.Manager


def list_loggers() -> List[logging.Logger]:
    """
    Lists all loggers.
    """
    return [x for x in get_manager().loggerDict.values() if isinstance( x, logging.Logger )]


def get_manager() -> _lm:
    """
    Returns the manager from `logging`.
    """
    manager = getattr( logging.Logger, "manager" )
    return manager


def get_description( logger: logging.Logger ):
    """
    Returns the description of a logger.
    """
    from .wrapper import MLog
    wrapper: Optional[MLog] = MLog.registered.get( logger.name )

    if wrapper:
        return wrapper.doc

    return None
