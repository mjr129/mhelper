from time import sleep

from mhelper.log_helper import MLog


def main():
    logger = MLog( "my_log" )
    logger.attach_as_root()

    while True:
        logger( "This is a log" )
        sleep(1)


if __name__ == "__main__":
    main()
