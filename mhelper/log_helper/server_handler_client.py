"""
A set of functions for dealing with a `LogServer`.
See `LogClient`.
"""
import logging
import logging.config
import logging.handlers
import logging.handlers
import pickle
import socket
import socketserver
import struct
import threading
import hashlib

from typing import Iterable, Literal, Optional, Sequence, Tuple, Union

from . import utilities
from .server_handler import LogServer


__all__ = ["LogClient"]


class LogClient:
    """
    !STATIC
    
    Log client helper.
    Use against an application using `LogServer`.
    
    Usage::
    
        LogClient.send( <config>, ... )
        LogClient.receive( ... )
    """
    TConfig = Union[str, Sequence[str]]


    class BaseHandler:
        def unPickle( self, data ):
            return pickle.loads( data )


        def on_handle( self, record: logging.LogRecord ):
            logger = logging.getLogger( record.name )
            logger.handle( record )


    class UdpRequestHandler( socketserver.DatagramRequestHandler, BaseHandler ):
        socket: socket.SocketIO  # inherited but not annotated
        server: "LogClient.BaseLogReceiver"  # in our case


        def __init__( self, *args, **kwargs ):
            super().__init__( *args, **kwargs )


        def handle( self ):
            try:
                while self.server.is_running:
                    chunk = self.socket.recv( 2048 )
                    chunk = chunk[4:]
                    obj = self.unPickle( chunk )
                    record = logging.makeLogRecord( obj )
                    self.on_handle( record )
            except socket.timeout:
                pass


    class TcpRequestHandler( socketserver.StreamRequestHandler, BaseHandler ):
        connection: socket.SocketIO  # inherited but not annotated


        def handle( self ):
            """
            Handle multiple requests - each expected to be a 4-byte length,
            followed by the LogRecord in pickle format.
            Sends the logs to `self.handler`.
            """
            try:
                while True:
                    chunk = self.connection.recv( 4 )

                    if len( chunk ) < 4:
                        break

                    slen = struct.unpack( '>L', chunk )[0]
                    chunk = self.connection.recv( slen )
                    while len( chunk ) < slen:
                        chunk = chunk + self.connection.recv( slen - len( chunk ) )
                    obj = self.unPickle( chunk )
                    record = logging.makeLogRecord( obj )
                    self.on_handle( record )
            except socket.timeout:
                pass


    class BaseLogReceiver( socketserver.BaseServer ):
        allow_reuse_address = True


        def __init__( self,
                      host = "localhost",
                      port = LogServer.Constants.DEFAULT_PORT,
                      handler: socketserver.BaseServer = None ):
            if handler is None:
                handler = self.default_handler

            super().__init__( (host, port), handler )
            self.is_running = True


        def shutdown( self ) -> None:
            self.is_running = False
            super().shutdown()


        @property
        def default_handler( self ):
            raise NotImplementedError( "abstract" )


    class TcpLogReceiver( BaseLogReceiver, socketserver.ThreadingTCPServer ):
        @property
        def default_handler( self ):
            return LogClient.TcpRequestHandler


    class UdpLogReceiver( BaseLogReceiver, socketserver.ThreadingUDPServer ):
        @property
        def default_handler( self ):
            return LogClient.UdpRequestHandler


    class AsyncReceiver:
        def __init__( self,
                      tcp_server: "LogClient.BaseLogReceiver",
                      thread: threading.Thread ):
            self.tcp_server = tcp_server
            self.thread = thread


        def stop( self ):
            self.tcp_server.shutdown()
            self.thread.join()


    @staticmethod
    def receive( **kwargs ) -> None:
        """
        Starts receiving logs (blocks forever).
        Can be used to receive native as well as MLog logs.
        
        :param kwargs: See `__make_receiver`.
        """
        tcp_server = LogClient.__make_receiver( **kwargs )
        tcp_server.serve_forever()


    @staticmethod
    def receive_async( **kwargs ) -> AsyncReceiver:
        """
        Threaded version of `receive`.
        
        :param kwargs: See `__make_receiver`. 
        :return:       Thread.  
        """
        tcp_server = LogClient.__make_receiver( **kwargs )
        thread = threading.Thread( target = tcp_server.serve_forever,
                                   name = "LogClient.async_receiver",
                                   daemon = True )
        thread.start()

        return LogClient.AsyncReceiver( tcp_server, thread )


    @staticmethod
    def format_commands( source: Sequence[Sequence[str]] ) -> str:
        return LogServer.format_response( source )


    @staticmethod
    def parse_response( source: Union[bytes, str] ) -> Iterable[Tuple[str, Sequence[str]]]:
        return LogServer.parse_commands( source )


    @staticmethod
    def send( data: Union[str, bytes],
              host: str = LogServer.Constants.DEFAULT_HOST,
              port: int = LogServer.Constants.DEFAULT_CONFIG_PORT,
              protocol: str = LogServer.Constants.DEFAULT_PROTOCOL,
              multicast_group: Optional[str] = LogServer.Constants.DEFAULT_CONFIG_MULTICAST_GROUP,
              secret: Union[bytes, bool] = None
              ) -> bytes:
        """
        Sends data to the server.
        
        :param data:            Data to send. 
        :param host:            Target host
        :param port:            Target port
        :param protocol:        Protocol ("tcp" or "udp")
        :param multicast_group: Multicast group.
        :param secret:          Secret, when set adds a signature.
        """
        # Get data bytes
        if isinstance( data, str ):
            tx_bytes = data.encode( LogServer.Constants.ENCODING )
        elif isinstance( data, bytes ):
            tx_bytes = data
        else:
            raise TypeError( "config" )

        # Add signature
        tx_bytes = LogClient.__append_signature( tx_bytes, secret )

        # Add length prefix
        tx_bytes = struct.pack( '>L', len( tx_bytes ) ) + tx_bytes

        # Acquire socket
        if protocol == "udp":
            s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        elif protocol == "tcp":
            s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        else:
            raise ValueError( "Invalid protocol" )

        if multicast_group:
            assert host == "localhost", "Host should be localhost for multicast."
            s.setsockopt( socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 0 )  # i.e. same host
            s.setsockopt( socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1 )
            s.connect( (multicast_group, port) )
        else:
            s.connect( (host, port) )

        # Send data
        s.send( tx_bytes )

        # Close connection
        s.close()
        return tx_bytes

    @staticmethod
    def __append_signature( data: bytes, secret: Union[bool, bytes] ):
        """
        Adds a signature to the `data`.
        The signature is a fixed length salt and a hash of the data, salt and secret.
        
        :param data:        Data 
        :param secret:      Secret.
                            `True` uses `LogServer.load_secret`.
                            `None` applies no signature. 
        :return:            Data with signature.    
        """
        if secret is None or secret is False:
            return data
        elif secret is True:
            secret = LogServer.load_secret()

        import secrets
        salt = secrets.token_bytes( LogServer.Constants.SECRET_SALT_SIZE )
        salted_data = data + salt
        signature = hashlib.sha256( salted_data + secret ).digest()
        signed_data = salted_data + signature
        
        return signed_data


    @staticmethod
    def __make_receiver( *, protocol: str = "udp", **kwargs ) -> "LogClient.BaseLogReceiver":
        for logger in utilities.list_loggers():
            logger.handlers.clear()

        if protocol == "tcp":
            return LogClient.TcpLogReceiver( **kwargs )
        elif protocol == "udp":
            return LogClient.UdpLogReceiver( **kwargs )
        else:
            raise ValueError( "Invalid protocol." )
