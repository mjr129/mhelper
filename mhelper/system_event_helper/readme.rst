========================================================================================================================
System Event Helper
========================================================================================================================

A set of **system-wide** inter-process-communication primitives that work on both Windows and Unix.

* `SystemEvent`
* `SystemSemaphore`
* `SystemMutex`

No magic. No duck typing. PEP484-annotated. PEP257-documented.

Usage
-----

Import, then call the `create` method to create an instance of the required
class::

    from mhelper.system_event_helper import SystemEvent

    my_event = SystemEvent.create( "my_event" )

This creates a `SystemEvent` which is a `WindowsEvent` on Windows and a
`UnixEvent` on Unix.

After construction, the objects behave like exactly their `threading`
counterparts.

(Note that ``my_event = SystemEvent()`` is not permitted and will cause an
error. While prevalent in some libraries this syntax requires magic to construct
a concrete type from an abstract class. This can break type checkers and be
confusing to anyone else reading the code, so has been avoided.).

------------------------------------------------------------------------------------------------------------------------
Windows notes
------------------------------------------------------------------------------------------------------------------------

On Windows, the three classes are explicitly defined in the API, and here we use the correct one,
so `SystemMutex` uses a Windows Mutex and `SystemEvent` uses a Windows Event, etc.

The primitives are managed by Windows, and are cleaned up when they are no longer in use, even if
the applications using them die unexpectedly. 


------------------------------------------------------------------------------------------------------------------------
Unix notes
------------------------------------------------------------------------------------------------------------------------

On Unix, there is only a Semaphore in the API. The Unix-Event and Unix-Mutex are thus derived from the Unix-Semaphore,
using the method described in the `SystemEvent` package by `rwarren` [1].

Of bigger concern is that the Unix-Semaphore is created in shared memory which is left to the programmer to manage, not
the OS. This means that if the process dies with the semaphore open, the semaphore will not be closed until the system
is restarted. A couple of work-arounds have been implemented in light of this:

* The Unix-Mutex actually creates a Unix-Flock.
  This isn't great since it involves polling a file.
  However, since most system-mutexes are non-blocking to prevent application reenterancy, this isn't usually an issue.
  A true Unix-Mutex can be explicitly requested by passing the `NO_FLOCK` flag to the `create` method.
* For the true Unix-Mutex, as well as the Unix-Semaphore and Unix-Event a "register of interests" table (ROIT) is kept
  on disk. This names the processes using the semaphores and is itself protected by an FLOCK.
  Whenever the module is imported, the ROIT is checked, and any unused semaphores are purged.
  Note that the semaphores only get cleaned up when all processes naming them have finished - if one of a set of
  processes *dies* without releasing the semaphore, the semaphore will still be in limbo until all the remaining
  processes naming the semaphore are terminated.
  The ROIT may be manually checked by running::

        python -m mhelper.system_event_helper_check
        
* All in all, the Flock method works well for mutexes, which are usually "opened" for long periods, while the ROIT works
  for Semaphores and Events, which are usually open only briefly. Neither method is perfect however. 
    


Footnotes
~~~~~~~~~

.. [1]  AUTHOR: rwarren
        YEAR: 2018
        NAME: SystemEvent 1.0.1
        URL: https://pypi.org/project/SystemEvent/
        LICENSE: MIT
            
            Nb. LICENSE file missing. MIT License available at https://opensource.org/licenses/MIT::

                Copyright [2018 rwarren]
                
                Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
                
                The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
                
                THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
            
