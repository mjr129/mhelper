"""
Classes for Windows.

Using native Windows events.
"""
import warnings
from typing import Optional

from .bases import EFlags, SystemEvent, SystemMutex, SystemSemaphore


try:
    # noinspection PyPackageRequirements
    import win32api
    # noinspection PyPackageRequirements
    import win32event
except ImportError:
    win32api = None
    win32event = None



class _WindowsEvent( SystemEvent ):
    def __init__( self, name: str, flags: EFlags = EFlags.NONE ):
        super().__init__( name, flags )
        self.__handle = win32event.CreateEvent( None, 0, 0, name )
        # Note that Windows uses a reference counter to clean up the event, that's nice.
    
    
    def is_set( self ):
        return _win32_wait_for_single_object( self.__handle, 0 )
    
    
    def wait( self, timeout: Optional[float] = None ) -> bool:
        return _win32_wait_for_single_object( self.__handle, timeout )
    
    
    def set( self ):
        win32event.SetEvent( self.__handle )
    
    
    def clear( self ):
        win32event.ResetEvent( self.__handle )


class _WindowsSemaphore( SystemSemaphore ):
    def __init__( self, name: str, initial_value: int, flags: EFlags = EFlags.NONE ):
        super().__init__( name, initial_value, flags )
        self.__handle = win32event.CreateSemaphore( None, initial_value, initial_value, name )  # status = win32api.GetLastError()
        assert self.__handle
    
    
    def __repr__( self ):
        return f"{self.__class__.__name__}(handle={self.__handle!r})"
    
    
    def acquire( self, timeout: Optional[float] = None ) -> bool:
        return _win32_wait_for_single_object( self.__handle, timeout )
    
    
    def release( self ):
        win32event.ReleaseSemaphore( self.__handle, 1 )
    
    
    def __del__( self ):
        win32api.CloseHandle( self.__handle )


class _WindowsMutex( SystemMutex ):
    def __init__( self, name: str, flags: EFlags = EFlags.NONE ) -> None:
        super().__init__( name, flags )
        self.__handle = win32event.CreateMutex( None, False, name )
    
    
    def acquire( self, timeout: Optional[float] = None ) -> bool:
        return _win32_wait_for_single_object( self.__handle, timeout )
    
    
    def release( self ):
        win32event.ReleaseMutex( self.__handle )
    
    
    def __del__( self ):
        win32api.CloseHandle( self.__handle )
        
def _win32_wait_for_single_object( handle, timeout = None ) -> bool:
    """
    Manages the call to `win32event.WaitForSingleObject` and handling its
    return value.
    
    :param handle:      Object handle 
    :param timeout:     Timeout. `None` for infinite and 0 for non-blocking. 
    :return:            `True` for acquired, `False` for timed-out. 
    """
    if timeout is None:
        timeout = win32event.INFINITE
    else:
        timeout = int( timeout * 1000 )
    
    r = win32event.WaitForSingleObject( handle, timeout )
    
    if r == win32event.WAIT_OBJECT_0:
        return True
    elif r == win32event.WAIT_TIMEOUT:
        return False
    elif r == win32event.WAIT_ABANDONED:
        warnings.warn( "Message from windows: WAIT_ABANDONED" )
        return True
    elif r == win32event.WAIT_FAILED:
        raise ValueError( "Event error. WAIT_FAILED." )
    else:
        raise ValueError( f"Event error. {r}." )