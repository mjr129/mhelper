"""
Classes for Unix.

Using native UNIX file locks.
"""
from typing import Optional

from .bases import EFlags, SystemMutex


class _UnixFlockMutex( SystemMutex ):
    def __init__( self, name: str, flags: EFlags = EFlags.NONE ):
        super().__init__( name, flags )
        
        from mhelper import specific_locations
        self.__file_name = specific_locations.get_application_file_name( "rusilowicz", "system_events", name + ".mutex" )
        
        self.handle = open( self.__file_name, "w" )
    
    
    def __del__( self ):
        pass
    
    
    def acquire( self, timeout: Optional[float] = None ) -> bool:
        import fcntl
        
        if timeout == 0:
            try:
                fcntl.lockf( self.handle, fcntl.LOCK_EX | fcntl.LOCK_NB )
            except OSError:
                return False
            else:
                return True
        elif timeout is None:
            fcntl.lockf( self.handle, fcntl.LOCK_EX )
            return True
        
        import time
        end = time.perf_counter() + timeout
        
        while time.perf_counter() <= end:
            if self.acquire( 0 ):
                return True
            
            time.sleep( 0.5 )
        
        return False
    
    
    def release( self ):
        import fcntl
        fcntl.lockf( self.handle, fcntl.LOCK_UN )