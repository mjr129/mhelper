import logging as _logging
import os
import sys
from typing import Any, Dict, List

from .bases import EFlags
from .unix_flock import _UnixFlockMutex


try:
    # noinspection PyPackageRequirements
    import posix_ipc
except ImportError:
    posix_ipc: Any = None

_roit_logger = _logging.Logger( "mhelper.system_event.roit" )
_roit_log = _roit_logger.info

if os.getenv( "SYSTEM_EVENT_HELPER_DEBUG" ):
    print( "SYSTEM_EVENT_HELPER_DEBUG" )
    _roit_logger.addHandler( _logging.StreamHandler( sys.stdout ) )


class _UnixRoitTable:
    """
    Makes the ROIT shared memory table.
    """
    
    
    def __init__( self, ):
        from mhelper import specific_locations
        self.file_name = specific_locations.get_application_file_name( "rusilowicz", "system_events", "roit.txt" )
        self.mutex = _UnixFlockMutex( "rusilowicz.system_events.roit.lock", EFlags.FLOCK )
        self.data: Dict[str, List[int]] = { }
        self.is_entered = False
    
    
    def __enter__( self ):
        assert not self.is_entered, "Cannot reenter"
        self.is_entered = True
        _roit_log( "Entering ROIT" )
        
        if not self.mutex.acquire( 5 ):
            raise RuntimeError( "Failed to acquire lock on ROIT." )
        
        if os.path.isfile( self.file_name ):
            with open( self.file_name, "r" ) as fin:
                text = fin.read()
            
            import json
            self.data = json.loads( text )
        else:
            self.data = { }
        
        return self
    
    
    def __exit__( self, exc_type, exc_val, exc_tb ):
        _roit_log( "Leaving ROIT" )
        
        import json
        text = json.dumps( self.data )
        
        with open( self.file_name, "w" ) as fout:
            fout.write( text )
        
        self.mutex.release()
        self.is_entered = False


def _unix_register_interests( semaphore_name: str ) -> None:
    """
    Adds a semaphore to the ROIT, registered against this process.
     
    :param semaphore_name:  Name of the semaphore.
    """
    
    with _UnixRoitTable() as roit:
        _roit_log( f"Adding to ROIT: {semaphore_name} {os.getpid()}" )
        roit.data.setdefault( semaphore_name, [] ).append( os.getpid() )


def _unix_update_interests() -> None:
    """
    Checks the ROIT.
    
    Any semaphores no longer in use are deleted and removed from the ROIT.
    Semaphores are considered no longer in use if they used by no processes, or
    are only used by this process. 
    """
    with _UnixRoitTable() as roit:
        process_ids: List[int]
        to_removes: List[str] = []
        
        for semaphore_name, process_ids in roit.data.items():
            for process_id in list( process_ids ):
                if _unix_check_pid( process_id ):
                    _roit_log( f"RETAIN: {semaphore_name} {process_id}" )
                else:
                    _roit_log( f"REMOVE: {semaphore_name} {process_id}" )
                    process_ids.remove( process_id )
            
            if not process_ids:
                to_removes.append( semaphore_name )
        
        for to_remove in to_removes:
            del roit.data[to_remove]
            
            orig_umask = os.umask( 0 )
            
            try:
                dead_semaphore = posix_ipc.Semaphore( semaphore_name,
                                                      mode = 438,
                                                      flags = 0 )
                dead_semaphore.unlink()
                _roit_log( f"DELETE-1: {semaphore_name}" )
            except posix_ipc.ExistentialError:
                _roit_log( f"DELETE-0: {semaphore_name}" )
            finally:
                os.umask( orig_umask )


def _unix_check_pid( pid: int ) -> bool:
    """
    Checks if the process with the associated PID still exists.
    
    :param pid: Process ID. 
    :return:    Process exists. 
    """
    try:
        os.kill( pid, 0 )
    except OSError:
        return False
    else:
        return True

