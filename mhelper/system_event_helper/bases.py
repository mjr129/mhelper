"""
Base classes.

    SystemEvent
    SystemMutex
    SystemSemaphore
    
All behave like their threading counterparts, but are system-wide.

The classes can be constructed using, e.g.::

    my_event = SystemEvent.create( args )
    
See the readme for more details.
"""
import sys
from enum import Enum, Flag
from typing import Optional, Type, TypeVar

_T = TypeVar( "_T" )


class EFlags( Flag ):
    """
    :cvar NONE:         None/default flags
    :cvar FLOCK:        On Unix, use file lock instead of Semaphore.
                        Ignored on Windows.
    :cvar NO_FLOCK:     On Unix, use Semaphore instead of file lock.
                        Ignored on Windows.
    """
    NONE = 0
    FLOCK = 0
    NO_FLOCK = 1


class SystemEvent:
    """
    A system wide version of `threading.Event`.
     
    All methods match `threading.Event` and are not re-documented here.
    """

    def __init__( self,
                  name: str,
                  flags: EFlags = EFlags.NONE
                  ) -> None:
        _ = flags
        self.__name = name

    @staticmethod
    def create( name: str, flags: EFlags = EFlags.NONE ):
        klass: Type[SystemEvent]

        if _g_system == _ESystem.WINDOWS:
            from .windows import _WindowsEvent as klass
        elif _g_system == _ESystem.UNIX:
            from .unix import _UnixEvent as klass
        else:
            raise RuntimeError( "Bad switch." )

        r: SystemEvent = klass.__new__( klass )
        r.__init__( name, flags )
        return r

    def __repr__( self ):
        return f"{self.__class__.__name__}(mutex_name={self.__name!r})"

    def is_set( self ) -> bool:
        raise NotImplementedError( "abstract" )

    def set( self ) -> None:
        raise NotImplementedError( "abstract" )

    def clear( self ) -> None:
        raise NotImplementedError( "abstract" )

    def wait( self, timeout: Optional[float] = None ) -> bool:
        raise NotImplementedError( "abstract" )


class SystemSemaphore:
    """
    A system-wide version of `threading.Semaphore`.
    
    All methods match `threading.Semaphore` and are not re-documented here.
    """

    def __init__( self,
                  name: str,
                  initial_value: int,
                  flags: EFlags = EFlags.NONE ):
        _ = initial_value
        _ = flags
        self.__name = name

    @staticmethod
    def create( name: str,
                initial_value: int,
                flags: EFlags = EFlags.NONE ):
        klass: Type[SystemSemaphore]

        if _g_system == _ESystem.WINDOWS:
            from .windows import _WindowsSemaphore as klass
        elif _g_system == _ESystem.UNIX:
            from .unix import _UnixSemaphore as klass
        else:
            raise RuntimeError( "Bad switch." )

        r: SystemSemaphore = klass.__new__( klass )
        r.__init__( name, initial_value, flags )
        return r

    def __repr__( self ):
        return f"{self.__class__.__name__}(mutex_name={self.__name!r})"

    def acquire( self, timeout: Optional[float] = None ) -> bool:  # true = ok, false = timeout
        raise NotImplementedError( "abstract" )

    def release( self ) -> None:
        raise NotImplementedError( "abstract" )

    def __enter__( self ):
        if not self.acquire( None ):
            raise RuntimeError( "expected to obtain" )

    def __exit__( self, exc_type, exc_val, exc_tb ):
        self.release()


class SystemMutex:
    """
    A system-wide Mutex.
    This is essentially a Semaphore that only permits a single caller to enter
    the protected region.
    
    All methods match `threading.Semaphore` and are not re-documented here.
    """

    def __init__( self, name, flags: EFlags = EFlags.NONE ):
        _ = flags
        self.__name = name

    @staticmethod
    def create( name, flags: EFlags = EFlags.NONE ) -> "SystemMutex":
        klass: Type[SystemMutex]

        if _g_system == _ESystem.WINDOWS:
            from .windows import _WindowsMutex as klass
        elif _g_system == _ESystem.UNIX:
            if EFlags.NO_FLOCK in flags:
                from .unix import _UnixMutex as klass
            else:
                from .unix_flock import _UnixFlockMutex as klass
        else:
            raise RuntimeError( "Bad switch." )

        r: SystemMutex = klass.__new__( klass )
        r.__init__( name, flags )
        return r

    def __repr__( self ):
        return f"{self.__class__.__name__}({self.__name!r})"

    def acquire( self, timeout: Optional[float] = None ) -> bool:  # true = ok, false = timeout
        raise NotImplementedError( "abstract" )

    def release( self ):
        raise NotImplementedError( "abstract" )

    def __enter__( self ):
        o = self.acquire( None )
        assert o

    def __exit__( self, exc_type, exc_val, exc_tb ):
        self.release()


class _ESystem( Enum ):
    WINDOWS = 0
    UNIX = 1


def __get_api() -> _ESystem:
    try:
        # noinspection PyPackageRequirements
        import win32api
        return _ESystem.WINDOWS
    except ImportError:
        pass

    try:
        import posix_ipc
        return _ESystem.UNIX
    except ImportError:
        pass

    raise RuntimeError( f"\n"
                        f"*******************************************************************************\n"
                        f"* Missing packages                                                            *\n"
                        f"*******************************************************************************\n"
                        f"* Either the `pywin32` (Windows) or the `posix_ipc` (UNIX) Python package     *\n"
                        f"* is required but neither of these are installed.                             *\n"
                        f"*                                                                             *\n"
                        f"* Please install the appropriate service for your platform, e.g.              *\n"
                        f"*                                                                             *\n"
                        f"*    {sys.executable} -m pip install posix_ipc\n"
                        f"*                                                                             *\n"
                        f"* Note that posix_ipc requires the development binaries to install, e.g.      *\n"
                        f"*                                                                             *\n"
                        f"*    sudo apt-get install python{sys.version_info[0]}.{sys.version_info[1]}-dev\n"
                        f"*                                                                             *\n"
                        f"*******************************************************************************\n" )


def __init():
    if _g_system == _ESystem.UNIX:
        import os
        from .unix_roit import _unix_update_interests

        if os.getenv( "NO_UPDATE_ROIT" ):
            return

        _unix_update_interests()


_g_system: _ESystem = __get_api()
__init()
