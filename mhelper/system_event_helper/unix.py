"""
Classes for Unix.

Using native UNIX semaphores.
"""
import os
from typing import Any, Optional

from .bases import EFlags, SystemEvent, SystemMutex, SystemSemaphore
from .unix_roit import _unix_register_interests 

try:
    # noinspection PyPackageRequirements
    import posix_ipc
except ImportError:
    posix_ipc: Any = None


class _UnixEvent( SystemEvent ):
    def __init__( self, name, flags: EFlags = EFlags.NONE ):
        super().__init__( name, flags )
        self.__semaphore = None
        self.__semaphore = _unix_make_semaphore( name, 0 )
    
    
    def __del__( self ):
        if self.__semaphore:
            self.__semaphore.close()
    
    
    def set( self ):
        self.__semaphore.release()
    
    
    def clear( self ):
        while True:
            try:
                self.__semaphore.acquire( 0 )
            except posix_ipc.BusyError:
                break
    
    
    def wait( self, timeout: Optional[float] = None ) -> bool:
        try:
            self.__semaphore.acquire( timeout )
        except posix_ipc.BusyError:
            return False
        
        self.__semaphore.release()
        return True
    
    
    def is_set( self ):
        return self.wait( 0 )


class _UnixSemaphore( SystemSemaphore ):
    def __init__( self, semaphore_name: str, initial_value: int, flags: EFlags = EFlags.NONE ):
        super().__init__( semaphore_name, initial_value, flags )
        self.__semaphore = None
        self.__semaphore = _unix_make_semaphore( semaphore_name, initial_value )
    
    
    def __del__( self ):
        if self.__semaphore:
            self.__semaphore.close()
    
    
    def acquire( self, timeout: Optional[float] = None ) -> bool:
        try:
            self.__semaphore.acquire( timeout )
            return True
        except posix_ipc.BusyError:
            return False
    
    
    def release( self ) -> None:
        self.__semaphore.release()


class _UnixMutex( SystemMutex ):
    def __init__( self, name: str, flags: EFlags = EFlags.NONE ):
        super().__init__( name, flags )
        self.__semaphore = None
        self.__semaphore = _unix_make_semaphore( name, 1 )
    
    
    def __del__( self ):
        if self.__semaphore:
            self.__semaphore.close()
    
    
    def acquire( self, timeout: Optional[float] = None ) -> bool:
        try:
            self.__semaphore.acquire( timeout )
            return True
        except posix_ipc.BusyError:
            return False
    
    
    def release( self ) -> None:
        self.__semaphore.release()


def _unix_make_semaphore( semaphore_name: str, initial_value: int ):
    """
    Creates a `posix_ipc.Semaphore` and registers it in the ROIT.
    
    :param semaphore_name:  Name of the semaphore 
    :param initial_value:   Initial count. 
    :return: The semaphore.
    """
    assert semaphore_name
    assert isinstance( semaphore_name, str )
    assert isinstance( initial_value, int )
    _unix_register_interests( semaphore_name )
    
    orig_umask = os.umask( 0 )
    
    try:
        return posix_ipc.Semaphore( semaphore_name, mode = 438, flags = posix_ipc.O_CREAT, initial_value = initial_value )
    finally:
        os.umask( orig_umask )
