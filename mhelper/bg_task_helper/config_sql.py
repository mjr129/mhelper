import re
from typing import Callable, Iterable, Optional, Union

from mhelper.bg_task_helper import TaskFilter, TaskQuery, TTaskData, TTaskId
from mhelper.sql_helper import now_sql, Remote, MySQLdb
from mhelper.exception_helper import safe_cast
from mhelper import string_helper

from .config import BgtsConfig
from .data import TaskInsert

__all__ = "MysqlBasedBgtsConfig", "Remote"

_TASK_IDENTIFIER_FORMAT_ = "ZZZZ-ZZZZ"
_TASK_IDENTIFIER_FORMAT = re.compile( "^[0-9A-Z]{4}-[0-9A-Z]{4}$" )


class C:
    table_name = "mhsh_task"
    accession = "accession"
    data = "data"
    user_id = "user_id"
    queued = "queued"
    run = "run"
    lost = "lost"
    completed = "completed"
    result = "result"

    cfg_table_name = "mhsh_task_config"
    value = "value"
    key = "key"


class MysqlBasedBgtsConfig( BgtsConfig ):
    """
    `BgtsConfig` that uses a MySql database to store and retrieve the task
    information.
    
    Features:
     
    * FIFO  - Supported
    * Users - Supported
    """


    def __init__( self,
                  queue_name: str,
                  remote: Union[Remote, Callable[[], Remote]],
                  **kwargs
                  ) -> None:
        super().__init__( queue_name, **kwargs )
        self.__remote = remote
        self.__actual_remote: Optional[Remote] = None


    @property
    def remote( self ):
        if self.__actual_remote is None:
            if isinstance( self.__remote, Remote ):
                self.__actual_remote = self.__remote
            elif callable( self.__remote ):
                self.__actual_remote = self.__remote()

            safe_cast( "remote", self.__actual_remote, Remote )

        return self.__actual_remote


    def initialise( self ) -> None:
        pass


    def __validate_task_id( self, task_id: str ):
        if not isinstance( task_id, str ):
            raise ValueError( f"`{type( task_id ).__name__}` is not a valid `task_id`." )

        if not _TASK_IDENTIFIER_FORMAT.match( task_id ):
            raise ValueError( "Invalid `task_id` format." )


    def query_task( self, task_id: str ) -> Optional[TaskQuery]:
        self.__validate_task_id( task_id )

        SQL = f"""
            SELECT {C.data}, {C.queued}, {C.run}, {C.completed}, {C.lost}, {C.result}, {C.user_id}
            FROM {C.table_name}
            WHERE {C.accession} = %s
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, (task_id,) )

                row = csr.fetchone()

                if row is None:
                    return None

                row_ = iter( row )
                data = next( row_ )
                queued = next( row_ )
                run = next( row_ )
                completed = next( row_ )
                lost = next( row_ )
                result = next( row_ )
                user_id = next( row_ )

                return TaskQuery( task_id = task_id,
                                  task_data = data,
                                  queued_time = queued,
                                  started_time = run,
                                  lost_time = lost,
                                  completed_time = completed,
                                  result_data = result,
                                  user_id = user_id )


    def list_tasks( self, filter: TaskFilter ) -> Optional[Iterable[TTaskId]]:
        if filter.user_id:
            if filter.include_completed:
                SQL = f"""
                    SELECT {C.accession}
                    FROM {C.table_name}
                    WHERE {C.user_id} = %s
                """
                a = (filter.user_id,)
            else:
                SQL = f"""
                    SELECT {C.accession}
                    FROM {C.table_name}
                    WHERE {C.user_id} = %s
                    AND {C.completed} IS NULL AND {C.lost} IS NULL
                """
                a = (filter.user_id,)
        else:
            if filter.include_completed:
                SQL = f"""
                    SELECT {C.accession}
                    FROM {C.table_name}
                """
                a = ()
            else:
                SQL = f"""
                    SELECT {C.accession}
                    FROM {C.table_name}
                    WHERE {C.completed} IS NULL AND {C.lost} IS NULL
                """
                a = ()

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, a )

                for row in csr:
                    yield next( iter( row ) )


    def acquire_task( self ) -> Optional[TTaskId]:
        SQL = f"""
        SELECT `{C.accession}`
        INTO @pk
        FROM `{C.table_name}`
        WHERE {C.run} IS NULL
        AND {C.lost} IS NULL
        ORDER BY `queued` ASC 
        LIMIT 1;

        UPDATE {C.table_name}
        SET {C.run} = %s
        WHERE {C.accession} = @pk;

        SELECT @pk;
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, (now_sql(),) )

                csr.nextset()
                csr.nextset()
                row = csr.fetchone()

                if row is None:
                    id_ = None
                else:
                    id_ = next( iter( row ) )

            con.commit()

        return id_


    def write_task_data( self, request: TaskInsert ) -> TTaskId:
        SQL = f"""
        INSERT INTO `{C.table_name}`
            ({C.accession}, {C.data}, {C.user_id}, {C.queued})
        VALUES
            (%s, %s, %s, %s);
        """

        task_id = string_helper.get_random_text( _TASK_IDENTIFIER_FORMAT_ )

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                try:
                    csr.execute( SQL, (task_id, request.task_data, request.user_id, now_sql()) )
                except MySQLdb.ProgrammingError as ex:
                    # Probably the database or table doesn't exist
                    raise RuntimeError( "Error adding task to database. Check inner exception for details. Please check the BGTS has been started." ) from ex

            con.commit()

        return task_id


    def read_task_data( self, task_id: TTaskId ) -> TTaskData:
        self.__validate_task_id( task_id )

        SQL = f"""
        SELECT {C.data}
        FROM {C.table_name}
        WHERE {C.accession} = %s
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, (task_id,) )

                row = csr.fetchone()

                if row is None:
                    raise RuntimeError( f"Cannot read the data for task ID {task_id!r}:\n{SQL}" )

                return next( iter( row ) )


    def write_task_result( self, task_id: TTaskId, result: TTaskData ) -> None:
        self.__validate_task_id( task_id )

        SQL = f"""
        UPDATE {C.table_name}
        SET {C.completed} = %s, {C.result} = %s
        WHERE {C.accession} = %s
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, (now_sql(), result, task_id) )

            con.commit()


    def get_stop_flag( self ) -> bool:
        SQL = f"""
        SELECT `{C.value}`
        FROM `{C.cfg_table_name}`
        WHERE `{C.key}` = "STOP"
        """
        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL )

                row = csr.fetchone()

                if row is None:
                    return False

                return bool( int( next( iter( row ) ) ) )


    def set_stop_flag( self, value: bool ) -> None:
        SQL = f"""
        REPLACE INTO `{C.cfg_table_name}`
            ( `{C.key}`, `{C.value}` )
        VALUES
            ( "STOP", %s )
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL, "1" if value else "0" )

            con.commit()


    def recover_tasks( self ) -> None:
        CHECK_TABLES_EXISTS = f"""
        SHOW TABLES LIKE '{C.table_name}'
        """

        CREATE_TABLES = f"""
        CREATE TABLE `{C.table_name}` (
          `{C.accession}` CHAR(16) NOT NULL,
          `{C.data}` BLOB NOT NULL,
          `{C.user_id}` int DEFAULT NULL,
          `{C.queued}` datetime NOT NULL,
          `{C.run}` datetime DEFAULT NULL,
          `{C.completed}` datetime DEFAULT NULL,
          `{C.lost}` datetime DEFAULT NULL,
          `{C.result}` BLOB DEFAULT NULL,
          PRIMARY KEY (`{C.accession}`),
          UNIQUE KEY `{C.accession}` (`{C.accession}`),
          KEY `ix_task_user_id` (`{C.user_id}`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
        
        CREATE TABLE `{C.cfg_table_name}` (
            `{C.key}` CHAR(8) NOT NULL,
            `{C.value}` CHAR(8) NOT NULL,
             PRIMARY KEY (`{C.key}`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
        """

        FLAG_LOST_TASKS = f"""
        UPDATE `{C.table_name}`
        SET `{C.lost}` = %s
        WHERE `{C.completed}` IS NULL
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( CHECK_TABLES_EXISTS )

                row = csr.fetchone()

                if row is None:
                    csr.execute( CREATE_TABLES )

                csr.execute( FLAG_LOST_TASKS, (now_sql(),) )

            con.commit()


    def drop_tables( self ):
        SQL = f"""
        DROP TABLE `{C.table_name}`;
        DROP TABLE `{C.cfg_table_name}`;
        """

        with self.remote.connect() as con:
            with con.cursor() as csr:
                csr: MySQLdb.cursors.Cursor

                csr.execute( SQL )

            con.commit()
