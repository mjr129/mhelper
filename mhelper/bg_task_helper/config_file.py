import os
import re
from datetime import datetime
from typing import Dict, Iterable, Optional, Tuple, Union
from time import sleep
from mhelper import file_helper, io_helper, string_helper

from .config import BgtsConfig
from .data import EStatus, TaskFilter, TaskQuery, TTaskData, TTaskId, TaskInsert

__all__="FileBasedBgtsConfig",

class FileBasedBgtsConfig( BgtsConfig ):
    """
    A basic implementation of `BgtsConfig` that sequesters the file system to
    perform its tasks.
    
    Features:
    
    * FIFO  - Not supported
    * Users - Not supported 
    """


    def __init__( self,
                  queue_name: str = "",
                  working_directory: Union[str, Tuple[str, ...]] = ("rusilowicz", "bg_task_helper", "(queue_name)"),
                  **kwargs ):
        """
        
        :param queue_name:         !INHERITED
        :param working_directory:  Where to store the files.
        :param kwargs:             !INHERITED
        """
        if isinstance( working_directory, tuple ) or isinstance( working_directory, list ):
            working_directory = file_helper.get_application_directory( *working_directory, mkdir = False )

        if "(queue_name)" in working_directory:
            assert queue_name, "Either a queue name or a working directory is required."
            working_directory = working_directory.replace( "(queue_name)", queue_name )

        if not queue_name:
            queue_name = file_helper.safe_file_name( working_directory )

        super().__init__( queue_name, **kwargs )

        self.stop_flag = os.path.join( working_directory, "stop.flag.txt" )
        self.pending_folder = os.path.join( working_directory, "pending" )
        self.running_folder = os.path.join( working_directory, "running" )
        self.completed_folder = os.path.join( working_directory, "completed" )
        self.results_folder = os.path.join( working_directory, "results" )
        self.lost_folder = os.path.join( working_directory, "lost" )
        self.meta_data_folder = os.path.join( working_directory, "meta_data" )

        os.makedirs( self.pending_folder, exist_ok = True )
        os.makedirs( self.running_folder, exist_ok = True )
        os.makedirs( self.completed_folder, exist_ok = True )
        os.makedirs( self.results_folder, exist_ok = True )
        os.makedirs( self.meta_data_folder, exist_ok = True )
        os.makedirs( self.lost_folder, exist_ok = True )


    def list_tasks( self, filter: TaskFilter ) -> Iterable[TTaskId]:
        if filter.include_completed:
            dirs = (self.pending_folder, self.running_folder, self.completed_folder)
        else:
            dirs = (self.pending_folder, self.running_folder)

        for directory in dirs:
            for file in file_helper.list_dir( directory, ".task.pkl" ):
                file = file_helper.get_file_name( file )
                file = file[:-len( ".task.pkl" )]
                yield file


    def is_valid( self, task_id: TTaskId ) -> bool:
        if isinstance( task_id, str ):
            return bool( re.match( r"^[A-Z][A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9]-[A-Z0-9][A-Z0-9][A-Z]$", task_id ) )
        else:
            return False


    def query_task( self, task_id: TTaskId ) -> Optional[TaskQuery]:
        task_id = str( task_id )

        if not self.is_valid( task_id ):
            return None

        fn_los = self.__get_lost_fn( task_id )
        fn_pen = self.__get_pending_fn( task_id )
        fn_run = self.__get_running_fn( task_id )
        fn_com = self.__get_completed_fn( task_id )
        fn_res = self.__get_results_fn( task_id )
        data = self.get_meta_data( task_id )

        pending: Optional[float] = data.get( EStatus.PENDING.name, None )
        running: Optional[float] = data.get( EStatus.RUNNING.name, None )
        completed: Optional[float] = data.get( EStatus.COMPLETED.name, None )

        if os.path.isfile( fn_los ):
            # Lost
            task_data = lambda: io_helper.read_all_bytes( fn_los )
            result_data = None
            pending = None
            running = None
            completed = None
        elif os.path.isfile( fn_pen ):
            # Pending
            task_data = lambda: io_helper.read_all_bytes( fn_pen )
            result_data = None
            running = None
            completed = None
        elif os.path.isfile( fn_run ):
            # Running
            task_data = lambda: io_helper.read_all_bytes( fn_run )
            result_data = None
            completed = None
        elif os.path.isfile( fn_com ):
            # Completed
            task_data = lambda: io_helper.read_all_bytes( fn_com )
            result_data = lambda: io_helper.read_all_bytes( fn_res )
        else:
            return None

        pending = datetime.fromtimestamp( pending ) if pending else None
        running = datetime.fromtimestamp( running ) if running else None
        completed = datetime.fromtimestamp( completed ) if completed else None

        return TaskQuery( task_id = task_id,
                          task_data = task_data,
                          queued_time = pending,
                          started_time = running,
                          completed_time = completed,
                          result_data = result_data,
                          lost_time = None,
                          user_id = None )


    def get_meta_data( self, task_id: TTaskId ) -> Dict[str, object]:
        fn = self.__get_meta_data_fn( task_id )
        return self.load_w_retry( fn, lambda: io_helper.load_json_data( fn, { } ) )


    def load_w_retry( self, fn, a ):
        # FILE READ - TRY 3 TIMES
        retry = 0

        while True:
            try:
                return a()
            except PermissionError as ex:
                retry += 1
                if retry >= 3:
                    raise RuntimeError( f"Cannot load data from '{fn}' after {retry} tries." ) from ex
                sleep( 0.1 * retry )


    def set_meta_data( self, task_id: TTaskId, property: EStatus, value ):
        fn = self.__get_meta_data_fn( task_id )
        data = io_helper.load_json_data( fn, { } )
        data[property.name] = value
        io_helper.save_json_data( fn, data )


    def __get_meta_data_fn( self, task_id: TTaskId ) -> str:
        fn = os.path.join( self.meta_data_folder, f"{task_id}.metadata.json" )
        return fn


    def __get_lost_fn( self, task_id: TTaskId ) -> str:
        return os.path.join( self.lost_folder, f"{task_id}.task.pkl" )


    def __get_pending_fn( self, task_id: TTaskId ) -> str:
        return os.path.join( self.pending_folder, f"{task_id}.task.pkl" )


    def __get_running_fn( self, task_id: TTaskId ) -> str:
        dest = os.path.join( self.running_folder, f"{task_id}.task.pkl" )
        return dest


    def __get_completed_fn( self, task_id: TTaskId ) -> str:
        return os.path.join( self.completed_folder, f"{task_id}.task.pkl" )


    def __get_results_fn( self, task_id: TTaskId ) -> str:
        return os.path.join( self.results_folder, f"{task_id}.results.pkl" )


    def acquire_task( self ) -> Optional[TTaskId]:
        pending = file_helper.list_dir( self.pending_folder, ".task.pkl" )

        if not pending:
            return None

        source = pending[0]

        task_id = file_helper.get_file_name( source )
        task_id = task_id.split( ".", 1 )[0]
        assert self.is_valid( task_id )

        dest = self.__get_running_fn( task_id )
        os.rename( source, dest )
        self.set_meta_data( task_id, EStatus.RUNNING, datetime.utcnow().timestamp() )
        self.log( "FILE: Task moved to RUNNING: {}", task_id )
        return task_id


    def write_task_data( self, request: TaskInsert ) -> TTaskId:
        task_id = string_helper.get_random_text( "AZZ-ZZ-ZZA" )
        dest = self.__get_pending_fn( task_id )
        assert not os.path.isfile( dest )

        with io_helper.write_intermediate( dest, "wb" ) as fout:
            fout.write( request.task_data )

        self.log( "FILE: Task created in PENDING: {}", task_id )
        self.set_meta_data( task_id, EStatus.PENDING, datetime.utcnow().timestamp() )

        return task_id


    def read_task_data( self, task_id: TTaskId ) -> TTaskData:
        task_id = str( task_id )
        self.log( "FILE: Reading task: {}", task_id )
        source = self.__get_running_fn( task_id )
        text = self.load_w_retry( source, lambda: io_helper.read_all_bytes( source ) )
        return text


    def write_task_result( self, task_id: TTaskId, result: TTaskData ):
        source = self.__get_running_fn( task_id )
        dest = self.__get_completed_fn( task_id )
        r_dest = self.__get_results_fn( task_id )
        os.rename( source, dest )
        self.set_meta_data( task_id, EStatus.COMPLETED, datetime.utcnow().timestamp() )
        self.log( "FILE: Task moved to COMPLETED: {}", task_id )
        io_helper.write_all_bytes( r_dest, result )


    def set_stop_flag( self, value: bool ):
        if value:
            if not os.path.isfile( self.stop_flag ):
                io_helper.write_all_text( self.stop_flag, "1" )
        else:
            if os.path.isfile( self.stop_flag ):
                os.remove( self.stop_flag )


    def get_stop_flag( self ):
        return os.path.isfile( self.stop_flag )


    def recover_tasks( self ):
        for source in file_helper.list_dir( self.running_folder ):
            task_id = file_helper.get_file_name( source ).split( ".", 1 )[0]
            dest = self.__get_lost_fn( task_id )
            os.rename( source, dest )
            self.log( f"FILE: Task recovered to LOST: {task_id}" )
