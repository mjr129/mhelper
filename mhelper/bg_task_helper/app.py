"""
Defines the API of the BGTS.
See the `Bgts` class.
"""
from typing import Callable, Dict, Iterable, List, Optional, Tuple, Union

from mhelper.dispatch_helper import ThreadedDispatcher

from ._dispatcher import _BgtsDispatcher, _run_task_sync
from .config import BgtsConfig
from .data import Request, TaskFilter, TaskInsert, TaskQuery, TTaskId, TTaskData
from .requisite import _DispatcherRequired, ERequire
import os

__all__ = "Bgts",


class Bgts:
    """
    This defines the API of the BGTS.
    
    This is a *minimal* set of methods for a background task service.
    
    An instance of this class should be constructed, which can be then used
    either client side or server side. ::
    
        myBgts = Bgts( ... )
        
    
    For instance, to submit a task to the BGTS:
    
        myBgts.submit_task( ... )
        
    To start the BGTS:
    
        myBgts.start_dispatcher() 
    
    .. note::
    
        The environment variable `MJR_BGTS_RUN_SYNC` causes tasks to be performed
        client-side and can be used for debugging. Its value should be "1" or "0".
    """
    InUseError = ThreadedDispatcher.InUseError


    class NotFoundError( Exception ):
        pass


    def __init__( self, config: BgtsConfig ):
        """
        Construct a `Bgts` instance.
         
        :param config:  `BgtsConfig`-derived class. 
                        `FileBasedBgtsConfig` is provided as an example.
        """
        self.__config = config
        self.__notifier = ThreadedDispatcher.Notifier( self.__config.get_queue_name() )


    def __repr__( self ):
        return f"{type( self ).__name__}({self.__config!r})"


    def start_dispatcher( self, asynchronous: bool = False ):
        """
        Runs the dispatch server's main loop.
        
        Typically called in the server's entry point.
        
        :param asynchronous:                        When `True` the service runs in a background thread.
        :exception ThreadedDispatcher.InUseError:   The dispatcher is already running.
        """
        try:
            dispatcher = ThreadedDispatcher( handler = _BgtsDispatcher( self, self.__config ),
                                             notifier = self.__notifier,
                                             max_num_threads = self.__config.get_max_num_threads(),
                                             poll_frequency = self.__config.get_poll_frequency() )

            if asynchronous:
                dispatcher.run_dispatcher_async()
            else:
                dispatcher.run_dispatcher()
        except Exception as ex:
            raise RuntimeError( "The dispatcher encountered a critical error and has been terminated." ) from ex


    def log( self, *args, **kwargs ) -> None:
        """
        Logs a message BGTS message.
        
        Called either client- or server-side.
        
        :param args:   Arguments are as for `logging.logger`. 
        """
        self.__config.log( *args, **kwargs )


    def stop_dispatcher_no_wait( self ) -> None:
        """
        Sets the stop flag and pings the dispatcher.
        
        Typically called client-side.
        """
        self.__config.set_stop_flag( True )
        self.__notifier.notify()


    def stop_dispatcher( self ) -> None:
        """
        Sets the stop flag, pings the dispatcher and waits for it to stop.
        
        Typically called client-side.
        """
        self.stop_dispatcher_no_wait()

        while self.__notifier.query_mutex():
            pass


    def query_task( self, task_id: TTaskId ) -> TaskQuery:
        """
        Retrieves information about a task.
        
        Typically called client-side.
        
        :param task_id: Task to query. 
        :return:        Object describing the task.
        """
        return self.__config.query_task( task_id )


    def list_tasks( self, filter: TaskFilter ) -> Optional[Iterable[TTaskId]]:
        """
        Lists the task IDs.
        
        If the dispatcher does not support task listing, the result will be
        `None`.
        
        Typically called client-side.
        """
        return self.__config.list_tasks( filter )


    def query_dispatcher( self ) -> bool:
        """
        Returns if the dispatcher is running.
        
        This checks the dispatcher mutex, however if the mutex has been
        abandoned and not recovered (something that only Unix lets happen!)
        then this method will also return `True`. `query_dispatcher_responding`
        can be a better check in that it checks for a *response* from the
        dispatcher. 
        
        Typically called client-side.
        """
        return self.__notifier.query_mutex()


    def query_dispatcher_responding( self ) -> bool:
        """
        Returns if the dispatcher is running *and* responding.
        
        This call will block until the dispatcher responds.
        If the system is very busy and cannot respond within the timeout period,
        `False` will be returned. `query_dispatcher` offers a non-blocking
        alternative.
        
        Typically called client-side.   
        """
        return self.__notifier.notify( 3 )


    def submit_task( self,
                     function: Callable = None,
                     args: Union[List, Tuple] = None,
                     task_name: str = None,
                     user: Optional[object] = None,
                     meta_data: Optional[Dict[str, object]] = None ) -> object:
        """
        Creates a task record and pings the dispatcher.
        
        Typically called client-side.
                            
        :param function:    Callable to apply to the task.
        
                            This will be executed when the task is run.
                              
        :param args:        Arguments to apply to the task.
        
                            These will be passed to the `function`.
                            
        :param task_name:   Name of the task, for reference purposes.
        
                            This is usually visible to the user.
                            
        :param user:        `BgtsConfig`-meta-data.
                            
                            This is passed directly  to the `BgtsConfig` and its
                            use - if any - depends on the config. For instance
                            `MysqlBasedBgtsConfig` makes this value available
                            as a queryable database column - the user than
                            submitted the task.
                            
        :param meta_data:   Task meta-data.
        
                            Unlike `user` this isn't (directly) visible to the
                            `BgtsConfig` as it is stored in the task-data BLOB.
                            
                            This may containing application-specific
                            information. For instance
                            `hatmul.screens.LongTaskScreen` uses it to store the
                            original POST request.
        
        :return:            Task ID.
        """
        assert callable( function ), "`function` must be callable."
        assert isinstance( args, tuple ) or isinstance( args, list ), "`args` must be a list or tuple."
        data_str: TTaskData = Request.create( task_name or function.__name__,
                                              function,
                                              tuple( args ),
                                              meta_data or { } )

        # Test the data can be recovered now
        assert Request.parse( data_str ) is not None

        # Write the data for the task and get the task ID
        run_sync: bool = _get_run_sync()
        task_id: TTaskId = self.__config.write_task_data( TaskInsert( data_str, user ) )

        if run_sync:
            self.__config.log( "Synchronous debugging of new task, {}", task_id )
            _run_task_sync( self.__config, task_id )
        else:
            self.__config.log( "Sending notification for new task, {}", task_id )
            self.__notifier.notify()

        return task_id


    def require_dispatcher( self,
                            require: "ERequire" = ERequire.COEXIST,
                            log: Optional[Callable[[str], None]] = None ) -> _DispatcherRequired:
        """
        Provides enter/exit semantics on the dispatcher.
        
        Typically called client-side.
        
        Usage::
        
            with bgts.require_dispatcher():
                ...stuff...
                
        :param require: See `ERequire`.
        :param log:     Used to display starting/stopping messages.
        """
        return _DispatcherRequired( self, require, log )


def _get_run_sync() -> bool:
    """
    Obtains the value of the ``MJR_BGTS_RUN_SYNC`` environment variable.
    """
    return bool( int( os.environ.get( "MJR_BGTS_RUN_SYNC", "0" ) ) )
