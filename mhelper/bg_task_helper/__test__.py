import os
import time
from uuid import uuid4
from mhelper import bg_task_helper as BG
from mhelper.bg_task_helper.config_sql import MysqlBasedBgtsConfig
from mhelper import specific_locations, ansi
from mhelper.sql_helper import Remote
import multiprocessing

__all__ = ()


def main():
    my_test_dir = specific_locations.get_application_directory( "rusilowicz", "mhelper", "testing", "bg_task_helper" )

    print( "ENSURING DATABASE EXISTS" )
    remote = Remote( "testing", db_name = "testing" )
    remote.create_database( exists_ok = True )

    print( "CREATING CONFIGURATIONS" )
    config1 = BG.FileBasedBgtsConfig( "test_1", os.path.join( my_test_dir, "tasks" ) )
    config2 = MysqlBasedBgtsConfig( "test_2", remote )
    config2.drop_tables()

    for config in config2, config1:
        print( f"USING CONFIGURATION {config.__class__.__name__}" )
        app = BG.Bgts( config )

        print( "STARTING DISPATCHER" )
        process: multiprocessing.Process = app.start_dispatcher( asynchronous = True )
        assert app.query_dispatcher( 10 )

        print( "CREATING TASKS" )
        uids = [uuid4().__str__() for _ in range( 10 )]
        task_ids = [app.submit_task( myfun, [uid] ) for uid in uids]

        last_results = None

        while True:
            print( "WAITING FOR TASKS TO COMPLETE" )
            results = [app.query_task( task_id ) for task_id in task_ids]
            q = [bool( q.completed_time ) for q in results]

            if q != last_results:
                last_results = q

                for uid, task_id, query in zip( uids, task_ids, results ):
                    if query.completed_time:
                        print( f"> {uid} COMPLETED" )
                        assert query.result == uid
                    else:
                        print( f"> {uid} PENDING" )

            if all( q ):
                break

            time.sleep( 1 )

        print( "CLOSING DISPATCHER" )
        app.stop_dispatcher_no_wait()
        process.join()

        print( "OK!" )
        time.sleep( 5 )

    print( "ALL DONE" )


def myfun( uid: str ):
    time.sleep( 2 )
    return uid


if __name__ == "__main__":
    main()
