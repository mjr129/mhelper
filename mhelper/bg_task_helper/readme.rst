mhelper/bgts
============

A *minimal* background-task service (BGTS) using `ThreadedDispatcher`.

Unlike e.g. Celery:

* No additional services are required.
* A single-processed application can start and stop the BGTS by itself.
* Synchronous processing is supported to assist debugging tasks.
  Please set the ``MJR_BGTS_RUN_SYNC`` environment variable to ``1`` to do this.
* Python "magic" is avoided - any regular function can be submitted to
  the BGTS, without requiring a decorator, etc.
* The BGTS uses a file-based processor by default.
  A MySql adapter is also provided, and custom protocols are supported.

Defining the BGTS and tasks is easy::

    | import sys
    | from mhelper import background_task_helper as BG
    | 
    | config = BG.FileBasedBgtsConfig( "myapp" )
    | app    = BG.Bgts( config )
    | 
    | def my_async_task( message ):
    |     print( message )

First, an example of an app that starts and stops its own dispatcher. This
is common for single-processed frontends, such as desktop GUIs ::

    | import sys
    | from mhelper import background_task_helper as BG
    | 
    | config = BG.FileBasedBgtsConfig( "myapp" )
    | app    = BG.Bgts( config )
    | 
    | def my_async_task( message ):
    |     print( message )
    | 
    * if __name__ == "__main__":
    *     message = sys.argv[1]
    *     
    *     with app.require_dispatcher():
    *         app.submit_task( my_async_task, [message] ) 
            
            
Second, an example of a separate dispatcher and client apps. This is common for
multi-processed frontends, such as HTTP interfaces::

    | import sys
    | from mhelper import background_task_helper as BG
    | 
    | config = BG.FileBasedBgtsConfig( "myapp" )
    | app    = BG.Bgts( config )
    | 
    | def my_async_task( message ):
    |     print( message )
    | 
    | if __name__ == "__main__":
    *     mode = sys.argv[1]
    *     
    *     if mode == "server":
    *         app.run_dispatcher()
    *     elif mode == "client":
    *         message = sys.argv[2]
    *         app.submit_task( my_async_task, [message] )
    *     else:
    *         raise ValueError("Unknown arguments.")
    |
    | # e.g.
    | # bash$ myapp server
    | # bash$ myapp client "hello world"

Finally, there exists a `BgtsCli` class, this may be used to quickly define
a CLI entry point for the BGTS::

    | import sys
    | from mhelper import background_task_helper as BG
    | 
    | config = BG.FileBasedBgtsConfig( "myapp" )
    | app    = BG.Bgts( config )
    * cli    = Bg.BgtsCli( app )
    | 
    | def my_async_task( message ):
    |     print( message )
    | 
    | 
    | if __name__ == "__main__":
    |     mode = sys.argv[1]
    | 
    |     if mode == "server":
    *         cli.send( sys.argv[2] )
    |     elif mode == "client":
    |         app.submit_task( my_async_task, [message] )
    |     else:
    |         raise ValueError("Unknown arguments.")
    | 
    | # e.g.
    | # bash$ myapp server start         # start BGTS
    | # bash$ myapp server tasks
    | # bash$ myapp client "hello world"
    | # bash$ myapp server stop          

