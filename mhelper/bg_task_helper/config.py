from typing import Iterable, Optional

from .data import TaskFilter, TaskQuery, TTaskData, TTaskId, TaskInsert
from .diagnostics import logger

import multiprocessing

__all__="BgtsConfig",

class BgtsConfig:
    """
    Controls the behaviour of a `Bgts`.
    
    Derived classes may *or may not* support the following features:
    
    * FIFO  - Earlier tasks get processed first
    * Users - Tasks may be assigned to specific users
    
    See the documentation on the derived class for details.
    """
    
    
    def __init__( self,
                  queue_name: str,
                  *,
                  poll_frequency: float = 60,
                  max_num_threads: int = 0 ):
        """
        :param queue_name:          Unique name of the BGTS. 
        :param poll_frequency:      Poll frequency.
                                    The BGTS uses mutexes and polling shouldn't technically ever be
                                    required. However, it acts a sanity check to let us know the
                                    difference between an idle system and a deadlocked one.
        :param max_num_threads:     Number of BGTS threads.
                                    0 uses the CPU count. 
        """
        self.poll_frequency = poll_frequency
        self.queue_name = queue_name
        self.max_num_threads = max_num_threads or multiprocessing.cpu_count()
    
    
    def __repr__( self ):
        return f"{type( self ).__name__}({self.queue_name!r})"
    
    
    def initialise( self ) -> None:
        """
        Called on creation and deserialisation. 
        
        This is called on the config object when it becomes bound to the BGTS
        and also the *first* time the instance receives a task in a new process.
        i.e. after it has been *deserialised*.
        """
        pass
    
    
    def query_task( self, task_id: object ) -> Optional[TaskQuery]:
        """
        Client only.
        
        Gets information about a task.
        The query may be declined by returning `None`.
        
        :param task_id: The task ID.
                        Note this may come from an outside source and should
                        be sanitised, for instance if used as part of a
                        database query or file-name.
        """
        _ = task_id
        return None
    
    
    def list_tasks( self, filter: TaskFilter ) -> Optional[Iterable[TTaskId]]:
        """
        Lists all submitted tasks.
        
        The query may be declined by returning `None`.
        """
        _ = filter
        return None
    
    
    def acquire_task( self ) -> Optional[TTaskId]:
        """
        Server only.
        
        Equivalent to the same call in `ThreadedDispatcher`.
        Obtains the ID of an non-started task.
        """
        raise NotImplementedError( "abstract" )
    
    
    def get_queue_name( self ):
        """
        Client and server.
        
        Obtains a arbitrary (but unique!) name for the message queue.
        """
        return self.queue_name
    
    
    def get_handle_exceptions( self ):
        """
        Server only.
        
        When this returns `True` task errors are gracefully handled and the
        exception is stored in the result.
        """
        return True
    
    
    def write_task_data( self, request: TaskInsert ) -> TTaskId:
        """
        Client only.
        
        Writes the data for a new task and generates the task ID.
        
        :param request: Object describing the task to insert 
        :return:        ID of the new task.
        """
        raise NotImplementedError( "abstract" )
    
    
    def log( self, *args, **kwargs ):
        """
        Client and server.
        
        Both client-side and server-side log messages are sent here.
        The function signature includes the message and formatting arguments,
        as per `logging.log`, 
        """
        logger( *args, **kwargs )
    
    
    def get_max_num_threads( self ) -> int:
        """
        Client *or* server side.
        
        Indicates the maximum number of concurrent tasks to permit.
        See `ThreadedDispatcher`.
        """
        return self.max_num_threads
    
    
    def get_poll_frequency( self ) -> float:
        """
        Client *or* server side.
        
        Indicates the poll (fallback) frequency. 
        See `ThreadedDispatcher`. 
        """
        return self.poll_frequency
    
    
    def read_task_data( self, task_id: TTaskId ) -> TTaskData:
        """
        Server only - worker thread.
        
        Reads the task data associated with a particular ID.
        This is only called once per task, when the task begins execution,
        however this is called from the worker thread, so its running status
        should be set when it is acquired (`acquire_task`).
        
        :param task_id: Task ID. 
        :return:        The same data sent to `write_task_data` earlier. 
        """
        raise NotImplementedError( "abstract" )
    
    
    def write_task_result( self, task_id: TTaskId, result: TTaskData ) -> None:
        """
        Server only - worker thread.
        
        Saves the result of a task associated with a particular ID.
        
        :param task_id:     Task ID. 
        :param result:      Result data. 
        """
        raise NotImplementedError( "abstract" )
    
    
    def get_stop_flag( self ) -> bool:
        """
        Server only.
        
        Equivalent to `ThreadedDispatcher.send_heartbeat`.
        Indicates if the server should continue running.
        """
        return True
    
    
    def set_stop_flag( self, value: bool ) -> None:
        """
        Client only.
        
        Sets or clears the stop flag.
        """
        raise NotImplementedError( "abstract" )
    
    
    def recover_tasks( self ) -> None:
        """
        Server only.
        
        Run every time the server starts or restarts, after `initialise` but
        before `set_stop_flag(False)`.
        
        The server may use this opportunity to recover previously failed tasks
        (i.e. tasks for which there was a previous `read_task_data` but no
        subsequent `write_task_result`). Old tasks may also be removed.
        Note that these may not be required (or wanted) depending on the intended
        behaviour of the server.
        """
