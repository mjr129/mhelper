import sys
from collections import Counter
from enum import Enum
from mhelper import bg_task_helper, string_helper

from .data import TaskFilter
from .diagnostics import logger


class BgtsCli:
    """
    Provides a CLI interface for a `Bgts`.
    
    Usage::
    
        app = Bgts( ... )
        cli = BgtsCli( app )
        
        if __name__ == "__main__":
            cli.main()
    """


    def __init__( self, app: "bg_task_helper.Bgts" ):
        """
        :param app: App to handle. 
        """
        self.__app = app


    def main( self ) -> int:
        """
        Entry point to this CLI handler.
        """
        from mhelper import arg_parser
        return arg_parser.execute( self.send )


    def send( self,
              command: "ECommand",
              quiet: bool = False ) -> int:
        """
        Processes a CLI command.
        
        :param command: A member of the `ECommand` or a string.  
        :param quiet:   Stops diagnostics output.
                        Usually the BGTS is a service and so logging is enabled
                        by default. However you may wish to disable it for
                        an application running in the terminal.
        :return:        Return code (0=success). 
        """
        if not quiet:
            logger.attach_to_console()

        if isinstance( command, str ):
            command = string_helper.string_to_enum( ECommand, command )

        name = f"BGTS '{self.__app}'"

        if command == ECommand.START:
            print( f"Starting {name}..." )
            self.__app.start_dispatcher()
            print( "RC0. BGTS has finished running." )
            return 0
        elif command == ECommand.STOP:
            print( f"Stopping {name}..." )
            self.__app.stop_dispatcher()
            print( "RC0. Stop signal sent. BGTS has finished." )
            return 0
        elif command == ECommand.STOP_NO_WAIT:
            print( f"Issuing stop to {name}..." )
            self.__app.stop_dispatcher_no_wait()
            print( "RC0. Stop signal has been sent. It may take a few moments for the BGTS to stop." )
            return 0
        elif command == ECommand.QUERY:
            print( f"Querying {name}..." )

            if not self.__app.query_dispatcher():
                print( f"EXIT1. BGTS is not running." )
                return 1

            if not self.__app.query_dispatcher_responding():
                print( f"RC1. BGTS is running but not responding." )
                return 2

            print( f"RC0. BGTS is running." )
            return 0
        elif command == ECommand.TEST:
            print( f"Submitting test task to {name}..." )
            self.__app.submit_task( _test )
            print( "RC0. Test task submitted." )
            return 0
        elif command == ECommand.TASKS:
            print( f"Listing tasks registered to {name}..." )
            tasks = self.__app.list_tasks( TaskFilter( None, True ) )

            if tasks is None:
                print( "RC1. Task listing is not supported by this BGTS." )
                return 1

            n = 0
            counter = Counter()

            for task_id in tasks:
                n += 1
                query = self.__app.query_task( task_id )
                status_name = query.status.name if query else 'UNKNOWN'
                counter[status_name] += 1
                print( f"{status_name} {task_id}" )

            print( f"RC0. {n} tasks have been listed: {dict( counter )}" )
            return 0
        else:
            from mhelper import arg_parser
            raise arg_parser.CliError( "No command specified." )

        # noinspection PyUnreachableCode
        assert False


def _test() -> None:
    time = 10
    import time as _time
    print( f"This test task has been started, it will run for {time} seconds." )
    _time.sleep( time )
    print( f"This test task has completed." )


class ECommand( Enum ):
    """
    BGTS direct control commands:
    
    :cvar START:        Starts the BGTS
                        
    :cvar STOP:         Stops the BGTS
                        Waits for it to finish.
                        
    :cvar STOP_NO_WAIT: Stops the BGTS.
                        Does not wait for it to finish.
                        
    :cvar QUERY:        Queries whether the BGTS is running
                        
    :cvar TEST:         Submits a test task to the BGTS
                        
    :cvar TASKS:        Lists the tasks registered to the BGTS
    """
    START = 1
    STOP = 3
    STOP_NO_WAIT = 4
    QUERY = 5
    TEST = 6
    TASKS = 7
