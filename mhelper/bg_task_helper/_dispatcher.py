"""
Contains the concrete definition of the underlying BGTS.
This module is internal, the actual protocol of the BGTS is defined by the 
`BgtsConfig`:class: and the interface is defined by the `Bgts`:class:. 

:data __process_is_aware:   Internal.
                            Used to identify when the first task is received on
                            a process so that we can lazily initialise the BGTS.
"""
import multiprocessing
from datetime import datetime
from typing import Optional

from mhelper import bg_task_helper, string_helper, exception_recorder
from mhelper.dispatch_helper import ThreadedDispatcher

from .data import Request, Result, TTaskId

__all__="_BgtsDispatcher", "_run_task_sync"

__process_is_aware = False


class _BgtsDispatcher( ThreadedDispatcher.IHandler ):
    """
    This is a derivative of our async message queue (`ThreadedDispatcher.IHandler`)
    that is used by the BGTS (`bg_task_helper.Bgts`).
    """
    
    
    def __init__( self, owner: "bg_task_helper.Bgts", config: "bg_task_helper.BgtsConfig" ):
        self.__owner = owner
        self.__config = config
    
    
    def initialise( self ):
        self.__config.initialise()
        self.__config.recover_tasks()
        self.__config.set_stop_flag( False )
    
    
    def run_task( self, task_id: TTaskId ) -> None:
        self.log( "STARTING PROCESS FOR TASK #{}", task_id )
        process = multiprocessing.Process( target = _run_task_sync, args = (self.__config, task_id,) )
        process.start()
        process.join()
    
    
    def acquire_task( self ) -> Optional[object]:
        return self.__config.acquire_task()
    
    
    def send_heartbeat( self ) -> bool:
        return not self.__config.get_stop_flag()
    
    
    def log( self, *args, **kwargs ) -> None:
        return self.__config.log( *args, **kwargs )





def _run_task_sync( config: "bg_task_helper.BgtsConfig", task_id: TTaskId ) -> None:
    """
    This method performs the actual task.
    
    It is almost always run from the background task service executable (BGTS).
    
    It *can* be run synchronously from the foreground process (e.g. the web
    service), but this is a debugging feature and will obviously cause the UI
    to become unresponsive while the task is being performed.  
    
    :param config:      The BGTS configuration in use. 
    :param task_id:     The ID of the task to run.
    :return:            Nothing is returned, the result of the task (if any),
                        or error, is registered with the `config`. 
    """
    time_executed = datetime.utcnow()
    
    try:
        global __process_is_aware
        if not __process_is_aware:
            __process_is_aware = True
            config.initialise()
        
        # GET THE TASK
        task_data_bin = config.read_task_data( task_id )
        task_data = Request.parse( task_data_bin, throw = True )
        
        task_fn = task_data.function
        task_args = task_data.arguments
        config.log( "RUNNING TASK #{} - {}{}", task_id, task_fn.__qualname__, (":\n * " + "\n * ".join( f"arg{i}: {x!r}" for i, x in enumerate( task_args ) )) if task_args else "()" )
        
        if config.get_handle_exceptions():
            try:
                result = task_fn( *task_args )
            except Exception as ex:
                result = ex
                __display_error( ex, "running" )
        else:
            result = task_fn( *task_args )
    except Exception as ex:
        result = __make_framework_error( ex )
    
    # STORE THE RESULT
    try:
        task_result_bin = Result.create( result )
    except Exception as ex:
        __display_error( ex, "pickling the result of" )
        task_result_bin = Result.create( Exception( f"Failed to serialise task result, repr: {result!r}" ) )
    
    config.write_task_result( task_id, task_result_bin )
    
    time_completed = datetime.utcnow()
    taken = time_completed - time_executed
    config.log( "COMPLETED TASK #{} IN {}\nResult: {}", task_id, string_helper.timedelta_to_string( taken ), result )


def __make_framework_error( ex: Exception ) -> object:
    """
    Raises and catches an exception to create a `RuntimeError` to set as the
    task result.
    Represents an error in the framework itself, rather than a error in the
    task's code.
    
    :param ex:  Cause 
    :return:    Object to set as the task's result. 
    """
    try:
        raise RuntimeError( "There was an error in processing this asynchronous task. "
                            "This error is from the task framework, "
                            "BGTS or serialised submission data - "
                            "not from the task itself." ) from ex
    except RuntimeError as ex2:
        __display_error( ex, "executing the framework code to run" )
        return ex2


def __display_error( ex: Exception, reason: str ) -> None:
    """
    Displays an error in the console.
    """
    exception_recorder.print_traceback( ex,
                                         message = f"there was an error {reason} an asynchronous task. "
                                                   f"I've caught the error and stored it as the result.",
                                         ref = True )
