"""
Background-task service
=======================

See readme.

.. note::
    
    `MysqlBasedBgtsConfig` is not included because it enforces a MySql dependency.
     It must be imported manually::
     
        from mhelper.bg_task_helper.config_sql import MysqlBasedBgtsConfig
"""

from .data import TTaskId, TaskFilter, TaskQuery, TTaskData, EStatus, Result, Request
from .config import BgtsConfig
from .app import Bgts, BgtsConfig
from .config_file import FileBasedBgtsConfig
from .cli import BgtsCli, ECommand
from .requisite import ERequire
from .diagnostics import logger

