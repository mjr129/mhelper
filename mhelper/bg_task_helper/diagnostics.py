"""
Logging.
"""
from mhelper import log_helper as _log_helper

__all__="logger",

logger = _log_helper.MLog( "bgt_helper",
                           doc = "Default logging destination for background tasks."
                                 "Enable this log to see background tasks submission and completions." )
