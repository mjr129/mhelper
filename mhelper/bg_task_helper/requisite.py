from enum import Enum, auto
from typing import Callable, Optional

from mhelper import bg_task_helper

__all__="ERequire","_DispatcherRequired"

class ERequire( Enum ):
    """
    BGTS application requirements.
    
    The APPLICATION is the block within the `with bgts.require_dispatcher` call,
    usually this is the GUI.
    
    :cvar COEXIST:  Start the BGTS before the APPLICATION, and shut it down after.
                    If the BGTS is already running when the APPLICATION is started,
                    don't do anything.
                    
                    .. warning::
                            
                        This is for *single processed* applications only!
                        Do not use if there will be more than one GUI process.
                        
                        e.g. do not use when running a HTTP server through Gunicorn,
                        there will be multiple processes handling the requests and if
                        this option is used, as soon as the first process ends, so will
                        the BGTS!
    
    :cvar ATTACH:   Attach to an existing BGTS.
                    This must be used if there is more than one GUI process.
                    The BGTS must be started manually in this case.
                    
                    The BGTS is pinged when the APPLICATION starts.
    """
    COEXIST = auto()
    ATTACH = auto()


class _DispatcherRequired:
    """
    Enter/exit semantics, starting server on enter and stopping it on exit,
    if required.
    
    This class is internal but is exposed via `Bgts.require_dispatcher`.
    """
    
    
    def __init__( self,
                  app: "bg_task_helper.Bgts",
                  require: "ERequire",
                  log: Optional[Callable[[str], None]] ):
        self.__app = app
        self.__coexist = require == ERequire.COEXIST
        self.enterant = False
        self.is_running = None
        self.requires_stop = None
        self.log = log if log is not None else lambda _: None
    
    
    def __enter__( self ):
        assert not self.enterant, "Already in use"
        
        self.enterant = True
        
        if self.__coexist:
            try:
                self.log( "Starting BGTS..." )
                self.__app.start_dispatcher( asynchronous = True )
            except bg_task_helper.Bgts.InUseError:
                self.log( "...BGTS is already running." )
            else:
                self.requires_stop = True
                self.log( "...started OK." )
        else:
            if not self.__app.query_dispatcher():
                raise bg_task_helper.Bgts.NotFoundError( "The background task service (BGTS) is not running. "
                                                         "Please start the BGTS first." )
        
        return self
    
    
    def __exit__( self, exc_type, exc_val, exc_tb ):
        if self.requires_stop:
            self.log( "Stopping BGTS..." )
            self.__app.stop_dispatcher()
            self.log( "...stopped OK." )
