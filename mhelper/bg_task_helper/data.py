"""
Data classes (`Request`\s and `Result`\s) and enumerations.
"""
import pickle
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Callable, Dict, Optional, Tuple, Union

__all__ = "EStatus", "TTaskId", "TTaskData", "TaskFilter", "Request", "Result", "TaskInsert", "TaskQuery"

TTaskId = object
TTaskData = bytes


class EStatus( Enum ):
    PENDING = 0
    RUNNING = 1
    COMPLETED = 2
    LOST = 3


@dataclass
class TaskFilter:
    user_id: Optional[object]
    include_completed: bool


class Request:
    """
    Manages the data associated with a task's request.
    """
    
    
    def __init__( self,
                  name: str,
                  function: Callable[..., Optional[object]],
                  arguments: Tuple[object, ...],
                  meta_data: Dict[str, object]
                  ) -> None:
        """
        :param name:            Arbitrary title of the task 
        :param function:        Function that should be called 
        :param arguments:       Arguments to pass to the function 
        """
        self.name = name
        self.function = function
        self.arguments = arguments
        self.meta_data = meta_data
    
    
    @staticmethod
    def parse( task_data: TTaskData, throw = False ) -> Optional["Request"]:
        """
        Reads a `Request` from the saved BLOB.
        """
        try:
            task_data_d = pickle.loads( task_data )
        except Exception as ex:
            if throw:
                raise RuntimeError("Failed to deserialise task request.") from ex
            
            return None
        
        return Request( task_data_d["n"],
                        task_data_d["f"],
                        task_data_d["a"],
                        task_data_d["m"] )
    
    
    @staticmethod
    def create( task_name: str, function: Callable, arguments: Tuple, meta_data: Dict[str, object] ) -> TTaskData:
        """
        Creates a BLOB from a `Request`.
        """
        task_data_d = dict( n = task_name, f = function, a = arguments, m = meta_data )
        return pickle.dumps( task_data_d )


class Result:
    """
    Manages the data associated with a task's result.
    """
    
    
    @staticmethod
    def create( result: object ) -> TTaskData:
        """
        Creates a BLOB from the result.
        """
        return pickle.dumps( result )
    
    
    @staticmethod
    def parse( result: TTaskData ) -> object:
        """
        Reads a result from a BLOB.
        """
        try:
            return pickle.loads( result )
        except Exception as ex:
            return ex


@dataclass()
class TaskInsert:
    """
    Defines a new task to be created.
    """
    task_data: TTaskData
    user_id: object


class TaskQuery:
    """
    Objects returned by querying a task.
    """
    
    
    def __init__( self,
                  task_id: TTaskId,
                  task_data: Union[TTaskData, Callable[[], TTaskData]],
                  queued_time: Optional[datetime],
                  started_time: Optional[datetime],
                  completed_time: Optional[datetime],
                  result_data: Optional[Union[TTaskData, Callable[[], TTaskData]]],
                  user_id: Optional[object],
                  lost_time: Optional[datetime] ):
        """
        :param task_id:             ID of the task queried. 
        :param task_data:           Either the task data or a callable returning it.
        :param queued_time:         Queued/submitted time, or `None` if not queued. 
        :param started_time:        Started/running time, or `None` if not run.         
        :param completed_time:      Completed time, or `None` if not completed. 
        :param result_data:         Either the result data or a callable returning it.
        """
        self.task_id = task_id
        self.__request_data = task_data
        self.__request = None
        self.__has_request = False
        self.queued_time = queued_time
        self.started_time = started_time
        self.completed_time = completed_time
        self.user_id = user_id
        self.lost_time = lost_time
        self.__result_data: Optional[TTaskData] = result_data
        self.__result: Optional[object] = None
        self.__has_result = False
        self.status = (EStatus.LOST if self.lost_time else
                       EStatus.COMPLETED if self.completed_time else
                       EStatus.RUNNING if self.started_time else
                       EStatus.PENDING if self.queued_time else
                       EStatus.LOST)
    
    
    @property
    def request( self ) -> Optional[Request]:
        if not self.__has_request:
            self.__has_request = True
            if callable( self.__request_data ):
                self.__request_data = self.__request_data()
            self.__request = Request.parse( self.__request_data )
            self.__request_data = None
        
        return self.__request
    
    
    @property
    def result( self ) -> Optional[object]:
        if not self.__has_result:
            self.__has_result = True
            if callable( self.__result_data ):
                self.__result_data = self.__result_data()
            self.__result = Result.parse( self.__result_data ) if self.__result_data is not None else None
            self.__result_data = None
        
        return self.__result
