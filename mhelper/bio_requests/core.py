"""
Centralises configuration and caching for the `bio_requests` package.
"""
import json
import os
import urllib.parse
import urllib.request
# !EXPORT_TO_README
from typing import Callable, Optional, Type, TypeVar

from mhelper import io_helper, lock_helper, log_helper, specific_locations, string_helper, web_helper
from mhelper.special_types import Sentinel

core_log = log_helper.MLog( "bio_requests.core" )

_sentinel = Sentinel( "no_value" )
_APP_DIR = "rusilowicz", "bio_requests"
_json_types = { int, str, bool, dict, type( None ), float }
_T = TypeVar( "_T" )


class DataError( Exception ):
    """
    An error has occurred, the user may be able to reconfigure the system to avoid the error.
    
    For instance, if a file cannot be downloaded, the user may be able to specify a different URL, or download the
    file manually and specify the local path instead.
    """


UNSET = Sentinel( "delete" )  # Do not default / delete existing value


class DiskBackedDict:
    """
    Maps a set of keys to values using a file on disk.
    
    The file can be used to cache values, and can be modified by the user to adjust the values.
    
    Concurrent writes are guaranteed not to corrupt the file or lose data.
    However, data is not reloaded unless necessary, and data is not saved unless a concrete change is made.
    Data on disk may therefore differ from applications own cached versions.
    """

    def __init__( self, *args ):
        file_name = get_data_file_name( *args )
        assert os.path.splitext( file_name )[1] == ".json"
        self.__file_name = file_name
        self.__lock_file_name = file_name + ".rcz.lock"
        self.__is_loaded = False
        self.__data = None

    def __repr__( self ):
        return f"{type( self ).__name__}({self.__file_name!r})"

    def __load( self ):
        if self.__is_loaded:
            return

        self.__is_loaded = True

        with lock_helper.FileLock( self.__lock_file_name ):
            self.__data = io_helper.load_json_data( self.__file_name, { } )

    def __save( self, key, value ):
        self.__is_loaded = True

        with lock_helper.FileLock( self.__lock_file_name ):
            self.__data = io_helper.load_json_data( self.__file_name, { } )
            self.__data[key] = value
            io_helper.save_json_data( self.__file_name, self.__data )

    def get_file( self, key: str, extension: str ):
        assert key
        assert extension == "" or extension.startswith( "." )
        directory = os.path.splitext( self.__file_name )[0]
        fn = urllib.parse.quote( key )
        default = os.path.join( directory, fn ) + extension
        r = self.get( key, default )

        if r:
            os.makedirs( os.path.dirname( r ), exist_ok = True )

        return r

    def get_dir( self, key: str ):
        assert key
        r = self.get_file( key, "" )
        os.makedirs( r, exist_ok = True )
        return r

    def get( self, key: str, default = None ):
        assert key
        self.__load()
        value = self.__data.get( key, _sentinel )

        if value is _sentinel:
            value = default
            self.__save( key, value )

        return value

    def __getitem__( self, item: str ):
        assert item
        self.__load()
        return self.__data[item]

    def __setitem__( self, key, value ):
        assert key
        self.__save( key, value )


large_data_path = specific_locations.get_application_directory( *_APP_DIR )
"""
Root of all data
"""


def get_data_file_name( *args ):
    return specific_locations.get_application_file_name( *_APP_DIR, *args )


def get_data_directory( *args ):
    return specific_locations.get_application_directory( *_APP_DIR, *args )


web_cache_path = get_data_directory( "core", "web_cache" )
"""
Controls the web cache directory.
This speeds up arbitrary URL requests by saving the data to disk, but may require a lot of space.
If this is set to empty, the cache is disabled.
"""


def make_request( *args, **kwargs ):
    return web_helper.CachedRequest( *args,
                                     dir = web_cache_path,
                                     **kwargs )


def request( *args, **kwargs ):
    """
    Gets the DATA for a remote URL.
    """
    return make_request( *args, **kwargs ).get()


def download_file( url: str, destination: str, version: Optional[int] ) -> str:  # ~~> FileNotFoundError
    """
    Downloads a file, with a progress indicator.
    """
    lock_fn = destination + ".rcz.download.lock"

    with lock_helper.FileLock( lock_fn ):
        #
        # Read the version indicator, delete the download if the version mismatches
        #
        if version is not None:
            version_tag_fn = destination + ".version"

            if not os.path.isfile( version_tag_fn ):
                disk_version = -1
            else:
                with open( version_tag_fn ) as fin:
                    jdata_in = json.load( fin )

                disk_version = jdata_in.get( "version" )

            if disk_version != version and os.path.isfile( destination ):
                core_log( "Deleting (deprecated): {}", destination )
                os.remove( destination )

        #
        # Download the file if it doesn't already exist
        #
        if not os.path.isfile( destination ):
            BUFFER = 1000
            tmp_fn = destination + ".rcz.download"
            core_log( "Downloading: {} --> {}", url, tmp_fn )

            try:
                with core_log.action( "Downloading", text = string_helper.format_size ) as action:
                    with urllib.request.urlopen( url ) as input:
                        with open( tmp_fn, "wb" ) as output:
                            while True:
                                x = input.read( BUFFER )

                                if not x:
                                    break

                                output.write( x )
                                action.increment( len( x ) )
            except Exception as ex:
                raise FileNotFoundError( f"Failed to download file {url!r}." ) from ex

            os.replace( tmp_fn, destination )

            #
            # Add the version indicator file
            #
            if version is not None:
                jdata_out = { "version": version,
                              "url"    : url }

                with open( version_tag_fn, "w" ) as fout:
                    json.dump( jdata_out, fout )

    return destination


__sys_type = type


def manual_cache( file_name: str,
                  function: Callable[[], object],
                  validate: Callable[[], bool] = lambda: True
                  ) -> None:
    """
    Checks if a cache file exists, or runs a function to create it.
    
    This is guarded by a FileLock and is multi-process safe.
    
    The function should consider using an intermediate and moving it to the destination to assure the result is always
    complete. 
    
    :param file_name:       File that has been/will be generated. 
    :param function:        Function that will be called to generate the file if it doesn't already exist. 
    :param validate:        Additional check performed on the file (in addition to `os.path.exists`).
     
    :exception RuntimeError:    Function did not generate the file or `validate` failed on the newly generated function.
    """
    if os.path.isfile( file_name ) and validate():
        return

    with lock_helper.FileLock( file_name + ".rcz.lock" ):
        if os.path.isfile( file_name ):
            return manual_cache( file_name = file_name,
                                 function = function,
                                 validate = validate )

        function()

        if not os.path.isfile( file_name ):
            raise RuntimeError( "`manual_cache` function failed to create output file." )

        if not validate():
            raise RuntimeError( "`manual_cache` function failed to validate output file." )

    return


def cache( type: Type[_T],
           file_name: str,
           function: Callable[[], object],
           validate: Callable[[object], bool] = None,
           version = None,
           ) -> _T:
    """
    Loads data from a cache file, or calls function to generate it.
    
    * Uses a lock file to allow multiple processes to process data
    * Uses an intermediate for safe writing
    
    If loading of the cache file fails for any reason (including a validation
    error or version mismatch, below) then the cache file is deleted and
    regenerated.
    
    :param type:        Type of value. 
    :param file_name:   Path of file to write to.
    :param function:    Function called to obtain the value if it is not cached and no other process is producing the data.     
    :param validate:    Called to validate the result.
                        If the result fails validation then loading fails. 
    :param version:     Cache version.
                        If there is a version mismatch then loading fails. 
    :return:            Result of `type`.
    """
    if os.path.isfile( file_name ):
        core_log( f"Cache file is present: {file_name}. Expecting version {version}." )

        if version is None:
            result = io_helper.load_binary( file_name, type_ = type, default = None, delete_on_fail = True )
        else:
            loaded_version = io_helper.load_binary( file_name, type_ = __sys_type( version ), default = None,
                                                    delete_on_fail = True )

            if loaded_version == version:
                core_log( f"Version looks OK ({loaded_version})." )
                loaded_version, result = io_helper.load_binary( file_name, type_ = [__sys_type( version ), type],
                                                                default = None, delete_on_fail = True )
            else:
                core_log( f"Version isn't {version} ({loaded_version})." )
                result = None

        if result is not None and (validate is None or validate( result )):
            return result
        else:
            core_log( f"Failed to load." )
    else:
        core_log( f"Cache file is not present: {file_name}" )

    with lock_helper.FileLock( file_name + ".rcz.lock" ):
        if os.path.isfile( file_name ):
            # Other process finished, released lock
            core_log( f"Cache file created by another process: {file_name}" )
            return cache( type = type,
                          file_name = file_name,
                          function = function,
                          validate = validate,
                          version = version )

        result = function()

        if version:
            io_helper.save_binary( file_name, values = [version, result] )
        else:
            io_helper.save_binary( file_name, result )

        return result
