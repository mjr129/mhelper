"""
AaIndex1 from GenomeNet
"""
from typing import Dict, List, Sequence, Tuple

from mhelper.bio_requests import parsing, generics


__all__ = "AaLine", "AaIndex1", "aaindex1"


class AaLine:
    """
    A single "AAIndex".
    
    This class is not internal and may be used to define custom indices.
    """
    __slots__ = "row","accession", "description"
    __mapping = { k: i for i, k in enumerate( "ARNDCQEGHILKMFPSTWYV" ) } # defined by AaIndex1
    
    
    def __init__( self, row: Sequence[float], accession="", description="" ):
        self.row = row
        self.accession=accession
        self.description=description
    
    
    def __getitem__( self, item: str ) -> float:
        i = AaLine.__mapping[item]
        return self.row[i]
    
    
    def transform( self, sequence: str ) -> Sequence[float]:
        """
        Returns the metrics for the specified sequence.
        """
        return tuple( self[aa] for aa in sequence )
    
    def __repr__(self):
        return f"{type(self).__qualname__}({self.accession!r}, {self.description!r}, n={self.num_real!r})"
    
    @property
    def num_real( self ):
        return sum(x==x for x in self.row)


class AaIndex1( Dict[str, AaLine] ):
    """
    The AAIndex, lookup table by accession.
    
    Obtain this object via::
    
        aaindex1 = aaindex1.parse()
        
    List metric accessions::
    
        list( aaindex1 )
        
    Apply an accession to a peptide::
    
        v    = aaindex1.transform( peptide )
        mean = sum( v ) / len( v ) 
    """
    
    
    def split_missing( self ) -> Tuple[List[str], List[str]]:
        """
        Obtains the sets of AAIndex1 metrics with and without missing values.
        
        If only the unordered metrics are required, use `parse_aaindex1`.
        
        :return:    A tuple.
                    The first list contains metrics *without* NAs.
                    The second list contains any metrics *with* NAs.
        """
        good = []
        bad = []
        
        for metric, values in self.items():
            if all( _isvalid( x ) for x in values.row ):
                good.append( metric )
            else:
                bad.append( metric )
        
        return good, bad
    
    
    def transform( self, sequence: str, metric: str ) -> Sequence[float]:
        """
        To be deprecated.
        Consider using `self[metric].transform` instead.  
        """
        return self[metric].transform( sequence )


class _Aaindex1Impl( generics.SingleMappedReceiver[AaIndex1] ):
    parse_version = 1
    
    def __init__( self ):
        super().__init__(
                module = __name__,
                config_key = "genomenet/aaindex1",
                local_extension = ".txt",
                cache = generics.ECache.ALL,
                source_url = "ftp://ftp.genome.jp/pub/db/community/aaindex/aaindex1",
                )
    
    
    def _on_parse( self, primary_ac: None ) -> AaIndex1:
        # order = [Ala    Arg    Asn    Asp    Cys    Gln    Glu    Gly    His    Ile
        #          Leu    Lys    Met    Phe    Pro    Ser    Thr    Trp    Tyr    Val]
        
        all_data = AaIndex1()
        
        current_ac = None
        current_data = None
        current_desc = None
        
        for key, lines in parsing.iter_text_db_fields( self.download( None ) ):
            if key == "H":  # Accession
                assert not current_ac
                current_ac = lines[0]
            elif key == "D":  # Description (title)
                assert not current_desc
                current_desc = lines[0]
            elif key == "I":
                assert not current_data
                assert lines[0] == "A/L     R/K     N/M     D/F     C/P     Q/S     E/T     G/W     H/Y     I/V"
                data_ = lines[1] + " " + lines[2]
                current_data =  tuple( _float( x ) for x in data_.split( " " ) if x )
            elif key == "//":
                assert current_ac
                assert current_data
                
                all_data[current_ac] = AaLine( current_data, current_ac, current_desc)
                
                current_ac = None
                current_data = None
                current_desc = None
        
        return all_data


_NAN = float( "NAN" )


def _isvalid( x ):
    return x == x


def _float( x ):
    if x == "NA":
        return _NAN
    
    return float( x )


aaindex1 = _Aaindex1Impl()
