"""
When executed, prints the location of the bio_requests data files.
"""
from mhelper.bio_requests import core


def main():
    print(core.large_data_path)
    return 0


if __name__ == "__main__":
    exit(main())
