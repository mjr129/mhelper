"""
Provides the common API for the library.
    
!INTERNAL [mhelper.bio_requests]
"""
import os
import sys
from enum import Flag
from functools import partial
from typing import Sequence, Optional, Generic, Type, Union, TypeVar, Dict, Iterable, NoReturn

from mhelper.exception_helper import safe_cast
from mhelper.bio_requests import core
from mhelper import exception_helper, generics_helper, io_helper

__all__ = "MappedReceiver", "SingleMappedReceiver"

_T = TypeVar( "_T" )

TPrimary = TypeVar( "TPrimary" )
"""
Type variable of `MappedReceiver`.

Primary accession (i.e. the database ID).
e.g. a pride-cluster-id.

As a parameter, this should be `None` if the function does not accept an accession.
If no function in the class accepts an accession and the value of the generic is meaningless and should be `None`.
"""

TSecondary = TypeVar( "TSecondary" )
"""
Type variable of `MappedReceiver`.

Something that can be mapped to a TPrimary.
e.g. a taxon-id.

If the generic is `None` the `MappedReceiver` will assume no secondary accessions and any attempt to map an secondary to
primary accession will raise an error.
Note that the secondary accession `None` always maps to the primary accession `None`.
"""

TParsed = TypeVar( "TParsed" )
"""
Type variable of `MappedReceiver`.

Output class of the `parse` method.

If this is `None`, parsing is not supported.
"""

TEither = Union[TPrimary, TSecondary]
"""
Annotation of `MappedReceiver` methods.

If `TPrimary` and `TSecondary` are different, the function takes either
`TPrimary` or `TSecondary`.
If `TPrimary` and `TSecondary` are the same, the function takes only `TPrimary`,
since it cannot infer if mapping needs to be performed. In this case a caller
with only a `TSecondary` must explicitly call `map_accession` to obtain the `TPrimary`.
"""


class ECache( Flag ):
    """
    Cache modes for `MappedReceiver`.
    Flags.
    
    Effects on `MappedReceiver.__init__` parameters `cache` and `parsed` are described.
    
    :data NONE:             Nothing cached.
                            
    :data REMOTE:           Remote URLs.
                            
                            * `cache`: Cache the remote URLs
                                       Otherwise redetermine the URL each time.
                            * `keyed`: Different URLs for each accession.
                                       Otherwise accession should be `None`.
                            
    :data SECONDARY:        Cache the mapping from secondary to primary accessions
    
                            * `cache`: Cache the primary to secondary mapping.
                                       Otherwise recalculate the mapping each time.
                            * `keyed`: Unused. 
                            
    :data LOCAL:            Keep local copies of downloaded data
    
                            * `cache`: Keep copies of downloaded data.
                                       Otherwise download the data each time (may still be cached internally).
                            * `keyed`: Different data file for each accession.
                                       Otherwise accession should be `None`.
                            
    :data PARSED:           Keep parsed versions of data
    
                            * `cache`: Keep copies of parsed data.
                                       Otherwise reparse the data each time.
                            * `keyed`: Different parsed data file for each accession. 
                                       Otherwise accession should be `None`.
                              
    :data ALL:              All of the above    
    """
    NONE = 0
    REMOTE = 1
    SECONDARY = 2
    LOCAL = 4
    PARSED = 8
    ALL = REMOTE | SECONDARY | LOCAL | PARSED


class MappedReceiver( Generic[TPrimary, TSecondary, TParsed] ):
    """
    Common API for `mhelper.bio_requests`.
    
    Defines how:
    
    * Files are downloaded
    * Files are parsed
    * Accessions are mapped
    
    Examples
    --------
    
    Using the "Pride Clusters" receiver:
    
    Download a cluster::
    
        file_name = pride_clusters.download( cluster_id )
    
    Parse (read in) a cluster::
    
        cluster_data = pride_clusters.parse( cluster_id )
        
    Map a taxon ID to a cluster ID::
    
        cluster_id = pride_clusters.map_accession( taxon_id )
        
    Mapping can also be implicit, so this also works::
    
        file_name = pride_clusters.download( taxon_id )
        
    Usage is the same for all other derivations of `MappedReceiver`. 
    
    :data TPrimary:     Type of primary accession.
                        Can be `None` if this is meaningless (i.e. if `keyed` is `NONE` or `primary_name` is `None`).
    :data TSecondary:   Type of secondary accession.
                        Can be `None` if this is meaningless (i.e. if `secondary_name` is `None`).
    :data TParsed:      Type of parsed data.
                        Can be `None` if this is meaningless (i.e. if `parse` is not supported).                          
    """
    DataError = core.DataError  # Alias for user convenience
    parse_version = None
    download_version = 0
    __instance_tag = "_MappedReceiver__instance_tag"

    def __init__( self,
                  *,
                  module: str,
                  config_key: Union[str, Sequence[str]],
                  primary_name: Optional[str],
                  local_extension: str = None,
                  secondary_name: Optional[str],
                  cache: ECache = ECache.ALL,
                  keyed: ECache = ECache.ALL,
                  default_map: Optional[Dict[TSecondary, TPrimary]],
                  source_url: Optional[str] ):
        """
        !INTERNAL [mhelper.bio_requests]
        
        :param module:          i.e. `__name__`
                                Used for error messages. 
        :param config_key:      Root config key. 
        :param primary_name:    Name of the primary accession type (e.g. "proteome-id").
                                Can be `None` if there is no primary accession (single file only).
                                If this is `None`, `TPrimary` should be `None` and no `keyed` should be present.
        :param local_extension: Extension given to the downloaded file (e.g. ".fasta"). 
        :param secondary_name:  Name of the secondary accession type (e.g. "taxon-id")
                                Can be `None` if there is no secondary accession (no mapping).
                                `TSecondary` should be `None` if this is `None`. 
        :param cache:           What to cache (see ECache)
                                Unsupported caches are safely ignored.
        :param keyed:           Where primary/secondary accessions are supported.
                                In cases where they are not supported, the accession provided must be `None`.
                                If no cases are supported, `primary_name` and `TPrimary` should be `None`. 
        :param default_map:     Default map of secondary to primary accessions.
                                Can be `None`: manual overriding of the mapping function may still
                                be performed.
                                Consider implementing `_on_load_default_map` to lazily load the
                                default map if generating the map is an expensive operation.
        :param source_url:      Remote URL format.
                                Should contain a placeholder '{}', unless `primary_name` is `None`.
        """
        self.__module_path = getattr( sys.modules.get( module ), "__file__", "unknown" )

        type_vars = generics_helper.TypeVarMap( self )
        self.__t_primary = safe_cast[type]( type_vars[MappedReceiver, TPrimary] )
        self.__t_secondary = safe_cast[type]( type_vars[MappedReceiver, TSecondary] )
        self.__t_parsed = safe_cast[type]( type_vars[MappedReceiver, TParsed] )

        self.__supports_keyed_remote: bool = bool( keyed & ECache.REMOTE )
        self.__supports_keyed_local: bool = bool( keyed & ECache.LOCAL )
        self.__supports_keyed_parsed: bool = bool( keyed & ECache.PARSED )

        self.__supports_secondary: bool = self.__t_secondary is not type( None )

        self.__supports_cache_parsed: bool = bool( cache & ECache.PARSED )
        self.__supports_cache_secondary: bool = bool( cache & ECache.SECONDARY ) and self.__supports_secondary
        self.__supports_cache_remote_url: bool = bool( cache & ECache.REMOTE )
        self.__supports_cache_local_copy: bool = bool( cache & ECache.LOCAL )

        self.__primary_name: str = primary_name
        self.__secondary_name: str = secondary_name
        self.__local_extension: str = local_extension

        # Maps {primary_name} to its remote URL
        self.__remote_dict = core.DiskBackedDict( config_key,
                                                  "_remote.json" ) if self.__supports_cache_remote_url else None

        # f"Maps {primary_name} to its local file
        self.__local_dict = core.DiskBackedDict( config_key,
                                                 "_local.json" ) if self.__supports_cache_local_copy else None

        # f"Maps {primary_name} to its local cache"
        self.__parsed_dict = core.DiskBackedDict( config_key, "_parsed.json" ) if self.__supports_cache_parsed else None

        # f"Maps {secondary_name} to {primary_name}"
        self.__map_dict = core.DiskBackedDict( config_key, "_map.json" ) if self.__supports_cache_secondary else None

        # The default (template) URL. This cannot be reconfigured.
        self.__source_url_format = source_url

        self.__default_map = default_map or { }
        self.__lazy_default_map = None

        #
        # VALIDATE
        #
        if self.__supports_keyed_remote:
            if self.__source_url_format and "{}" not in self.__source_url_format:
                raise RuntimeError(
                        "__supports_keyed_remote is set but the remote URL does not contain a placeholder." )
        else:
            if self.__source_url_format and "{}" in self.__source_url_format:
                raise RuntimeError( "__supports_keyed_remote is not set but the remote URL contains a placeholder." )

        if any( [self.__supports_keyed_remote, self.__supports_keyed_local, self.__supports_keyed_parsed] ):
            if not primary_name:
                raise RuntimeError( "__supports_keyed_* is set but no primary name is given." )
        elif not any( [self.__supports_keyed_remote, self.__supports_keyed_local, self.__supports_keyed_parsed] ):
            if primary_name:
                raise RuntimeError( "A primary name is given but there is no __supports_keyed_* to use it." )

        if self.__supports_cache_local_copy:
            if not self.__local_extension:
                raise RuntimeError( "If the class __supports_cache_local_copy then it must have a __local_extension." )

        if self.__supports_secondary:
            if not secondary_name:
                raise RuntimeError( "__supports_secondary is set but no secondary_name is given." )
        else:
            if secondary_name:
                raise RuntimeError( "A secondary_name is given but this class does not have `__supports_secondary`." )

        # Propagate defaults to the configuration file  
        self.__propagate_accessions( self.__default_map )

    @classmethod
    def get_instance( cls: Type[_T] ) -> _T:
        """
        Obtains a singleton instance of this class.
        `cls` must be concrete.
        :return:    Instance 
        """
        instance = cls.__dict__.get( MappedReceiver.__instance_tag, None )

        if instance is None:
            instance = cls()
            setattr( cls, MappedReceiver.__instance_tag, instance )

        return instance

    def coerce_accession( self, either_ac: TEither ) -> TPrimary:
        """
        Converts an ambiguous accession type to a primary accession.
        If the type cannot be determined, the accession is assumed to be a primary accession and is passed through 
        verbatim (functions taking `TEither` therefore must be provided with only a primary accession).
        
        See `TEither` for more details. 
        
        :param either_ac:   See `TEither` 
        :return:    Primary accession
        :exception: DataError -- Mapping requires user configuration. 
        """
        if either_ac is None:
            return None

        if self._on_is_secondary( either_ac ):
            return self.map_accession( either_ac )  # ~~> DataError

        return either_ac

    def map_accession( self, secondary_ac: TSecondary ) -> TPrimary:
        """
        Maps an SECONDARY ACCESSION to a PRIMARY ACCESSION.
        """
        if not self.__supports_secondary:
            raise exception_helper.NotSupportedError( "This type does not support secondary accessions" )

        if self.__map_dict is not None:
            primary_ac = self.__map_dict.get( secondary_ac.__str__() )

            # 
            # Try from config
            #
            if primary_ac:
                return primary_ac

        #
        # Try from default map
        #
        primary_ac = self.__default_map.get( secondary_ac, None )

        if primary_ac is not None:
            if self.__map_dict is not None:
                self.__map_dict[secondary_ac.__str__()] = primary_ac
            return primary_ac

        #
        # Try from lazy-loaded default map
        #
        self.load_default_map()

        primary_ac = self.__lazy_default_map.get( secondary_ac, None )

        if primary_ac is not None:
            if self.__map_dict is not None:
                self.__map_dict[secondary_ac.__str__()] = primary_ac
            return primary_ac

        #
        # Ask derived class
        #
        primary_ac = self._on_map_accession( secondary_ac )  # ~~> DataError
        assert primary_ac is not None
        return primary_ac

    def __propagate_accessions( self, secondary_acs: Iterable[TSecondary] ):
        """
        If the default map is loaded, ensures all the values are propagated to
        the configuration file.
        """
        for secondary_ac in secondary_acs:
            if self.__supports_cache_remote_url:
                self._get_source_url( secondary_ac )

            if self.__supports_cache_local_copy:
                self.__get_local_path( secondary_ac )

    def load_default_map( self ) -> None:
        """
        Loads the lazily loaded default map now (if not already).
        
        This can be called externally to force the lazy loading and propagate
        the accessions to the config file, for instance if the data to be lazily
        loaded becomes available as a side-effect of another action.
        """
        if self.__lazy_default_map is None:
            self.__lazy_default_map = { }  # prevent reentry, also prevents trying again if it fails
            self.__lazy_default_map = self._on_load_default_map()
            self.__propagate_accessions( self.__lazy_default_map )

    def _on_load_default_map( self ) -> Dict[TSecondary, TPrimary]:
        """
        Called to obtain a lazily loaded default map.
        This is never called if TSecondary is None.
        This is only called if a required value cannot be obtained from the
        config or the default map provided in the constructor.
        If the default map is simple it should be provided in the constructor,
        since this allows values to propagate to the config at startup.
        """
        return { }

    def _on_map_accession( self, secondary_ac: TSecondary ) -> TPrimary:
        """
        The derived class should convert a secondary accession to a primary
        accession. It should raise a DataError if the accession cannot be
        converted. If the class does not support secondary accessions, this
        method will never be called. If the secondary accession is in the
        default map, this method will not be called.
        
        The base implementation always raises an error. 
        """
        self._raise_error(
                f"Cannot map a secondary accession ({self.__secondary_name}) to a primary accession ({self.__primary_name})." )
        return TPrimary  # unreachable but makes IntelliJ happy

    def __is_secondary( self, either_ac: TEither ) -> bool:
        """
        Requests `_on_is_secondary` if secondary accessions are supported, 
        otherwise returns `False`.
        """
        if self.__supports_secondary:
            return False

        return self._on_is_secondary( either_ac )

    def _on_is_secondary( self, either_ac: TEither ) -> bool:
        """
        The derived class should return whether an accession is an
        *secondary* accession, which requires converting to a *primary*
        accession. If the class does not support secondary accessions, this
        method will not be called.
        
        :return:    `True` if the accession is an secondary accession, `False`
                    if it is a primary accession or unknown.
        """
        if isinstance( either_ac, self.__t_primary ):
            return False
        elif isinstance( either_ac, self.__t_secondary ):
            return True
        else:
            raise exception_helper.type_error( f"accession (provided to mhelper.bio_requests: {self})", either_ac,
                                               [self.__t_primary, self.__t_secondary] )

    def _get_source_url( self, primary_ac: TPrimary ) -> str:
        """
        Obtains the remote source of an accession.
        
        See `coerce_accession` for parameters and errors. 
        """
        default_value = self._on_get_source_url( primary_ac ) or ""

        if self.__remote_dict is not None:
            cache_key = self.__name_primary( primary_ac, ECache.REMOTE )
            value = self.__remote_dict.get( cache_key, default_value )
        else:
            value = default_value

        if not value:
            self._raise_error(
                    f"Cannot obtain the source URL for the primary accession ({self.__primary_name}): {primary_ac!r}." )

        return value

    def __name_primary( self, primary_ac: TPrimary, stage: ECache ) -> str:
        """
        Converts a primary accession to the value used as the cache key.
        """
        if stage == ECache.LOCAL:
            is_supported = self.__supports_keyed_local
        elif stage == ECache.REMOTE:
            is_supported = self.__supports_keyed_remote
        elif stage == ECache.PARSED:
            is_supported = self.__supports_keyed_parsed
        else:
            raise exception_helper.SwitchError( "stage", stage )

        if not is_supported:
            # e.g. for accessionless (singleton) downloads
            if primary_ac is not None:
                raise ValueError(
                        f"An accession has been provided but accessions are not supported for this mode. "
                        f"Owner = {self}. "
                        f"Accession = {primary_ac!r}. "
                        f"Mode = {stage.name}." )

            return "value"
        else:
            # e.g. for accessioned downloads
            if primary_ac is None:
                raise ValueError(
                        f"An accession has not been provided but is required for this mode. "
                        f"Owner = {self}. "
                        f"Accession = {primary_ac!r}. "
                        f"Mode = {stage.name}." )

            return str( primary_ac )

    def _on_get_source_url( self, primary_ac: TPrimary ) -> Optional[str]:
        """
        The derived class should translate the primary accession to its
        default remote URL, or empty ("" or `None`) if it cannot do so.
        
        The base implementation uses the `__source_url_format`, raising an error
        if this was not provided in the constructor.
        """
        if self.__source_url_format is None:
            raise RuntimeError( "The derived class must either provide a `source_url` "
                                "to `__init__` or override `_on_get_source_url`." )

        return self.__source_url_format.format( primary_ac )

    def __get_local_path( self, primary_ac: TPrimary ) -> str:
        """
        Obtains the path to the local file on disk (which may or may not exist).
        
        See `coerce_accession` for parameters and errors.
        """
        assert self.__supports_cache_local_copy
        return self.__local_dict.get_file( self.__name_primary( primary_ac, ECache.LOCAL ), self.__local_extension )

    def request( self, either_ac: TEither ) -> str:  # ~~> DataError
        """
        Obtains the data for the specified ID.
        
        :param either_ac:   Accession; see `coerce_accession` for details.
                            Should be `None` if the accession is not used in determining the remote URL or local file.
                            (i.e. when `REMOTE`/`LOCAL` is not in `keyed`) 
        """
        if self.__supports_cache_local_copy:
            file_name = self.download( either_ac )
            return io_helper.read_all_text( file_name )
        else:
            primary_ac = self.coerce_accession( either_ac )
            remote_url = self._get_source_url( primary_ac )
            return core.request( remote_url )

    def download( self, either_ac: TEither ) -> str:  # ~~> DataError
        """
        Returns the path to the local file, downloading it if necessary.
        
        This method is only supported for classes that cache the downloaded
        copy.
        
        :param either_ac:   Accession; see `coerce_accession` for details.
                            Should be `None` if the accession is not used in determining the remote URL or local file.
                            (i.e. when `REMOTE`/`LOCAL` is not in `keyed`) 
        """
        if not self.__supports_cache_local_copy:
            raise exception_helper.NotSupportedError(
                    f"`download` is not supported by this class ({self.__class__.__name__}) because it doesn't support saving the local copy to disk. Did you mean to call `request` instead?" )

        primary_ac = self.coerce_accession( either_ac )
        remote_url = self._get_source_url( primary_ac )
        local_path = self.__get_local_path( primary_ac )

        try:
            core.download_file( remote_url, local_path, self.download_version )
        except FileNotFoundError:
            pass

        if not os.path.isfile( local_path ) or not os.path.getsize( local_path ):
            self._raise_error(
                    f"The primary accession ({self.__primary_name}, {primary_ac!r}) does not exist at '{local_path}' or the file is empty.\n"
                    f"The remote URL is '{remote_url}'.\n"
                    f"If you are sure that this is the correct primary accession ({self.__primary_name}) check that the path has been correctly configured." )

        return local_path

    def parse( self, either_ac: TEither ) -> TParsed:
        """
        Parses the local file, downloading it if necessary and using or
        generating the cached result if supported.
        
        :param either_ac:   Accession; see `coerce_accession` for details.
                            Should be `None` if the accession is not used in determining the parsed file
                            (i.e. when `PARSED` is not in `keyed`)
        """
        primary_ac = self.coerce_accession( either_ac )

        if self.__parsed_dict is not None:
            parsed_path = self._get_parsed_file_path( primary_ac, ".pickle" )

            return core.cache( self.__t_parsed,
                               parsed_path,
                               partial( self.__parse, primary_ac ),
                               version = self.parse_version )  # ~~> DataError
        else:
            return self.__parse( primary_ac )

    def _get_parsed_file_path( self, primary_ac: TPrimary, extension: str ):
        """
        Obtains the path to which the parsed data is stored.
        """
        assert self.__supports_cache_parsed
        return self.__parsed_dict.get_file( self.__name_primary( primary_ac, ECache.PARSED ), extension )

    def __parse( self, primary_ac: TPrimary ) -> TParsed:
        """
        Requests the actual parsing, independent of any caching.
        
        See `coerce_accession` for parameters and errors.
        """
        return self._on_parse( primary_ac )

    def _on_parse( self, primary_ac: TPrimary ) -> TParsed:
        """
        The derived class should parse the file for the accession.
        
        Use `download` or `request` to obtain the filename or data (whichever
        is more useful).
        
        The result must be serialisable if caching is supported.
        
        The base implementation raises a `NotSupportedError` - "This type does
        not support parsing".
        """
        raise exception_helper.NotSupportedError( f"{type( self ).__name__} does not support parsing. "
                                                  f"Did you mean to call `download` instead?" )

    def __repr__( self ):
        return f"{type( self ).__name__}(...)"

    def _raise_error( self, message: str ) -> NoReturn:
        """
        Raises a `DataError` with a descriptive error message.
        """
        message = (f"{message}\n"
                   f"\n"
                   f"This error originates from the following {MappedReceiver.__name__}.\n"
                   f"If this error is due to a unknown or obsolete default value, additional user configuration may "
                   f"resolve the issue.\n"
                   f"{self.describe()}")

        raise core.DataError( message )

    def describe( self ):
        """
        Describes the MappedReceiver instance.
        """
        return (f"{MappedReceiver.__name__}: {type( self ).__name__}:\n"
                f"|- Module:\n"
                f"|  |- {self.__module_path}\n"
                f"|- Primary accession:\n"
                f"|  |- {self.__primary_name} : {self.__t_primary}\n"
                f"|- Secondary accession:\n"
                f"|  |- {self.__secondary_name} : {self.__t_secondary}\n"
                f"|- Primary accession to local file mapping:\n"
                f"|  |- {self.__local_dict}\n"
                f"|- Secondary accession to primary accession mapping:\n"
                f"|  |- {self.__map_dict}\n"
                f"|- Primary accession to cache file mapping:\n"
                f"|  |- {self.__parsed_dict}\n"
                f"|- Primary accession to remote URL mapping:\n"
                f"   |- {self.__remote_dict}\n")


class SingleMappedReceiver( MappedReceiver[None, None, TParsed], Generic[TParsed] ):
    """
    In the case where `TPrimary` is `None` (i.e. no accessions, only one file)
    this class overloads the methods to avoid the user having to expliclty
    specify a meaningless accession.
    """

    def __init__( self,
                  *,
                  module: str,
                  config_key: Union[str, Sequence[str]],
                  local_extension: str = None,
                  cache: ECache = ECache.ALL,
                  source_url: str
                  ) -> None:
        super().__init__( module = module,
                          config_key = config_key,
                          primary_name = None,
                          secondary_name = None,
                          local_extension = local_extension,
                          cache = cache,
                          keyed = ECache.NONE,
                          default_map = None,
                          source_url = source_url )

    def request( self, x: None = None ) -> str:
        return super().request( None )

    def download( self, x: None = None ) -> str:
        return super().download( None )

    def parse( self, x: None = None ) -> TParsed:
        return super().parse( None )


def _identity( x ):
    return x
