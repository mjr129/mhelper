from typing import Iterator, Tuple, List, Dict, Optional


def iter_text_db_fields( file_name ) -> Iterator[Tuple[str, List[str]]]:
    """
    Iterates a "text database", an ad-hoc format used by Uniprot, GenomeNet,
    amongst others. Looks something like this::
    
    
        H ANDN920101
        D Shifts
        A Andersen
        I    A/L     R/K     N/M
            4.35    4.38    4.75
            4.17    4.36    4.52
        //
        H ARGP820101
        D Hydrophobicity
        A Argos
        
    This iterator iterates over the fields, i.e.
    
        ["A", ["ANDN920101"]]
        ["D", ["Shifts"]]
        
    Note the key is a `str` and the value is a list of one or more lines.
    
    In the source, multi-line fields can be specified using either convention::
    
        F a
          b
          
    Or::
    
        F a
        F b
    
    For record separation, see `iter_text_db_records`. 
    """
    keysp = " "
    key = None
    current = None
    
    with open( file_name ) as fin:
        for line in fin:
            line = line.rstrip()
            
            if line[0].isspace():
                # Continuation (I)
                current.append( line.lstrip() )
            elif line.startswith( keysp ):
                # Continuation (II)
                current.append( line.split( " ", 1 )[1].lstrip() )
            else:
                # New
                if key is not None:
                    yield key, current
                
                current = []
                if " " in line:
                    key, line = line.split( " ", 1 )
                    current.append( line.lstrip() )
                else:
                    key = line
                
                keysp = key + " "
    
    if key is not None:
        yield key, current


def iter_text_db_records( file_name: str, delimiter: Optional[str] = "//" ) -> Iterator[Dict[str, List[str]]]:
    """
    Delimits the records of a text database.
    See `iter_text_db_fields`.
     
    Records are delimited by a `delimiter` key.
    
    Note that if the delimiter fields themselves contain data they will be
    assumed to form part of the successive record. 
    """
    current = { }
    
    for key, lines in iter_text_db_fields( file_name ):
        if key == delimiter:
            if current:
                yield current
            
            current = { }
            current[key] = lines
        else:
            current[key] = lines
    
    if current:
        yield current
