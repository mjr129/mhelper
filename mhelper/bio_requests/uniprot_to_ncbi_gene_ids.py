"""
Maps Uniprot Protein IDs to NCBI Gene IDs
"""
from typing import Dict, List, Optional, Sequence, Set, Tuple

import pandas
from pathlib import Path
from mhelper.bio_requests.generics import ECache, SingleMappedReceiver, MappedReceiver
from mhelper.bio_requests import core
from mhelper import io_helper
from collections import defaultdict

_TDictList = Dict[str, List[str]]


def map_uniprot_to_ncbi_gene( uniprot_ids: Sequence[str],
                              taxon_id: Optional[int]
                              ) -> _TDictList:
    """
    Maps Uniprot protein IDs to NCBI gene IDs.
    
    :param uniprot_ids: List of uniprot IDs to find
    :param taxon_id:    Taxon to constrain search to.
                        This can be none or zero, which indicate no constraint but will make the search much slower.
    :return:            A dictionary mapping each uniprot ID and to a list of associated NCBI gene IDs.
                        Note one Uniprot ID may map to zero, one or multiple NCBI gene IDs.
    """
    u_to_g = {}
    u_to_p = map_uniprot_to_ncbi_protein( uniprot_ids )
    all_p = [x for l in u_to_p.values() for x in l]
    p_to_g = map_ncbi_protein_to_ncbi_gene( all_p, taxon_id )

    for u in set( uniprot_ids ):
        lst = set()

        for p in u_to_p.get( u, () ):
            for g in p_to_g.get( p, () ):
                lst.add( g )
                
        u_to_g[u] = list(lst)

    return u_to_g


class __ncbi_to_uniprot( SingleMappedReceiver[None] ):
    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = "ncbi/gene_refseq_uniprotkb_collab",
                          cache = ECache.ALL,
                          local_extension = ".tsv.gz",
                          source_url = "https://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_refseq_uniprotkb_collab.gz" )


def map_uniprot_to_ncbi_protein( uniprot_ids: Sequence[str] ) -> _TDictList:
    mapping = defaultdict( list )
    uniprot_ids = set( uniprot_ids )
    file_name = __ncbi_to_uniprot.get_instance().download( None )

    with io_helper.open_for_read( file_name, gzip = True ) as fin:
        h = fin.readline().rstrip( "\n" ).split( "\t" )

        col_ncbi = h.index( "#NCBI_protein_accession" )
        col_uniprot = h.index( "UniProtKB_protein_accession" )

        for line in fin:
            cells = line.rstrip( "\n" ).split( "\t" )

            uniprot = cells[col_uniprot]

            if uniprot not in uniprot_ids:
                continue

            ncbi = cells[col_ncbi]
            mapping[uniprot].append( ncbi )

    return dict( mapping )


class __gene_to_ncbi( MappedReceiver[int, None, Path] ):
    """
    Downloads the NCBI gene2refseq table.
    
    Obtain path to entire table::
    
        file_path : Path = __gene_to_ncbi.get_instance().parse(None)
        
    Obtain subset of table for a particular taxon, e.g. 9606:
    
        file_path : Path = __gene_to_ncbi.get_instance().parse( 9606 )
    """

    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = "ncbi/gene2refseq/version2",
                          cache = ECache.ALL,
                          keyed = ECache.PARSED,
                          default_map = {},
                          primary_name = "taxon-id",
                          secondary_name = None,
                          local_extension = ".tsv.gz",
                          source_url = "https://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2refseq.gz" )

    def parse( self, either_ac: Optional[int] ) -> Path:
        primary_ac: Optional[int] = self.coerce_accession( either_ac )

        if not primary_ac:
            return Path( self.download( None ) )

        file_name_out = self._get_parsed_file_path( primary_ac, ".tsv.gz" )

        def ___gen():
            file_name = self.download( None )

            with io_helper.open_for_write( file_name_out, gzip = True ) as fout:
                with io_helper.open_for_read( file_name, gzip = True ) as fin:
                    line = fin.readline().rstrip( "\n" )
                    h = line.rstrip( "\n" ).split( "\t" )
                    fout.write( line )
                    fout.write( "\n" )

                    col_tax_id = h.index( "#tax_id" )

                    for line in fin:
                        line = line.rstrip( "\n" )

                        cells = line.split( "\t" )
                        tax_id = int( cells[col_tax_id] )

                        if tax_id != either_ac:
                            continue

                        fout.write( line )
                        fout.write( "\n" )

        core.manual_cache( file_name_out, ___gen )
        return Path( file_name_out )


def map_ncbi_protein_to_ncbi_gene( ncbi_proteins: List[str], taxon_id: Optional[int] ) -> _TDictList:
    ncbi_proteins = set( ncbi_proteins )
    file_path: Path = __gene_to_ncbi.get_instance().parse( taxon_id )
    mapping = defaultdict( list )

    with io_helper.open_for_read( file_path, gzip = True ) as fin:
        h = fin.readline().rstrip( "\n" ).split( "\t" )

        col_protein_accession_version = h.index( "protein_accession.version" )
        col_gene_id = h.index( "GeneID" )

        for row in fin:
            row = row.rstrip( "\n" )

            if not row:
                continue

            cells = row.split( "\t" )

            protein_accession = cells[col_protein_accession_version].split( ".", 1 )[0]

            if protein_accession not in ncbi_proteins:
                continue

            gene_id = cells[col_gene_id]
            mapping[protein_accession].append( gene_id )

    return dict( mapping )
