from dataclasses import dataclass

from mhelper.bio_requests.generics import MappedReceiver, TParsed, ECache
import json
from mhelper import exception_helper


__all__ = "ensembl_transcripts", "EnsemblEntity", "TEnsemblId"

TEnsemblId = str


@dataclass
class EnsemblEntity:
    accession: str
    sequence: str


class _EnsemblSequenceImpl( MappedReceiver[TEnsemblId, None, EnsemblEntity] ):
    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = ["ensembl", "sequences"],
                          primary_name = "ensembl-id",
                          local_extension = ".txt",
                          secondary_name = None,
                          cache = ECache.NONE,
                          keyed = ECache.ALL,
                          default_map = None,
                          source_url = "https://rest.ensembl.org/sequence/id/{}?content-type=text/plain" )
    
    
    def _on_parse( self, primary_ac: str ) -> TParsed:
        sequence = self.request( primary_ac )
        
        if not sequence or sequence.startswith( "{" ):
            raise exception_helper.NotFoundError( "No or invalid sequence obtained." )
        
        return EnsemblEntity( primary_ac, sequence )


class _EnsemblArchiveImpl( MappedReceiver[TEnsemblId, None, EnsemblEntity] ):
    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = ["ensembl", "sequences"],
                          primary_name = "ensembl-id",
                          local_extension = ".json",
                          secondary_name = None,
                          cache = ECache.NONE,
                          keyed = ECache.ALL,
                          default_map = None,
                          source_url = "https://rest.ensembl.org/archive/id/{}?content-type=application/json" )
    
    
    def _on_parse( self, primary_ac: str ) -> TParsed:
        data = self.request( primary_ac )
        
        json_ = json.loads( data )
        
        if "error" in json_:
            raise exception_helper.NotFoundError( "No or invalid sequence obtained." )
        
        sequence = json_["peptide"]
        
        if not sequence:
            raise exception_helper.NotFoundError( "No or invalid sequence obtained." )
        
        return EnsemblEntity( primary_ac, sequence )


ensembl_archive = _EnsemblArchiveImpl()
ensembl_sequence = _EnsemblSequenceImpl()
