"""
Access the NCBI taxonomy.

* Download, cache, and open the taxonomy database
* Translate IDs to and from names
* Traverse the taxonomy hierarchy

!EXECUTABLE
"""
# !EXPORT_TO_README
import functools
from dataclasses import dataclass
from typing import Optional, Dict, List, Sequence, Iterator, Any

import warnings
import os
import os.path
import zipfile

from mhelper import io_helper, lock_helper

from mhelper.bio_requests import core
from mhelper.special_types import NOT_PROVIDED

_download_location = core.DiskBackedDict( "ncbi/taxonomy/local.json" ).get_dir( "value" )
_source_url = core.DiskBackedDict( "ncbi/taxonomy/remote.json" ).get( "value", "ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.zip" )
_cache_location = core.DiskBackedDict( "ncbi/taxonomy/cache.json" ).get_dir( "value" )


def get_taxonomy_files() -> Dict[str, str]:
    zip_fn = os.path.join( _download_location, "new_taxdump.zip" )
    tax_fn = os.path.join( _download_location, "nodes.dmp" )
    nam_fn = os.path.join( _download_location, "names.dmp" )
    lok_fn = os.path.join( _download_location, ".rcz.lock" )

    #
    # Download and extract the zip if necessary
    #
    if not os.path.isfile( tax_fn ) or not os.path.isfile( nam_fn ):
        with lock_helper.FileLock( lok_fn ):
            tax_exists = os.path.isfile( tax_fn )  # Check again, inside the lock
            nam_exists = os.path.isfile( nam_fn )

            if not nam_exists or not tax_exists:
                #
                # If there is only half a database, remove it
                #
                if nam_exists:
                    os.remove( nam_fn )
                    warnings.warn( "A partial output existed, this has been now deleted:\n * {}".format( nam_fn ) )

                if tax_exists:
                    os.remove( tax_fn )
                    warnings.warn( "A partial output existed, this has been now deleted:\n * {}".format( tax_fn ) )

                if os.path.isfile( zip_fn ):
                    os.remove( zip_fn )
                    warnings.warn( "A partial output existed, this has been now deleted:\n * {}".format( zip_fn ) )

                #
                # Download the data
                #
                core.download_file( _source_url, zip_fn, None )

                #
                # Extract the data
                #
                with core.core_log.action( "Extracting taxonomy files..." ):
                    zip = zipfile.ZipFile( zip_fn, 'r' )
                    zip.extract( "nodes.dmp", path = _download_location )
                    zip.extract( "names.dmp", path = _download_location )
                    zip.close()

                #
                # Remove the ZIP
                #
                os.remove( zip_fn )

    assert os.path.isfile( tax_fn )
    assert os.path.isfile( nam_fn )

    return { "nodes.dmp": tax_fn, "names.dmp": nam_fn }


TNames = Dict[int, Dict[str, List[str]]]


def read_names() -> TNames:
    """
    Reads a taxonomy names file.
     
    :return: Dictionary:
                K: Taxon ID
                V: Dictionary:
                    K: Name type (e.g. "scientific_name")
                    V: LIST:
                        V: Name
    """
    file_name = get_taxonomy_files()["names.dmp"]

    result_names: Dict[int, Dict[str, List[str]]] = { }

    with core.core_log.action( "Reading names.dmp" ) as action:
        with open( file_name, "r" ) as file:
            for line in file:
                action.increment()
                fields = [x.strip( " \t|\n" ) for x in line.split( "\t|\t" )]

                id = int( fields[0] )
                name = fields[1]
                name_type = fields[3].lower().replace( " ", "_" ).replace( "-", "_" )

                if id in result_names:
                    dict_for_id = result_names[id]
                else:
                    dict_for_id = { }
                    result_names[id] = dict_for_id

                if name_type in dict_for_id:
                    names_for_class = dict_for_id[name_type]
                else:
                    names_for_class = []
                    dict_for_id[name_type] = names_for_class

                names_for_class.append( name )

    return result_names


class ITaxonomy:
    def get_name( self, tax_id: int ) -> str:
        pass

    def get_id( self, name: str ) -> int:
        pass

    def get_parent( self, tax_id: int ) -> int:
        pass

    def get_children( self, tax_id: int ) -> Sequence[int]:
        pass


class Taxon:
    """
    This is a convenience class that uses `get_taxonomy` and `get_full_taxonomy`.
    See the warnings associated with those methods.
    """

    def __init__( self, tax_id ):
        self.tax_id = tax_id

    @staticmethod
    def get_tax_ids( *args ):
        return tuple( x.tax_id if isinstance( x, Taxon ) else x for x in args )

    def inherits( self, other ):
        this, other = Taxon.get_tax_ids( self, other )

        for ancestor in get_taxonomy().iter_ancestors( this ):
            if other == ancestor:
                return True

        return False


class Taxonomy:
    def __init__( self ):
        self.__id_to_parent_id: Dict[int, int] = { }
        self.__id_to_name: Dict[int, str] = { }

    def __repr__( self ):
        return f"{type( self ).__name__}(n={len( self.__id_to_name )})"

    def __len__( self ) -> int:
        return len( self.__id_to_name )

    def __iter__( self ) -> Iterator[int]:
        return iter( self.__id_to_parent_id )

    @staticmethod
    def load() -> "Taxonomy":
        cac_fn = os.path.join( _cache_location, "core_taxonomy.pickle" )

        if os.path.isfile( cac_fn ):
            with core.core_log.time( "Loading cached NCBI taxonomy (core_taxonomy.pickle)" ) as timer:
                result = io_helper.load_binary( cac_fn, type_ = Taxonomy, default = None, delete_on_fail = True )
                timer.result = result

                if result is not None:
                    return result

        rr = Taxonomy()
        full = FullTaxonomy.load()

        for taxon in full:
            assert taxon.tax_id not in rr.__id_to_parent_id
            rr.__id_to_parent_id[taxon.tax_id] = taxon.parent_tax_id
            rr.__id_to_name[taxon.tax_id] = taxon.scientific_name

        assert len( rr )
        io_helper.save_binary( cac_fn, rr )

        return rr

    def iter_ancestors( self, tax_id: int ) -> Iterator[int]:
        while tax_id is not None:
            yield tax_id
            tax_id = self.get_parent( tax_id )

    def get_name( self, tax_id: int ) -> str:
        try:
            return self.__id_to_name[tax_id]
        except KeyError as ex:
            assert isinstance( tax_id, int ), "Wrong type?"
            raise KeyError( f"The taxon ID {tax_id!r} does not exist." ) from ex

    def get_id( self, scientific_name: str, default: Any = NOT_PROVIDED ) -> int:
        r = []

        for key, value in self.__id_to_name.items():
            if value == scientific_name:
                r.append( key )

        if len( r ) == 1:
            return r[0]
        elif default is NOT_PROVIDED:
            raise KeyError( f"The scientific name {scientific_name!r} cannot be mapped to a taxon." )
        else:
            return default

    def get_parent( self, tax_id: int ) -> Optional[int]:
        try:
            return self.__id_to_parent_id[tax_id]
        except KeyError as ex:
            raise KeyError( f"The taxon ID {tax_id!r} does not exist." ) from ex

    def get_children( self, tax_id: int ) -> List[int]:
        r = []

        for key, value in self.__id_to_parent_id:
            if value == tax_id:
                r.append( key )

        return r


TTaxa = Dict[int, "NcbiTaxonNode"]


class FullTaxonomy:
    __slots__ = "__id_to_taxon", "__id_to_names"

    def __init__( self ):
        self.__id_to_taxon: TTaxa = { }
        self.__id_to_names: TNames = { }

    def __len__( self ):
        return len( self.__id_to_taxon )

    def __iter__( self ):
        return iter( self.__id_to_taxon.values() )

    def get_parent( self, child: "NcbiTaxonNode" ) -> "NcbiTaxonNode":
        try:
            return self.__id_to_taxon[child.parent_tax_id]
        except KeyError as ex:
            raise KeyError( f"The taxon ID {child.parent_tax_id} does not exist." ) from ex

    def get_names( self, taxon: "NcbiTaxonNode", name_type: str ) -> Sequence[str]:
        try:
            names = self.__id_to_names[taxon.tax_id]
        except KeyError as ex:
            raise KeyError( f"The taxon ID {taxon.tax_id} does not exist." ) from ex

        try:
            return names[name_type]
        except KeyError as ex:
            raise KeyError( f"The taxon ID {taxon.tax_id} does not provide a {name_type!r}." ) from ex

    def get_name( self, taxon: "NcbiTaxonNode", name_type: str ) -> str:
        names = self.get_names( taxon, name_type )

        if not names:
            raise KeyError( f"The taxon ID {taxon.tax_id} does not have a {name_type!r}." )

        return names[0]

    @staticmethod
    def load() -> "FullTaxonomy":
        cac_fn = os.path.join( _cache_location, "full_taxonomy.pickle" )

        if os.path.isfile( cac_fn ):
            with core.core_log.action( "Loading cached full_taxonomy.pickle" ):
                r = io_helper.load_binary( cac_fn, type_ = FullTaxonomy, default = None, delete_on_fail = True )

                if r is not None:
                    return r

        rr = FullTaxonomy()

        nodes_fn = get_taxonomy_files()["nodes.dmp"]
        rr.__id_to_names = read_names()

        with core.core_log.action( "Reading nodes.dmp" ) as action:
            with open( nodes_fn, "r" ) as file:
                for line in file:
                    action.increment()
                    taxon = rr.__yield_line( line )
                    rr.__id_to_taxon[taxon.tax_id] = taxon

        with core.core_log.action( "Saving taxonomy to cache" ):
            io_helper.save_binary( cac_fn, rr )

        return rr

    def __yield_line( self, line: str ) -> "NcbiTaxonNode":
        fields = [x.strip( " \t|\n" ) for x in line.split( "\t|\t" )]
        nex = 18

        if len( fields ) < nex:
            raise ValueError( f"This is not a taxonomy node database (it has {len( fields )} fields, I expected {nex})." )

        if len( fields ) > nex:
            warnings.warn(
                    f"This taxonomy node database has {len( fields )} fields, I expected {nex}. This may be a new version but please check that the fields are in the order expected. See {__file__!r}.",
                    UserWarning )

        tax_id = int( fields[_k.tax_id] )
        parent_tax_id = int( fields[_k.parent_tax_id] )

        return NcbiTaxonNode(
                tax_id = tax_id,
                parent_tax_id = parent_tax_id if parent_tax_id != tax_id else None,
                rank = fields[_k.rank],
                embl_code = fields[_k.embl_code],
                division_id = int( fields[_k.division_id] ),
                inherited_div_flag = _str_to_bool( fields[_k.inherited_div_flag] ),
                genetic_code_id = int( fields[_k.genetic_code_id] ),
                inherited_gc_flag = _str_to_bool( fields[_k.inherited_gc_flag] ),
                mitochondrial_genetic_code_id = int( fields[_k.mitochondrial_genetic_code_id] ),
                inherited_mgc_flag = _str_to_bool( fields[_k.inherited_mgc_flag] ),
                genbank_hidden_flag = _str_to_bool( fields[_k.genbank_hidden_flag] ),
                hidden_subtree_root_flag = _str_to_bool( fields[_k.hidden_subtree_root_flag] ),
                plastid_genetic_code_id = int( fields[_k.plastid_genetic_code_id] ) if fields[
                    _k.plastid_genetic_code_id] else None,
                comments = fields[_k.comments],
                inherited_pgc_flag = _str_to_bool( fields[_k.inherited_pgc_flag] ) if fields[_k.inherited_pgc_flag] else None,
                specified_species = _str_to_bool( fields[_k.specified_species] ),
                hydrogenosome_genetic_code_id = int( fields[_k.hydrogenosome_genetic_code_id] ) if fields[
                    _k.hydrogenosome_genetic_code_id] else None,
                inherited_hgc_flag = _str_to_bool( fields[_k.inherited_hgc_flag] ),
                _owner = self )


__persistent = { }


@functools.lru_cache
def get_taxonomy() -> Taxonomy:
    """"
    Downloads, caches, and parses NCBI taxonomy files.
    Extraenouous information is stripped.
    
    For full information see `get_full_taxonomy`.
    
    .. warning:
    
        This persists the taxonomy in memory.
        For a non-persistent version, use `Taxonomy.load`.
    """
    return Taxonomy.load()


@functools.lru_cache
def get_full_taxonomy() -> FullTaxonomy:
    """
    Downloads, caches, and parses NCBI taxonomy files.
    
    .. warning:
    
        This persists the taxonomy in memory.
        For a non-persistent version, use `Taxonomy.load`.
    """
    return FullTaxonomy.load()


class _k:
    """
    nodes.dmp mapping, from https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/taxdump_readme.txt.
    
    !TODO This could be reflected from `Taxon`.
    """
    tax_id = 0  # node id in GenBank taxonomy database
    parent_tax_id = 1  # parent node id in GenBank taxonomy database
    rank = 2  # rank of this node (superkingdom, kingdom, ...) 
    embl_code = 3  # locus-name prefix; not unique
    division_id = 4  # see division.dmp file
    inherited_div_flag = 5  # (1 or 0) 1 if node inherits division from parent
    genetic_code_id = 6  # see gencode.dmp file
    inherited_gc_flag = 7  # (1 or 0) 1 if node inherits genetic code from parent
    mitochondrial_genetic_code_id = 8  # see gencode.dmp file
    inherited_mgc_flag = 9  # (1 or 0) 1 if node inherits mitochondrial gencode from parent
    genbank_hidden_flag = 10  # (1 or 0) 1 if name is suppressed in GenBank entry lineage
    hidden_subtree_root_flag = 11  # (1 or 0) 1 if this subtree has no sequence data yet
    comments = 12  # free-text comments and citations
    plastid_genetic_code_id = 13  # see gencode.dmp file
    inherited_pgc_flag = 14  # (1 or 0) 1 if node inherits plastid gencode from parent
    specified_species = 15  # (1 or 0) 1 if species in the node's lineage has formal name
    hydrogenosome_genetic_code_id = 16  # see gencode.dmp file
    inherited_hgc_flag = 17  # (1 or 0) 1 if node inherits hydrogenosome gencode from parent


class _read_names:
    def __init__( self, key ):
        self.key = key

    def __get__( self, instance: "NcbiTaxonNode", owner ) -> Sequence[str]:
        return instance._owner.get_names( instance, self.key )


class _read_name:
    def __init__( self, key ):
        self.key = key

    def __get__( self, instance: "NcbiTaxonNode", owner ) -> str:
        return instance._owner.get_name( instance, self.key )


@dataclass
class NcbiTaxonNode:
    tax_id: int
    parent_tax_id: int
    rank: str
    embl_code: str
    division_id: int
    inherited_div_flag: bool
    genetic_code_id: int
    inherited_gc_flag: bool
    mitochondrial_genetic_code_id: int
    inherited_mgc_flag: bool
    genbank_hidden_flag: bool
    hidden_subtree_root_flag: bool
    comments: str
    plastid_genetic_code_id: Optional[int]
    inherited_pgc_flag: Optional[bool]
    specified_species: bool
    hydrogenosome_genetic_code_id: Optional[int]
    inherited_hgc_flag: bool
    _owner: FullTaxonomy

    def __str__( self ):
        return f"Taxon{self.tax_id}"

    @property
    def parent( self ) -> Optional["NcbiTaxonNode"]:
        return self._owner.get_parent( self )

    acronym = _read_name( "acronym" )
    anamorph = _read_name( "anamorph" )
    authority = _read_name( "authority" )
    blast_name = _read_name( "blast_name" )
    common_name = _read_name( "common_name" )
    equivalent_name = _read_name( "equivalent_name" )
    genbank_acronym = _read_name( "genbank_acronym" )
    genbank_anamorph = _read_name( "genbank_anamorph" )
    genbank_common_name = _read_name( "genbank_common_name" )
    genbank_synonym = _read_name( "genbank_synonym" )
    in_part = _read_name( "in_part" )
    includes = _read_name( "includes" )
    misnomer = _read_name( "misnomer" )
    misspelling = _read_name( "misspelling" )
    scientific_name = _read_name( "scientific_name" )
    synonym = _read_name( "synonym" )
    teleomorph = _read_name( "teleomorph" )
    type_material = _read_name( "type_material" )

    acronym_list = _read_names( "acronym" )
    anamorph_list = _read_names( "anamorph" )
    authority_list = _read_names( "authority" )
    blast_name_list = _read_names( "blast_name" )
    common_name_list = _read_names( "common_name" )
    equivalent_name_list = _read_names( "equivalent_name" )
    genbank_acronym_list = _read_names( "genbank_acronym" )
    genbank_anamorph_list = _read_names( "genbank_anamorph" )
    genbank_common_name_list = _read_names( "genbank_common_name" )
    genbank_synonym_list = _read_names( "genbank_synonym" )
    in_part_list = _read_names( "in_part" )
    includes_list = _read_names( "includes" )
    misnomer_list = _read_names( "misnomer" )
    misspelling_list = _read_names( "misspelling" )
    scientific_name_list = _read_names( "scientific_name" )
    synonym_list = _read_names( "synonym" )
    teleomorph_list = _read_names( "teleomorph" )
    type_material_list = _read_names( "type_material" )


def _str_to_bool( text: str ) -> bool:
    """
    Converts the text to a boolean value.
    """
    if text == "0":
        return False
    elif text == "1":
        return True
    else:
        raise ValueError( "Cannot convert text \"" + text + "\" to bool." )


def get_url( tax_id: int ):
    return f"https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id={tax_id}"


def main():
    import sys
    id = sys.argv[1]
    print( get_taxonomy().get_name( int( id ) ) )


if __name__ == "__main__":
    main()
