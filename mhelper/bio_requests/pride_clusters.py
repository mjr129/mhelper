import gzip
from dataclasses import dataclass
from typing import FrozenSet

from mhelper.bio_requests.generics import TParsed, ECache, MappedReceiver


__all__ = "pride_clusters", "PrideCluster"


@dataclass( frozen = True )
class PrideCluster:
    peptides: FrozenSet[str]


class _PrideClustersImpl( MappedReceiver[str, int, PrideCluster] ):
    __DEFAULT_URL = "ftp://ftp.pride.ebi.ac.uk/pride/data/cluster/spectrum-libraries/{}"
    
    # The map can be geenrated by using the regex:
    #       taxonomy\/([0-9]+).*?href="(ftp[^"]+)
    # At the *rendered* version of:
    #       https://www.ebi.ac.uk/pride/cluster/#/libraries
    __DEFAULT_MAP = {
        # ...
        9606 : "2015-04/Human.msp.gz",
        10090: "2015-04/Mouse.msp.gz",
        3702 : "2015-04/Arabidopsis thaliana.msp.gz",
        562  : "2015-04/Escherichia coli.msp.gz",
        10116: "2015-04/Rat.msp.gz",
        4932 : "2015-04/Saccharomyces cerevisiae.msp.gz",
        1423 : "2015-04/Bacillus subtilis.msp.gz",
        7227 : "2015-04/Drosophila melanogaster.msp.gz",
        90371: "2015-04/Salmonella typhimurium.msp.gz",
        1131 : "2015-04/Synechococcus sp..msp.gz",
        35554: "2015-04/Geobacter sulfurreducens.msp.gz",
        6239 : "2015-04/Caenorhabditis elegans.msp.gz",
        3847 : "2015-04/Glycine max.msp.gz",
        139  : "2015-04/Borrelia burgdorferi.msp.gz",
        1134 : "2015-04/Synechocystis sp..msp.gz",
        1396 : "2015-04/Bacillus cereus.msp.gz",
        # 0    : "ftp://ftp.pride.ebi.ac.uk/pride/data/cluster/spectrum-libraries/2015-04/crap.msp.gz",
        }
    
    
    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = ["pride", "clusters"],
                          primary_name = "cluster",
                          local_extension = ".msp.gz",
                          secondary_name = "taxon",
                          cache = ECache.ALL,
                          keyed = ECache.ALL,
                          default_map = _PrideClustersImpl.__DEFAULT_MAP,
                          source_url = _PrideClustersImpl.__DEFAULT_URL )
    
    
    def _on_parse( self, primary_ac: str ) -> TParsed:
        peptides = set()
        
        with gzip.open( self.download( primary_ac ), "rt" ) as fin:
            for line in fin:
                if line.startswith( "Name:" ):
                    line = line.split( ":", 1 )[1].strip()
                    
                    if "/" in line:
                        line = line.split( "/", 1 )[0]
                        peptides.add( line )
        
        return PrideCluster( frozenset( peptides ) )


pride_clusters = _PrideClustersImpl()
