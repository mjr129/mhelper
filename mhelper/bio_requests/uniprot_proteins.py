"""
Obtains protein information from Uniprot.

* Get the sequence, name, and taxonomy information from a uniprot accession.


Example
-------

Download a protein and show the name and sequence::

    from mhelper.bio_requests.uniprot_proteins import uniprot_proteins, UniprotProtein, ProteinNotFoundError
    
    try:
        protein : UniprotProtein = uniprot_proteins.parse( "P12345" )
    except ProteinNotFoundError:
        print( "Not found." )
    else:
        print( protein.name )
        print( protein.sequence )
        
        
Implementation details
----------------------

* The primary accession should be a Uniprot ID.
* The secondary accession is not supported.
* Calling `download` returns the raw protein information file.
* Calling `parse` gives the information as a `UniprotProtein` object or raises a `ProteinNotFoundError`.
* Information is not cached (parsing is quick, assuming the raw protein information file is maintained in the web cache)
"""
# !EXPORT_TO_README
import json as _json
from dataclasses import dataclass as _dataclass

from mhelper import array_helper as _array_helper
from mhelper.exception_helper import NotFoundError as _NotFoundError

from mhelper.bio_requests.generics import ECache as _ECache, MappedReceiver as _MappedReceiver


_TUniprotId = str

__all__ = "uniprot_proteins", "UniprotProtein", "ProteinNotFoundError"


@_dataclass
class UniprotProtein:
    """
    Wraps up the parsed protein information file into a Python dataclass.
    """
    accession: str
    name: str
    sequence: str
    organism: int


class ProteinNotFoundError( _NotFoundError ):
    """
    Raised by `uniprot_proteins.parse` if the results indicate the protein was not found in Uniprot.
    """
    pass


class _UniprotProteinsImpl( _MappedReceiver[_TUniprotId, None, UniprotProtein] ):
    """
    See module comments for further details.
    This is a singleton class exposed as `uniprot_proteins`. 
    """
    def __init__( self ):
        super().__init__( module = __name__,
                          config_key = ["uniprot", "proteins"],
                          primary_name = "uniprot-id",
                          local_extension = ".json",
                          secondary_name = None,
                          cache = _ECache.NONE,
                          keyed = _ECache.ALL,
                          default_map = None,
                          source_url = "https://www.ebi.ac.uk/proteins/api/proteins/{}" )
    
    
    def _on_parse( self, primary_ac: str ) -> UniprotProtein:
        rq = self.request( primary_ac )
        
        if "Can not find Entry for accession" in rq:
            raise ProteinNotFoundError( f"Uniprot entry {primary_ac!r} not found. See {self._get_source_url( primary_ac )!r}." )
        
        try:
            data = _json.loads( rq )
        except Exception as ex:
            raise RuntimeError( f"Cannot parse Uniprot entry {primary_ac!r}. See {self._get_source_url( primary_ac )!r}." ) from ex
        
        protein_sequence = data["sequence"]["sequence"]
        protein_name = _array_helper.get_path( data, "protein", "recommendedName", "fullName", "value", default = "Untitled protein" )
        organism_ = int( _array_helper.get_path( data, "organism", "taxonomy", default = -1 ) )
        
        return UniprotProtein( primary_ac, protein_name, protein_sequence, organism_ )


uniprot_proteins = _UniprotProteinsImpl()
