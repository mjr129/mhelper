============
Bio Requests
============

A Python library for requesting, downloading, caching, and parsing biological data.
For instance it is able to obtain Uniprot_ protein sequences and `NCBI taxonomic`_ classifications.
Data is typically be sourced from disk and downloaded if required. 


.. _Uniprot: https://www.uniprot.org
.. _`NCBI taxonomic`: https://www.ncbi.nlm.nih.gov/taxonomy

.. contents::

Supported datasets
------------------

* NCBI Taxonomy
* Uniprot accessions
* Uniprot protein sequences
* Uniprot proteomes
* GenomeNet AA indices (1)
* *Custom datasets via* `core`


Usage
-----

All modules are *independent*, they can be imported individually as required, e.g. ::

    from mhelper.bio_requests import uniprot_accessions

Documentation module-specific and is included in the modules themselves. It is not repeated here.


Configuration
~~~~~~~~~~~~~

`bio_requests` provides an *executable*, by which the user may explore the system, such as finding
out where datasets have been downloaded to, or to configure the system, such as providing the
location of pre-downloaded datasets.

To access help on this executable invoke::

    python -m mhelper.bio_requests --help

This command also provides the path to the configuration file, which is in JSON format and may be
edited manually.


Caching
~~~~~~~

Files are downloaded and converted to a Python-understandable format, which is also saved to disk.
Web requests are also cached to avoid multiple repeated requests to academic servers.
Due to the variety of datasets in size and scope, there is no reliable "uncaching" automation and
cached files must be removed manually.

The *default* location for cache and configuration files is governed by `mhelper.specific_locations`
- see this module for more information. Note that this may be overridden on a per-file or global
basis, by the *configuration*, above.


Core
~~~~

The `core` module provides access to common features such as determining whether a particular dataset needs downloading, and if not, where to find it.
This module may be used externally for such requests in order to provide configuration and file acquisition in a coherent and user-configurable manner.
