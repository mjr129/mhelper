"""
Peptide atlas data dumps.
"""
import re
from dataclasses import dataclass
from functools import lru_cache
from typing import Dict, Optional

from mhelper.bio_requests import core
from mhelper.bio_requests.generics import MappedReceiver, TPrimary, TSecondary, ECache
from mhelper.string_helper import string_to_int


__all__ = "peptide_atlas_builds"

TUrl = str
TTaxon = int
DataError = core.DataError


@dataclass( frozen = True )
class PeptideAtlasUrls:
    mysql_database: TUrl
    protein_tsv: TUrl
    peptide_fasta: TUrl


class _PeptideAtlasImpl( MappedReceiver[TUrl, TTaxon, None] ):
    def __init__( self,
                  local_extension: str,
                  attr_name: str
                  ):
        super().__init__( module = __name__,
                          config_key = ["peptideatlas", "builds", attr_name],
                          primary_name = f"URL({local_extension})",
                          local_extension = local_extension,
                          secondary_name = "taxon-id",
                          cache = ECache.ALL,
                          keyed = ECache.ALL,
                          default_map = None,
                          source_url = "{}" )
        self.attr_name = attr_name
        assert attr_name in PeptideAtlasUrls.__annotations__
    
    
    def _on_load_default_map( self ) -> Dict[TSecondary, TPrimary]:
        return _load_default_map( self.attr_name )


class peptide_atlas_builds:
    peptide_fasta = _PeptideAtlasImpl( ".fasta", "peptide_fasta" )
    mysql_database = _PeptideAtlasImpl( ".mysql.gz", "mysql_database" )
    protein_tsv = _PeptideAtlasImpl( ".tsv", "protein_tsv" )


def _load_default_map( attr_name: str ) -> Dict[int, str]:
    # peptide_atlas_builds.peptide_fasta.load_default_map()
    # peptide_atlas_builds.mysql_database.load_default_map()
    # peptide_atlas_builds.protein_tsv.load_default_map()
    
    return { k: getattr( v, attr_name ) for k, v in __load_default_map().items() }


@lru_cache
def __load_default_map() -> Dict[int, PeptideAtlasUrls]:
    url = "http://www.peptideatlas.org/builds/"
    data = core.request( url )
    row_finder = re.compile( r"<tr[^>]*>(.*?)</tr>", re.IGNORECASE | re.DOTALL )
    column_finder = re.compile( r"<td[^>]*>(.*?)</td>", re.IGNORECASE | re.DOTALL )
    mysql_finder = re.compile( r'"([^"]+\.mysql\.gz)"' )
    fasta_finder = re.compile( r'"([^"]+\.fasta)"' )
    tsv_finder = re.compile( r'"([^"]+\.tsv)"' )
    
    urls = { }
    
    for match in row_finder.finditer( data ):
        row = match.group( 1 )
        
        cells = [match2.group( 1 ) for match2 in column_finder.finditer( row )]
        
        if len( cells ) < 13:
            continue
        
        tax_id = string_to_int( cells[2], None )
        num_samples = string_to_int( cells[4], None )
        
        if num_samples is None:
            continue
        
        peptide_sequences = cells[9]
        peptide_cds_coordinates = cells[10]
        db_downloads = cells[12]
        
        my_sql = __read_rx( url, mysql_finder, db_downloads )
        fasta = __read_rx( url, fasta_finder, peptide_sequences )
        tsv = __read_rx( url, tsv_finder, peptide_cds_coordinates )
        
        current_count = urls.get( tax_id, (-1,) )[0]
        
        if num_samples > current_count:
            urls[tax_id] = num_samples, PeptideAtlasUrls( my_sql, tsv, fasta )
    
    return { k: v[1] for k, v in urls.items() }


def __read_rx( prefix: str, regex: re.Pattern, string: str ) -> Optional[str]:
    mat = regex.search( string )
    if not mat:
        return None
    
    return prefix + mat.group( 1 )
