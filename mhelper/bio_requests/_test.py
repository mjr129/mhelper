import os

from mhelper.bio_requests import ncbi_taxonomy, uniprot_accessions, uniprot_proteins, uniprot_proteomes, core
from mhelper import log_helper, ansi_format_helper


def run_tests():
    ansi_format_helper.install_error_hook()
    core.core_log.enabled_hint = True
    
    taxonomy = ncbi_taxonomy.get_taxonomy()
    
    print( f"There are {len( taxonomy )} taxa in NCBI." )
    
    hs_name = "Homo sapiens"
    hs_id = taxonomy.get_id( hs_name )
    an_id = taxonomy.get_parent( hs_id )
    an_name = taxonomy.get_name( an_id )
    print( f"The ancestor of {hs_name!r} ({hs_id}) is {an_name!r} ({an_id})" )
    
    protein_id = "P19414"
    protein = uniprot_proteins.get_uniprot_protein( protein_id )
    print( f"The protein {protein_id!r} is called {protein.name!r} and it's sequence is {protein.sequence.__len__()} amino acids long." )
    
    gene_name = uniprot_accessions.convert_accession( fr = uniprot_accessions.EAccessionType.UniProtKB_AC,
                                                      to = uniprot_accessions.EAccessionType.Gene_name,
                                                      accession = "P19414" )
    print( f"The corresponding gene is {gene_name!r}." )
    
    proteome_id = uniprot_proteomes.find_proteome( 9606 )
    print( f"The Uniprot ID of the human proteome is {proteome_id!r}." )
    
    proteome_file = uniprot_proteomes.get_proteome_file( proteome_id )
    assert os.path.isfile( proteome_file )
    print( f"This proteome has been downloaded to {proteome_file!r}" )
