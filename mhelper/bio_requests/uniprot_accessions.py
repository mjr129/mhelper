"""
Check and convert uniprot accessions.

* Check accessions are valid
* Convert to and from different accession types
* Enumerate the available accession types
"""
# !EXPORT_TO_README
import re
import time
from collections import defaultdict
from enum import Enum
from typing import Dict, List, Union, Optional, Iterable

from mhelper.bio_requests import core


_query_url = core.DiskBackedDict( "uniprot/accessions/_remote.json" ).get( "value", "https://www.uniprot.org/uploadlists/" )

_rx1 = re.compile( "^[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}$" )


def check_accession( accession: str ) -> bool:
    """
    Determines if a uniprot accession is valid.
    """
    return len( accession ) == 6 and bool( _rx1.match( accession ) )


def convert_accession( fr: Union["EAccessionType", str],
                       to: Union["EAccessionType", str],
                       accession: str ) -> str:
    """
    See `convert_accessions`.
    """
    return convert_accessions( fr, to, accession )[accession][0]


def convert_accessions( fr: Union["EAccessionType", str],
                        to: Union["EAccessionType", str],
                        accessions: Union[str, Iterable[str]]
                        ) -> Optional[Dict[str, List[str]]]:
    """
    Maps accessions. 
    """
    if isinstance( accessions, str ):
        accessions = accessions,

    if not accessions:
        raise ValueError( "At least one accession must be specified." )

    if isinstance( fr, Enum ):
        fr = fr.value

    if isinstance( to, Enum ):
        to = to.value

    data = { "from"  : fr,
             "to"    : to,
             "format": "tab",
             "query" : " ".join( accessions ) }

    rq = core.make_request( _query_url, data = data )
    n = 0

    while True:
        result_str = rq.get()

        # Service errors - retry
        if not result_str or "Service Unavailable" in result_str:
            time.sleep( 1 )
            n += 1

            if n > 3:
                raise ValueError( f"Uniprot doesn't appear to be responding correctly right now. My request was {_query_url!r} with data {data!r}." )

            continue

        break

    # No such resource errors
    if "The resource you requested could not be found" in result_str:
        return None

    rows_lst: List[List[str]] = [line.split( "\t" ) for line in result_str.split( "\n" ) if line]
    rows = iter( rows_lst )

    header: List[str] = next( rows ) if rows_lst else []
    try:
        col_from: int = header.index( "From" )
        col_to: int = header.index( "To" )
    except ValueError as ex:
        # Other errors (bad: these should have been classified as service-errors or no-such-resource errors, above, instead.)
        rq.uncache()
        raise RuntimeError( f"Could not decode the response from {_query_url} with data {data!r}. If an error has been returned from the server, appropriate handler code should be present in `convert_accessions`. The result has been removed from the cache." ) from ex

    r = defaultdict( list )

    for row in rows:
        from__ = row[col_from]
        to__ = row[col_to]

        r[from__].append( to__ )

    return dict( r )


# noinspection SpellCheckingInspection
class EAccessionType( Enum ):
    """
    Names of the accession types converted by Uniprot.
    """
    # Category: UniProt
    UniProtKB_AC_ID = "ACC+ID"  # from
    UniProtKB_AC = "ACC"  # both
    UniProtKB_ID = "ID"  # both
    UniParc = "UPARC"  # both
    UniRef50 = "NF50"  # both
    UniRef90 = "NF90"  # both
    UniRef100 = "NF100"  # both
    Gene_name = "GENENAME"  # both
    CRC64 = "CRC64"  # both
    # Category: Sequence databases
    EMBL_GenBank_DDBJ = "EMBL_ID"  # both
    EMBL_GenBank_DDBJ_CDS = "EMBL"  # both
    Entrez_Gene_GeneID = "P_ENTREZGENEID"  # both
    GI_number = "P_GI"  # both
    PIR = "PIR"  # both
    RefSeq_Nucleotide = "REFSEQ_NT_ID"  # both
    RefSeq_Protein = "P_REFSEQ_AC"  # both
    # Category: 3D structure databases
    PDB = "PDB_ID"  # both
    # Category: Protein-protein interaction databases
    BioGrid = "BIOGRID_ID"  # both
    ComplexPortal = "COMPLEXPORTAL_ID"  # both
    DIP = "DIP_ID"  # both
    STRING = "STRING_ID"  # both
    # Category: Chemistry
    ChEMBL = "CHEMBL_ID"  # both
    DrugBank = "DRUGBANK_ID"  # both
    GuidetoPHARMACOLOGY = "GUIDETOPHARMACOLOGY_ID"  # both
    SwissLipids = "SWISSLIPIDS_ID"  # both
    # Category: Protein family/group databases
    Allergome = "ALLERGOME_ID"  # both
    ESTHER = "ESTHER_ID"  # both
    MEROPS = "MEROPS_ID"  # both
    mycoCLAP = "MYCOCLAP_ID"  # both
    PeroxiBase = "PEROXIBASE_ID"  # both
    REBASE = "REBASE_ID"  # both
    TCDB = "TCDB_ID"  # both
    # Category: PTM databases
    GlyConnect = "GLYCONNECT_ID"  # both
    # Category: Polymorphism and mutation databases
    BioMuta = "BIOMUTA_ID"  # both
    DMDM = "DMDM_ID"  # both
    # Category: 2D gel databases
    World_2DPAGE = "WORLD_2DPAGE_ID"  # both
    # Category: Proteomic databases
    CPTAC = "CPTAC_ID"  # both
    ProteomicsDB = "PROTEOMICSDB_ID"  # both
    # Category: Protocols and materials databases
    DNASU = "DNASU_ID"  # both
    # Category: Genome annotation databases
    Ensembl = "ENSEMBL_ID"  # both
    Ensembl_Protein = "ENSEMBL_PRO_ID"  # both
    Ensembl_Transcript = "ENSEMBL_TRS_ID"  # both
    Ensembl_Genomes = "ENSEMBLGENOME_ID"  # both
    Ensembl_Genomes_Protein = "ENSEMBLGENOME_PRO_ID"  # both
    Ensembl_Genomes_Transcript = "ENSEMBLGENOME_TRS_ID"  # both
    GeneDB = "GENEDB_ID"  # both
    GeneID_Entrez_Gene = "P_ENTREZGENEID"  # both
    KEGG = "KEGG_ID"  # both
    PATRIC = "PATRIC_ID"  # both
    UCSC = "UCSC_ID"  # both
    VectorBase = "VECTORBASE_ID"  # both
    WBParaSite = "WBPARASITE_ID"  # both
    # Category: Organism-specific databases
    ArachnoServer = "ARACHNOSERVER_ID"  # both
    Araport = "ARAPORT_ID"  # both
    CCDS = "CCDS_ID"  # both
    CGD = "CGD"  # both
    ConoServer = "CONOSERVER_ID"  # both
    dictyBase = "DICTYBASE_ID"  # both
    EchoBASE = "ECHOBASE_ID"  # both
    euHCVdb = "EUHCVDB_ID"  # both
    EuPathDB = "EUPATHDB_ID"  # both
    FlyBase = "FLYBASE_ID"  # both
    GeneCards = "GENECARDS_ID"  # both
    GeneReviews = "GENEREVIEWS_ID"  # both
    HGNC = "HGNC_ID"  # both
    HPA = "HPA_ID"  # both
    LegioList = "LEGIOLIST_ID"  # both
    Leproma = "LEPROMA_ID"  # both
    MaizeGDB = "MAIZEGDB_ID"  # both
    MGI = "MGI_ID"  # both
    MIM = "MIM_ID"  # both
    neXtProt = "NEXTPROT_ID"  # both
    Orphanet = "ORPHANET_ID"  # both
    PharmGKB = "PHARMGKB_ID"  # both
    PomBase = "POMBASE_ID"  # both
    PseudoCAP = "PSEUDOCAP_ID"  # both
    RGD = "RGD_ID"  # both
    SGD = "SGD_ID"  # both
    TubercuList = "TUBERCULIST_ID"  # both
    VGNC = "VGNC_ID"  # both
    WormBase = "WORMBASE_ID"  # both
    WormBase_Protein = "WORMBASE_PRO_ID"  # both
    WormBase_Transcript = "WORMBASE_TRS_ID"  # both
    Xenbase = "XENBASE_ID"  # both
    ZFIN = "ZFIN_ID"  # both
    # Category: Phylogenomic databases
    eggNOG = "EGGNOG_ID"  # both
    GeneTree = "GENETREE_ID"  # both
    HOGENOM = "HOGENOM_ID"  # both
    KO = "KO_ID"  # both
    OMA = "OMA_ID"  # both
    OrthoDB = "ORTHODB_ID"  # both
    TreeFam = "TREEFAM_ID"  # both
    # Category: Enzyme and pathway databases
    BioCyc = "BIOCYC_ID"  # both
    PlantReactome = "PLANT_REACTOME_ID"  # both
    Reactome = "REACTOME_ID"  # both
    UniPathway = "UNIPATHWAY_ID"  # both
    # Category: Gene expression databases
    CollecTF = "COLLECTF_ID"  # both
    # Category: Family and domain databases
    DisProt = "DISPROT_ID"  # both
    # Category: Other
    ChiTaRS = "CHITARS_ID"  # both
    GeneWiki = "GENEWIKI_ID"  # both
    GenomeRNAi = "GENOMERNAI_ID"  # both
