"""
Uniprot proteomes
=================

This module exports a `ProteomeReceiver` which allows the user to find, download and parse uniprot proteomes.
The API is identical to `MappedReceiver`.

E.g. find the proteome ID for a given taxon::

    from mhelper.bio_requests.uniprot_proteomes import uniprot_canonical as rx
    
    human_taxid = 9606 
    human_protid = rx.map_accession( human_taxid )

E.g. download the human proteome::

    file_name = rx.download( human_taxid )

Or::

    file_name = rx.download( human_protid )
    
4 versions of the object are exported, which allow you to select from
*uniprot* and *uniparc* and whether or not to *include isoforms*::

     uniprot_canonical
     uniparc_canonical
     uniprot_isoforms
     uniparc_isoforms

This can also be accessed programatically::

    from mhelper.bio_requests.uniprot_proteomes import get_api
    rx = get_api( include_isoforms = True, include_uniparc = False ) 

The API is identical for all versions and derives from `MappedReceiver`.
See `MappedReceiver` for technical details.

:data uniprot_canonical: Instance of `ProteomeReceiver` using *Uniprot* and only including the *canonical* isoform.
:data uniparc_canonical: Instance of `ProteomeReceiver` using *Uniparc* and only including the *canonical* isoform.
:data uniprot_isoforms:  Instance of `ProteomeReceiver` using *Uniprot* and including *all isoforms*.
:data uniparc_isoforms:  Instance of `ProteomeReceiver` using *Uniparc* and including *all isoforms*.
"""
# !EXPORT_TO_README
from typing import Tuple

from mhelper.bio_requests.generics import TSecondary, TPrimary, ECache

from mhelper.bio_requests import core, generics
from mhelper.bio_requests.core import DataError
from urllib import parse
import json as _json


__all__ = "uniprot_proteomes", "DataError", "uniprot_canonical", "DataError", "uniparc_canonical", "uniprot_isoforms", "uniparc_isoforms", "get_api"

TProteomeId = str
TTaxonId = int


class ProteomeReceiver( generics.MappedReceiver[TProteomeId, TTaxonId, None] ):
    """
    Downloader and mapper for Uniprot/Uniparc.
    See module documentation for usage.
    """
    # __URL =
    __ACCESSIONS_URL = "https://sparql.uniprot.org/sparql/?query={query}&format=json"

    def __init__( self, key: str, url: str ):
        super().__init__(
                module = __name__,
                config_key = ["uniprot", "proteomes", key],
                primary_name = "proteome-id",
                secondary_name = "taxon-id",
                cache = ECache.ALL,
                keyed = ECache.ALL,
                default_map = None,
                source_url = url,
                local_extension = ".fasta.gz" )


    def _on_map_accession( self, secondary_ac: TSecondary ) -> TPrimary:
        proteomes = self.map_accessions( secondary_ac )

        if not proteomes:
            raise self._raise_error( f"No reference proteomes are available for the taxon {secondary_ac}.\n"
                                     f"There may be non-reference proteomes available, but these were not included in the search.\n"
                                     f"See also `{self.__get_request_url( secondary_ac )}`." )

        if len( proteomes ) > 1:
            raise self._raise_error( f"More than one reference proteome is available for the taxon {secondary_ac}." )

        return proteomes[0]


    def map_accessions( self, x: TSecondary, reference_only = True ) -> Tuple[TPrimary, ...]:
        """
        Finds the proteome ID(s) associated with the specified taxon.
        
        :param x:               Taxon, as an NCBI numeric identifier. 
        :param reference_only:  Only include reference proteomes in the results. 
        :return:                List of proteome IDs, e.g. "UP000005640". 
        """
        #
        # Query URL
        #
        url = self.__get_request_url( x, reference_only )

        json_txt = core.request( url )
        json = _json.loads( json_txt )

        # Json example:
        # {
        #   "head" : {
        #     "vars" : [
        #       "proteome"
        #     ]
        #   },
        #   "results" : {
        #     "bindings" : [
        #       {
        #         "proteome" : {
        #           "type" : "uri",
        #           "value" : "http://purl.uniprot.org/proteomes/UP000005640"
        #         }
        #       },
        #       {
        #         "proteome" : {
        #           "type" : "uri",
        #           "value" : "http://purl.uniprot.org/proteomes/UP000307385"
        #         }
        #       },
        #       {
        #         "proteome" : {
        #           "type" : "uri",
        #           "value" : "http://purl.uniprot.org/proteomes/UP000308576"
        #         }
        #       }
        #     ]
        #   }
        # }

        results = json["results"]["bindings"]
        return tuple( r["proteome"]["value"].rsplit( "/", 1 )[1] for r in results )

    def __get_request_url( self, x, reference_only = True ):
        query = """
                SELECT * FROM <http://sparql.uniprot.org/proteomes> WHERE
                {{
                  ?proteome <http://purl.uniprot.org/core/organism> <http://purl.uniprot.org/taxonomy/{taxon}> .
                  {constraint}
                }}
                """
        if reference_only:
            constraint = "?proteome <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.uniprot.org/core/Reference_Proteome> ."
        else:
            constraint = ""
        query = parse.quote( query.format( taxon = x, constraint = constraint ) )
        url = self.__ACCESSIONS_URL.format( query = query )
        return url


uniprot_canonical = ProteomeReceiver( "uniprot_canonical", "https://www.uniprot.org/uniprot?include=false&format=fasta&compress=yes&force=true&query=proteome:{}" )
uniparc_canonical = ProteomeReceiver( "uniparc_canonical", "https://www.uniprot.org/uniparc?include=false&format=fasta&compress=yes&force=true&query=proteome:{}" )
uniprot_isoforms = ProteomeReceiver( "uniprot_isoforms", "https://www.uniprot.org/uniprot?include=true&format=fasta&compress=yes&force=true&query=proteome:{}" )
uniparc_isoforms = ProteomeReceiver( "uniparc_isoforms", "https://www.uniprot.org/uniparc?include=true&format=fasta&compress=yes&force=true&query=proteome:{}" )

uniprot_proteomes = uniprot_canonical  # deprecated


def get_api( *, include_isoforms: bool = False, include_uniparc: bool = False ) -> ProteomeReceiver:
    """
    Returns the instance of the API pertinent to the arguments.
    
    :param include_isoforms:    `True`  --> Return the API that includes isoforms.
                                `False` --> Return the API that only includes the canonical isoform.  
    :param include_uniparc:     `True`  --> Return the API that uses Uniparc.
                                `False` --> Return the API that uses Uniprot.
    :return: The API, an instance of `ProteomeReceiver`.
    """
    if include_uniparc:
        return uniparc_isoforms if include_isoforms else uniparc_canonical
    else:
        return uniprot_isoforms if include_isoforms else uniprot_canonical
