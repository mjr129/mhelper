from .classes import MAnnotation
from .predefined_classes import EFileMode
from .predefined import isOptionList, isFilename, isReadonly, isDirname, isOptional, isUnion, isPassword
from .inspector import AnnotationInspector
from .predefined_inspectors import FunctionInspector, ArgCollection, ArgInspector, ArgsKwargs, ArgValueCollection, IFunction
